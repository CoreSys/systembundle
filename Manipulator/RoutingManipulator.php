<?php

namespace CoreSys\CoreBundle\Manipulator;

use Sensio\Bundle\GeneratorBundle\Manipulator\Manipulator;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class RoutingManipulator
 * @package CoreSys\CoreBundle\Manipulator
 */
class RoutingManipulator extends Manipulator
{

    /**
     * @var string|null
     */
    private $file;

    /**
     * RoutingManipulator constructor.
     *
     * @param $file
     */
    public function __construct( $file )
    {
        $this->file = $file;
    }

    /**
     * @param        $bundle
     * @param string $prefix
     * @param        $entity
     *
     * @return bool
     */
    public function addResource( $bundle, $prefix = '/', $entity )
    {
        $current = '';
        $code    = sprintf( "%s:\n", Container::underscore( substr( $bundle, 0, -6 ) ) . '_' . Container::underscore( $entity ) . ( '/' !== $prefix ? '_' . str_replace( '/', '_', substr( $prefix, 1 ) ) : '' ) );
        if ( file_exists( $this->file ) ) {
            $current = file_get_contents( $this->file );

            // Don't add same bundle twice
            if ( FALSE !== strpos( $current, $code ) ) {
                throw new \RuntimeException( sprintf( 'Bundle "%s" is already imported.', $bundle ) );
            }
        } elseif ( !is_dir( $dir = dirname( $this->file ) ) ) {
            mkdir( $dir, 0777, TRUE );
        }

        $code .= sprintf( "    resource: \"@%s/Controller/%sRestController.php\"\n", $bundle, $entity );
        $code .= sprintf( "    type:   %s\n", "rest" );
        $code .= sprintf( "    prefix:   %s\n", $prefix );
        $code .= sprintf( "    options:  \n", "");
        $code .= sprintf( "        expose:    true\n", "");
        $code .= "\n";
        $code .= $current;

        if ( FALSE === file_put_contents( $this->file, $code ) ) {
            return FALSE;
        }

        return TRUE;
    }
}