<?php

namespace CoreSys\CoreBundle\Handler;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class AuthenticationHandler
    implements AuthenticationSuccessHandlerInterface,
    AuthenticationFailureHandlerInterface
{

    private $router;

    private $session;

    public function __construct( RouterInterface $router, Session $session )
    {
        $this->router  = $router;
        $this->session = $session;
    }

    public function onAuthenticationFailure( Request $request, AuthenticationException $exception )
    {
        if ( $request->isXmlHttpRequest() ) {
            $array    = array(
                'success' => FALSE,
                'message' => $exception->getMessage()
            );
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;
        } else {
            $request->getSession()->set(
                Security::AUTHENTICATION_ERROR,
                $exception
            );

            return new RedirectResponse( '/login' );
        }
    }

    public function onAuthenticationSuccess( Request $request, TokenInterface $token )
    {
        if ( $request->isXmlHttpRequest() ) {
            $user = $token->getUser();
            $array    = array( 'success' => TRUE, 'user' => array( 'id' => $user->getId(), 'username' => $user->getUsername() ) );
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;
        } else {
            if ( $this->session->get( '_security.main.target_path' ) ) {
                $url = $this->session->get( '_security.main.target_path' );
            } else {
                $url = '/';
                $uri = $request->getRequestUri();
                if( strstr( $uri, 'app_dev.php' ) ) {
                    $url = '/app_dev.php';
                }
            }

            return new RedirectResponse( $url );
        }
    }


}
