# CoreSystems CoreBundle

Generalized bundle which makes rapid development much easier.
Contains databased roles, access, and user management.

- [Setup Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Setup.md)
- [Configuration Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Configuration.md)
- [Routing Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Routing.md)
- [Security Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Security.md)
- [Entity Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Entities.md)
- [Controller Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Controllers.md)
- [Manager Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Managers.md)
- [Bundles Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Bundles.md)
- [Twig Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Twig.md)
- [Generators Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Generators.md)
- [Rest Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Rest.md)
- [Database Backup Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/DatabaseBackup.md)