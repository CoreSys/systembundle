<?php

namespace CoreSys\CoreBundle\Generator;

use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Generate a Rest Controller
 */
class RestGenerator extends Generator
{

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var string
     */
    protected $routePrefix;

    /**
     * @var string
     */
    protected $routeNamePrefix;

    /**
     * @var BundleInterface
     */
    protected $bundle;

    /**
     * @var mixed
     */
    protected $entity;

    /**
     * @var ClassMetadata
     */
    protected $metadata;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var array
     */
    protected $actions;

    /**
     * RestGenerator constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct( Filesystem $filesystem )
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param BundleInterface $bundle
     * @param                 $entity
     * @param ClassMetadata   $metadata
     * @param                 $routePrefix
     * @param                 $forceOverwrite
     */
    public function generate( BundleInterface $bundle, $entity, ClassMetadata $metadata, $routePrefix, $forceOverwrite )
    {
        $this->routePrefix     = $routePrefix;
        $this->routeNamePrefix = str_replace( '/', '_', $routePrefix );
        $this->actions         = array( 'getById', 'getAll', 'post', 'put', 'delete' );

        if ( count( $metadata->identifier ) > 1 ) {
            throw new \RuntimeException( 'The Rest Api Generator does not support entity classes with multiple primary keys.' );
        }

        if ( !in_array( 'id', $metadata->identifier ) ) {
            throw new \RuntimeException( 'The Rest Api Generator expects that the entity object has a primary key field names "id" with a getId() method' );
        }

        $this->entity   = $entity;
        $this->bundle   = $bundle;
        $this->metadata = $metadata;
        $this->setFormat( 'yml' );

        $this->generateControllerClass( !!$forceOverwrite );
        $this->generateRestForms( !!$forceOverwrite );
    }

    /**
     *
     */
    public function generateConfiguration()
    {
        if ( !in_array( $this->format, array( 'yml', 'xml', 'php' ) ) ) {
            return;
        }

        $target = sprintf(
            '%s/Resource/config/routing/%s.%s',
            $this->bundle->getPath(),
            strtolower( str_replace( '\\', '_', $this->entity ) ),
            $this->format
        );

        $this->renderFile(
            'rest/config/routing.' . $this->format . '.twig',
            $target,
            array(
                'actions'           => $this->actions,
                'route_prefix'      => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'bundle'            => $this->bundle->getName(),
                'entity'            => $this->entity
            )
        );
    }

    /**
     * @param $format
     */
    public function setFormat( $format )
    {
        switch ( $format ) {
            case 'yml':
            case 'xml':
            case 'php':
            case 'annotation':
                $this->format = $format;
                break;
            default:
                $this->format = 'yml';
                break;
        }
    }

    /**
     * @param $forceOverwrite
     */
    public function generateControllerClass( $forceOverwrite )
    {
        $dir = $this->bundle->getPath();

        $parts           = explode( '\\', $this->entity );
        $entityClass     = array_pop( $parts );
        $entityNamespace = implode( '\\', $parts );

        $target = sprintf(
            '%s/Controller/%s/%sRestController.php',
            $dir,
            str_replace( '\\', '/', $entityNamespace ),
            $entityClass
        );

        if ( !$forceOverwrite && file_exists( $target ) ) {
            throw new \RuntimeException( 'Unable to generate the controller as it already exists.' );
        }

        $this->renderFile(
            'rest/controller.php.twig',
            $target,
            array(
                'actions'           => $this->actions,
                'route_prefix'      => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'bundle'            => $this->bundle->getName(),
                'entity'            => $this->entity,
                'entity_class'      => $entityClass,
                'namespace'         => $this->bundle->getNamespace(),
                'entity_namespace'  => $entityNamespace,
                'templateNamespace' => str_replace( '\\', '', str_replace( '/', '', $this->bundle->getNamespace() ) ),
                'format'            => $this->format
            )
        );
    }

    public function generateRestForms( $forceOverwrite = FALSE )
    {
        $dir             = $this->bundle->getPath();
        $parts           = explode( '\\', $this->entity );
        $entityClass     = array_pop( $parts );
        $entityNamespace = implode( '\\', $parts );

        $baseTarget = sprintf(
            '%s/Resources/views/%sRest',
            $dir,
            $entityClass
        );

        $newTarget  = $baseTarget . '/new.html.twig';
        $editTarget = $baseTarget . '/edit.html.twig';
        $formTarget = $baseTarget . '/form.html.twig';

        $data = array(
            'bundle'            => $this->bundle->getName(),
            'entity'            => $entityClass,
            'entitylower'       => strtolower( trim( $entityClass ) ),
            'namespace'         => $this->bundle->getNamespace(),
            'entity_namespace'  => $entityNamespace,
            'templateNamespace' => str_replace( '\\', '', str_replace( '/', '', $this->bundle->getNamespace() ) )
        );

        $this->generateRestNew( $newTarget, $data, !!$forceOverwrite );
        $this->generateRestEdit( $editTarget, $data, !!$forceOverwrite );
        $this->generateRestForm( $formTarget, $data, !!$forceOverwrite );
    }

    /**
     * Get filesystem
     *
     * @return Filesystem
     */
    public function getFilesystem()
    {
        return $this->filesystem;
    }

    /**
     * Set Filesystem
     *
     * @param Filesystem $filesystem
     *
     * @return RestGenerator
     */
    public function setFilesystem( $filesystem = NULL )
    {
        $this->filesystem = $filesystem;

        return $this;
    }

    /**
     * Get routePrefix
     *
     * @return string
     */
    public function getRoutePrefix()
    {
        return $this->routePrefix;
    }

    /**
     * Set RoutePrefix
     *
     * @param string $routePrefix
     *
     * @return RestGenerator
     */
    public function setRoutePrefix( $routePrefix = NULL )
    {
        $this->routePrefix = $routePrefix;

        return $this;
    }

    /**
     * Get routeNamePrefix
     *
     * @return string
     */
    public function getRouteNamePrefix()
    {
        return $this->routeNamePrefix;
    }

    /**
     * Set RouteNamePrefix
     *
     * @param string $routeNamePrefix
     *
     * @return RestGenerator
     */
    public function setRouteNamePrefix( $routeNamePrefix = NULL )
    {
        $this->routeNamePrefix = $routeNamePrefix;

        return $this;
    }

    /**
     * Get bundle
     *
     * @return BundleInterface
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Set Bundle
     *
     * @param BundleInterface $bundle
     *
     * @return RestGenerator
     */
    public function setBundle( $bundle = NULL )
    {
        $this->bundle = $bundle;

        return $this;
    }

    /**
     * Get entity
     *
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set Entity
     *
     * @param mixed $entity
     *
     * @return RestGenerator
     */
    public function setEntity( $entity = NULL )
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return ClassMetadata
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set Metadata
     *
     * @param ClassMetadata $metadata
     *
     * @return RestGenerator
     */
    public function setMetadata( $metadata = NULL )
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get actions
     *
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Set Actions
     *
     * @param array $actions
     *
     * @return RestGenerator
     */
    public function setActions( $actions = NULL )
    {
        $this->actions = $actions;

        return $this;
    }

    protected function generateRestNew( $target, array $data, $forceOverwrite = FALSE )
    {
        if ( !$forceOverwrite && file_exists( $target ) ) {
            throw new \RuntimeException( 'Unable to generate new.html.twig as it already exists.' );
        }

        $this->renderFile(
            'form/new.html.twig',
            $target,
            $data
        );
    }

    protected function generateRestEdit( $target, array $data, $forceOverwrite = FALSE )
    {
        if ( !$forceOverwrite && file_exists( $target ) ) {
            throw new \RuntimeException( 'Unable to generate edit.html.twig as it already exists.' );
        }

        $this->renderFile(
            'form/edit.html.twig',
            $target,
            $data
        );
    }

    protected function generateRestForm( $target, array $data, $forceOverwrite = FALSE )
    {
        if ( !$forceOverwrite && file_exists( $target ) ) {
            throw new \RuntimeException( 'Unable to generate form.html.twig as it already exists.' );
        }

        $this->renderFile(
            'form/form.html.twig',
            $target,
            $data
        );
    }

    /**
     *
     */
    protected function generateTestClass()
    {
        $parts           = explode( '\\', $this->entity );
        $entityClass     = array_pop( $part );
        $entityNamespace = implode( '\\', $parts );

        $dir    = $this->bundle->getPath() . '/Tests/Controller';
        $target = $dir . '/' . str_replace( '\\', '/', $entityNamespace ) . '/' . $entityClass . 'RestControllerTest.php';

        $this->renderFile(
            'rest/tests/test.php.twig',
            $target,
            array(
                'actions'           => $this->actions,
                'route_prefix'      => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'bundle'            => $this->bundle,
                'entity'            => $this->entity,
                'entity_class'      => $entityClass,
                'namespace'         => $this->bundle->getNamespace(),
                'entity_namespace'  => $entityNamespace,
                'templateNamespace' => str_replace( '\\', '', str_replace( '/', '', $this->bundle->getNamespace() ) ),
                'format'            => $this->format
            )
        );
    }

}