<?php

namespace CoreSys\CoreBundle\Util\Renderer;

/**
 * Interface RenderInterface
 * @package CoreSys\CoreBundle\Util\Renderer
 */
interface RenderInterface
{

    /**
     * @return void
     */
    public function process();

    /**
     * @return string
     */
    public function getTemplateUrl();

    /**
     * @return array
     */
    public function getTemplateParameters();

    /**
     * @return mixed
     */
    public function getRenderedName();
}