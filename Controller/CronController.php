<?php

namespace CoreSys\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class CronController
 * @package CoreSys\CoreBundle\Controller
 * @Route("/coreCron")
 */
class CronController extends BaseController
{

    /**
     * @Route("/databaseBackup", name="core_cron_database_backup")
     * @Template()
     */
    public function databaseBackupAction()
    {
        $manager = $this->get( 'core_sys_core.manager.database' );
        $result  = $manager->createBackup();

        echo $result ? 'Backup Complete' : 'Backup Failed';
        exit;
    }
}