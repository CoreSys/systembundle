<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\User;
use CoreSys\CoreBundle\Form\UserType;
use CoreSys\CoreBundle\Form\AdminUserType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * User Controller
 * @Rest\RouteResource("User")
 * @Rest\NamePrefix("api_")
 */
class UserRestController extends BaseRestController
{

    /**
     * Datatables User entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for User",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:User' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables User entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for User",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:User' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Get a User entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single User entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when User not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for User" }
     *   },
     *   output="CoreSys\CoreBundle/entity/User"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( User $entity )
    {
        return $entity;
    }

    /**
     * Get all User entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all User(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<CoreSys\CoreBundle\Entity\User>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many User(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many User(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'CoreSysCoreBundle:User' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new User
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new User",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/UserType",
     *   output="CoreSys\CoreBundle/Entity/User"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        $entity = new User();
        $form   = $this->createForm( UserType::class, $entity, array( 'method' => $request->getMethod() ) );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $this->get( 'core_sys_core.manager.user' )->updateUser( $entity, TRUE );

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Enable a user
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Enable a user",
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when there is an error",
     *      404="Returned when the user is not found"
     *     },
     *     input="CoreSys\CoreBundle\Entity\User",
     *     output="CoreSys\CoreBundle\Entity\User"
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function enableAction( User $user )
    {
        $user->setEnabled( FALSE );
        $this->get( 'core_sys_core.manager.user' )->updateUser( $user, TRUE );

        return $user;
    }

    /**
     * Disable a user
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Disable a user",
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when there is an error",
     *      404="Returned when the user is not found"
     *     },
     *     input="CoreSys\CoreBundle\Entity\User",
     *     output="CoreSys\CoreBundle\Entity\User"
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function disableAction( User $user )
    {
        $user->setEnabled( FALSE );
        $this->get( 'core_sys_core.manager.user' )->updateUser( $user, TRUE );

        return $user;
    }

    /**
     * Update a(n) User entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) User",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/UserType",
     *   output="CoreSys\CoreBundle/Entity/User"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, User $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $formClass = UserType::class;

            if ( $this->isGranted( 'ROLE_ADMIN' ) ) {
                $formClass    = AdminUserType::class;
            }

            $form = $this->createForm( $formClass, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );

            if ( $form->isValid() ) {
                $entity = $form->getData();
                $sysRoles = $entity->getSysRoles();
                $entity = $this->get( 'core_sys_core.manager.user' )->updateUser( $entity, TRUE );

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Patch a(n) User entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) User",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/UserType",
     *   output="CoreSys\CoreBundle/Entity/User"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param User    $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, User $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) User
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) User",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when User not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request $request
     * @param User    $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, User $entity )
    {
        try {
            $this->removeAndFlush( $entity );

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new User
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new User form to create a new User",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity       = new User();
        $templateName = 'new.html.twig';
        $formClass    = UserType::class;

        if ( $this->isGranted( 'ROLE_ADMIN' ) ) {
            $templateName = 'adminNew.html.twig';
            $formClass    = AdminUserType::class;
        }

        $form = $this->createForm( $formClass, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:UserRest:" . $templateName )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) User to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for User to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when User not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( User $entity )
    {
        $templateName = 'edit.html.twig';
        $formClass    = UserType::class;

        if ( $this->isGranted( 'ROLE_ADMIN' ) ) {
            $templateName = 'adminEdit.html.twig';
            $formClass    = AdminUserType::class;
        }

        $form = $this->createForm( $formClass, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:UserRest:" . $templateName )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Lock a user
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Lock a user",
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when there is an error",
     *      404="Returned when the user is not found"
     *     },
     *     input="CoreSys\CoreBundle\Entity\User",
     *     output="CoreSys\CoreBundle\Entity\User"
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function lockAction( User $user )
    {
        $user->setLocked( FALSE );
        $this->get( 'core_sys_core.manager.user' )->updateUser( $user, TRUE );

        return $user;
    }

    /**
     * Unlock a user
     *
     * @ApiDoc(
     *     resource=true,
     *     description="UnLock a user",
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when there is an error",
     *      404="Returned when the user is not found"
     *     },
     *     input="CoreSys\CoreBundle\Entity\User",
     *     output="CoreSys\CoreBundle\Entity\User"
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function unlockAction( User $user )
    {
        $user->setLocked( FALSE );
        $this->get( 'core_sys_core.manager.user' )->updateUser( $user, TRUE );

        return $user;
    }
}