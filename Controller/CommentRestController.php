<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\Note;
use CoreSys\CoreBundle\Form\CommentType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
* Comment Controller
* @Rest\RouteResource("Comment")
* @Rest\NamePrefix("api_")
*/
class CommentRestController extends BaseRestController
{
    /**
    * Datatables Comment entity
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Return the datatables response for Comment",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is an internal server error"
    *       }
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @return Response
        */
    public function getDatatablesAction(Request $request)
{
    $repo = $this->getRepo( 'CoreSysCoreBundle:Note' );
    return $this->processDatatables( $request, $repo );
}
    /**
    * Datatables Comment entity
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Return the datatables response for Comment",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is an internal server error"
    *       }
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @return Response
        */
    public function postDatatablesAction(Request $request)
{
    $repo = $this->getRepo( 'CoreSysCoreBundle:Note' );
    return $this->processDatatables( $request, $repo );
}
   /**
    * Get a Comment entity
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Get a single Comment entity",
    *   statusCodes={
    *       200="Returned when successful",
    *       404="Returned when Comment not found",
    *       500="Returned when there is a server error"
    *   },
    *   parameters={
    *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for Comment" }
    *   },
    *   output="CoreSys\CoreBundle/entity/Note"
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @return Response
        */
    public function getAction(Note $entity)
    {
        return $entity;
    }
    /**
    * Get all Comment entities
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @ApiDoc(
    *       resource=true,
    *       desription="Return all Comment(s)",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is a sever error"
    *       },
    *       output="ArrayCollection<CoreSys\CoreBundle\Entity\Note>",
    *       requirements={
    *           {
    *               "name"="limit",
    *               "dataType"="integer",
    *               "requirement"="\d+",
    *               "description"="How many Comment(s) to return"
    *           }
    *       },
    *       parameters={
    *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many Comment(s) to return" },
    *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
    *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
    *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
    *       }
    *   )
    *
    * @param ParamFetcherInterface $paramFetcher
    * @return Response
    *
    * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing comments.")
    * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many comments to return.")
    * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
    * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
        */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
{
    try {
        $offset = $paramFetcher->get( 'offset' );
        $limit = $paramFetcher->get( 'limit' );
        $order_by = $paramFetcher->get( 'order_by' );
        $filters = !is_null( $paramFetcher->get( 'filters' ) )
            ? $paramFetcher->get( 'filters' )
            : array();

        $entities = $this->getRepository( 'CoreSysCoreBundle:Note' )
            ->findBy( $filters, $order_by, $limit, $offset );

        if( $entities ) {
            return $entities;
        }

        return FOSView::create('Not Found', Codes::HTTP_NO_CONTENT);
    } catch( \Exception $e ) {
        return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
}
/**
    * Create a new Comment
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Create a new Comment",
    *   statusCodes={
    *       201="Returned when successful",
    *       400="Returned when there is an error",
    *       500="Returned where there is a server error"
    *   },
    *   input="CoreSys\CoreBundle/Form/CommentType",
    *   output="CoreSys\CoreBundle/Entity/Note"
    * )
    *
    * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @return Response
    */
    public function postAction(Request $request)
{
    $entity = new Note();
    $entity->setType( 'comment' );
    $form = $this->createForm( CommentType::class, $entity, array( 'method' => $request->getMethod()));
    $this->removeExtraFields( $request, $form );
    $form->handleRequest( $request );

    if( $form->isValid() ) {
        $entity = $form->getData();
        $this->persistAndFlush( $entity );

        return $entity;
    }
    return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
}
/**
    * Update a(n) Comment entity
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Update a(n) Comment",
    *   statusCodes={
    *       201="Returned when successful",
    *       400="Returned when there is an error",
    *       500="Returned where there is a server error"
    *   },
    *   input="CoreSys\CoreBundle/Form/CommentType",
    *   output="CoreSys\CoreBundle/Entity/Note"
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param $entity
    * @return Response
    */
    public function putAction(Request $request, Note $entity)
{
    try {
        $request->setMethod( 'PATCH' ); /* treat all puts as patch */
        $form = $this->createForm(CommentType::class, $entity, array( 'method' => $request->getMethod()));
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );
        if( $form->isValid() ) {
            $this->persistAndFlush( $entity );
            return $entity;
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    } catch( \Exception $e ) {
        return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
}
/**
    * Patch a(n) Comment entity
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Update a(n) Comment",
    *   statusCodes={
    *       201="Returned when successful",
    *       400="Returned when there is an error",
    *       500="Returned where there is a server error"
    *   },
    *   input="CoreSys\CoreBundle/Form/CommentType",
    *   output="CoreSys\CoreBundle/Entity/Note"
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param Note $entity
    * @return Response
    */
    public function patchAction(Request $request, Note $entity)
{
    return $this->putAction( $request, $entity );
}
/**
    * Delete a(n) Comment
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Delete a(n) Comment",
    *   statusCodes={
    *       204="Returned when successful",
    *       404="Returned when Comment not found",
    *       500="Returned when there is a server error"
    *   }
    * )
    *
    * @Rest\View(statusCode=204)
    *
    * @param Request $request
    * @param Note $entity
    * @return Response
    */
    public function deleteAction(Request $request, Note $entity)
{
    try {
        $this->removeAndFlush( $entity );

        return null;
    } catch( \Exception $e ) {
        return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
}
    /**
    * Get the form for a new Comment
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Get a new Comment form to create a new Comment",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is an internal server error"
    *       }
    *   )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @return Response
        */
    public function newAction()
{
    $entity = new Note();
    $entity->setType( 'comment' );
    $form = $this->createForm(CommentType::class, $entity, array('method' => 'POST'));
    $view = FOSView::create( array( 'form' => $form->createView() ) );
    $view->setTemplate( "CoreSysCoreBundle:CommentRest:new.html.twig" )
        ->setTemplateVar( 'form' );

    return $this->handleView( $view );
}
    /**
    * Get the form for a(n) Comment to edit
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Get a the form for Comment to edit",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           404="Returned when Comment not found",
    *           500="Returned when there is an internal server error"
    *       }
    *   )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @return Response
        */
    public function editAction(Note $entity)
{
    $form = $this->createForm(CommentType::class, $entity, array('method' => 'POST'));
    $view = FOSView::create( array( 'form' => $form->createView() ) );
    $view->setTemplate( "CoreSysCoreBundle:CommentRest:edit.html.twig" )
    ->setTemplateVar( 'form' );

    return $this->handleView( $view );
}
}