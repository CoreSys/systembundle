<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\AdminMenu;
use CoreSys\CoreBundle\Form\AdminMenuType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * AdminMenu Controller
 * @Rest\RouteResource("AdminMenu")
 * @Rest\NamePrefix("api_")
 */
class AdminMenuRestController extends BaseRestController
{

    /**
     * Datatables AdminMenu entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for AdminMenu",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:AdminMenu' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables AdminMenu entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for AdminMenu",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:AdminMenu' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Get a AdminMenu entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single AdminMenu entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when AdminMenu not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for AdminMenu" }
     *   },
     *   output="CoreSys\CoreBundle/entity/AdminMenu"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( AdminMenu $entity )
    {
        return $entity;
    }

    /**
     * Get all AdminMenu entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all AdminMenu(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<CoreSys\CoreBundle\Entity\AdminMenu>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many AdminMenu(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many AdminMenu(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'CoreSysCoreBundle:AdminMenu' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new AdminMenu
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new AdminMenu",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/AdminMenuType",
     *   output="CoreSys\CoreBundle/Entity/AdminMenu"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        $entity = new AdminMenu();
        $form   = $this->createForm( AdminMenuType::class, $entity, array( 'method' => $request->getMethod() ) );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $this->persistAndFlush( $entity );

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Update a(n) AdminMenu entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) AdminMenu",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/AdminMenuType",
     *   output="CoreSys\CoreBundle/Entity/AdminMenu"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, AdminMenu $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $form = $this->createForm( AdminMenuType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $this->persistAndFlush( $entity );

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Update the order of menus lists
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Update the order of adminMenus",
     *     statusCodes={
     *          200="Returned when successful"
     *     }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function putOrderAction( Request $request )
    {
        $order = $request->get( 'order' );
        if ( is_array( $order ) ) {
            $repo     = $this->getRepo( 'CoreSysCoreBundle:AdminMenu' );
            $position = 0;
            foreach ( $order as $id ) {
                $menu = $repo->find( $id );
                if ( !empty( $menu ) ) {
                    $menu->setPosition( $position++ );
                    $this->persist( $menu );
                }
            }
            $this->flush();

            return array( 'success' => TRUE, 'order' => $order );
        }

        return array( 'success' => FALSE, 'order' => $order );
    }

    /**
     * Patch a(n) AdminMenu entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) AdminMenu",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/AdminMenuType",
     *   output="CoreSys\CoreBundle/Entity/AdminMenu"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request   $request
     * @param AdminMenu $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, AdminMenu $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) AdminMenu
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) AdminMenu",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when AdminMenu not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request   $request
     * @param AdminMenu $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, AdminMenu $entity )
    {
        try {
            $this->removeAndFlush( $entity );

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new AdminMenu
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new AdminMenu form to create a new AdminMenu",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity = new AdminMenu();
        $form   = $this->createForm( AdminMenuType::class, $entity, array( 'method' => 'POST' ) );
        $view   = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:AdminMenuRest:new.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) AdminMenu to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for AdminMenu to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when AdminMenu not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( AdminMenu $entity )
    {
        $form = $this->createForm( AdminMenuType::class, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:AdminMenuRest:edit.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }
}