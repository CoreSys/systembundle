<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\Role;
use CoreSys\CoreBundle\Form\RoleType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use CoreSys\CoreBundle\Manager\RoleManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Role Controller
 * @Rest\RouteResource("Role")
 * @Rest\NamePrefix("api_")
 */
class RoleRestController extends BaseRestController
{

    /**
     * Datatables Role entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Role",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $manager = $this->get( 'core_sys_core.manager.roles');
        $repo = $this->getRepo( 'CoreSysCoreBundle:Role' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables Role entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Role",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
        $manager = $this->get( 'core_sys_core.manager.roles');
        $repo = $this->getRepo( 'CoreSysCoreBundle:Role' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Get a Role entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single Role entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when Role not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for Role" }
     *   },
     *   output="CoreSys\CoreBundle/entity/Role"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( Role $entity )
    {
        return $entity;
    }

    /**
     * Get all Role entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all Role(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<CoreSys\CoreBundle\Entity\Role>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many Role(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many Role(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'CoreSysCoreBundle:Role' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new Role
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new Role",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/RoleType",
     *   output="CoreSys\CoreBundle/Entity/Role"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        $entity = new Role();
        $form   = $this->createForm( RoleType::class, $entity, array( 'method' => $request->getMethod() ) );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $this->persistAndFlush( $entity );

            $this->updateRoleYml();

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Update a(n) Role entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Role",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/RoleType",
     *   output="CoreSys\CoreBundle/Entity/Role"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, Role $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $form = $this->createForm( RoleType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $this->persistAndFlush( $entity );

                $this->updateRoleYml();

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Patch a(n) Role entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Role",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/RoleType",
     *   output="CoreSys\CoreBundle/Entity/Role"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param Role    $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, Role $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) Role
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) Role",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when Role not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request $request
     * @param Role    $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, Role $entity )
    {
        try {
            $checks   = array( 'mandatory', 'getMandatory', 'isMandatory' );
            $continue = TRUE;
            foreach ( $checks as $check ) {
                if ( method_exists( $entity, $check ) ) {
                    $val = $entity->$check();
                    if ( $val === TRUE ) {
                        $continue = FALSE;
                    }
                }
            }
            if ( $continue ) {
                $this->removeAndFlush( $entity );

                $this->updateRoleYml();
            } else {
                return FOSView::create( 'Cannot delete a mandatory Role', Codes::HTTP_METHOD_NOT_ALLOWED );
            }

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new Role
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new Role form to create a new Role",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity = new Role();
        $form   = $this->createForm( RoleType::class, $entity, array( 'method' => 'POST' ) );
        $view   = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:RoleRest:new.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) Role to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for Role to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when Role not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( Role $entity )
    {
        $form = $this->createForm( RoleType::class, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:RoleRest:edit.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Write the Roles YaML file
     */
    protected function updateRoleYml()
    {
        $manager = $this->get( 'core_sys_core.manager.roles' );
        $manager->dumpYml();
    }
}