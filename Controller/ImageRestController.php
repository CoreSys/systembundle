<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\Image;
use CoreSys\CoreBundle\Form\ImageType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use CoreSys\CoreBundle\Manager\ImageManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Image Controller
 * @Rest\RouteResource("Image")
 * @Rest\NamePrefix("api_")
 */
class ImageRestController extends BaseRestController
{

    /**
     * Datatables Image entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Image",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:Image' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables Image entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Image",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:Image' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Get a Image entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single Image entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when Image not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for Image" }
     *   },
     *   output="CoreSys\CoreBundle/entity/Image"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( Image $entity )
    {
        return $entity;
    }

    /**
     * Get all Image entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all Image(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<CoreSys\CoreBundle\Entity\Image>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many Image(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many Image(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'CoreSysCoreBundle:Image' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new Image
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new Image",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/ImageType",
     *   output="CoreSys\CoreBundle/Entity/Image"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        $entity = new Image();
        $form   = $this->createForm( ImageType::class, $entity, array( 'method' => $request->getMethod() ) );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $this->persistAndFlush( $entity );

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Update image positions
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update image positions",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postOrderAction( Request $request )
    {
        $ids  = $request->get( 'ids', array() );
        $repo = $this->getRepo( 'CoreSysCoreBundle:Image' );

        if ( is_array( $ids ) ) {
            $position = 0;
            foreach ( $ids as $id ) {
                $image = $repo->find( $id );
                if ( !empty( $image ) ) {
                    $image->setPosition( $position++ );
                    $this->persist( $image );
                }
            }

            $this->flush();
        }

        return NULL;
    }

    /**
     * Update a(n) Image entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Image",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/ImageType",
     *   output="CoreSys\CoreBundle/Entity/Image"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, Image $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $form = $this->createForm( ImageType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $this->persistAndFlush( $entity );

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Patch a(n) Image entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Image",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/ImageType",
     *   output="CoreSys\CoreBundle/Entity/Image"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param Image   $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, Image $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) Image
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) Image",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when Image not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request $request
     * @param Image   $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, Image $entity )
    {
        try {
            $manager = $this->get( 'core_sys_core.manager.image' );
            $manager->removeImage( $entity );

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new Image
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new Image form to create a new Image",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity = new Image();
        $form   = $this->createForm( ImageType::class, $entity, array( 'method' => 'POST' ) );
        $view   = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:ImageRest:new.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) Image to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for Image to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when Image not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( Image $entity )
    {
        $form = $this->createForm( ImageType::class, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:ImageRest:edit.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }
}