<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\ImageSize;
use CoreSys\CoreBundle\Form\ImageSizeType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use CoreSys\CoreBundle\Manager\ImageSizeManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * ImageSize Controller
 * @Rest\RouteResource("ImageSize")
 * @Rest\NamePrefix("api_")
 */
class ImageSizeRestController extends BaseRestController
{

    /**
     * Datatables ImageSize entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for ImageSize",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'CoreSysCoreBundle:ImageSize' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables ImageSize entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for ImageSize",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
//        $repo = $this->getRepo( 'CoreSysCoreBundle:Image' );
//        foreach( $repo->findAll() as $img ) {
//            $this->removeAndFlush( $img );
//        }

        $repo = $this->getRepo( 'CoreSysCoreBundle:ImageSize' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Get a ImageSize entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single ImageSize entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when ImageSize not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for ImageSize" }
     *   },
     *   output="CoreSys\CoreBundle/entity/ImageSize"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( ImageSize $entity )
    {
        return $entity;
    }

    /**
     * Get all ImageSize entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all ImageSize(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<CoreSys\CoreBundle\Entity\ImageSize>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many ImageSize(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many ImageSize(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'CoreSysCoreBundle:ImageSize' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new ImageSize
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new ImageSize",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/ImageSizeType",
     *   output="CoreSys\CoreBundle/Entity/ImageSize"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        try {
            $entity = new ImageSize();
            $form   = $this->createForm( ImageSizeType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );

            if ( $form->isValid() ) {
                $entity  = $form->getData();
                $manager = $this->getImageSizeManager();
                $manager->addSize( $entity );

//                $this->persistAndFlush( $entity );

                return $entity;
            } else {
                var_dump( $this->getErrorMessages( $form ) );
            }
        } catch ( \Exception $e ) {
            echo $e->getMessage();
            exit;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * @return ImageSizeManager
     */
    public function getImageSizeManager()
    {
        return $this->get( 'core_sys_core.manager.image_size' );
    }

    /**
     * Update a(n) ImageSize entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) ImageSize",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/ImageSizeType",
     *   output="CoreSys\CoreBundle/Entity/ImageSize"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, ImageSize $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $form = $this->createForm( ImageSizeType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $entity  = $form->getData();
                $manager = $this->getImageSizeManager();
                $manager->updateSize( $entity );

//                $this->persistAndFlush( $entity );

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Patch a(n) ImageSize entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) ImageSize",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="CoreSys\CoreBundle/Form/ImageSizeType",
     *   output="CoreSys\CoreBundle/Entity/ImageSize"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request   $request
     * @param ImageSize $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, ImageSize $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) ImageSize
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) ImageSize",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when ImageSize not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request   $request
     * @param ImageSize $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, ImageSize $entity )
    {
        try {
            $manager = $this->getImageSizeManager();
            $manager->removeSize( $entity );
            $this->removeAndFlush( $entity );

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new ImageSize
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new ImageSize form to create a new ImageSize",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity = new ImageSize();
        $form   = $this->createForm( ImageSizeType::class, $entity, array( 'method' => 'POST' ) );
        $view   = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:ImageSizeRest:new.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) ImageSize to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for ImageSize to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when ImageSize not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( ImageSize $entity )
    {
        $form = $this->createForm( ImageSizeType::class, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "CoreSysCoreBundle:ImageSizeRest:edit.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }
}