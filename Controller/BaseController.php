<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\Configuration;
use CoreSys\CoreBundle\Entity\User;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class BaseController
 * @package CoreSys\CoreBundle\Controller
 */
class BaseController extends Controller
{

    /**
     * @var array
     */
    protected $folders = array();

    /**
     * @var Configuration
     */
    private $config;

    /**
     * @var Session
     */
    private $session;

    /**
     * @return object|Session
     */
    public function getSession()
    {
        if ( !empty( $this->session ) ) {
            return $this->session;
        }

        return $this->session = $this->get( 'session' );
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        $request = $this->getRequest();
        $baseUrl = $request->getScheme() . '://'
                   . $request->getHttpHost()
                   . $request->getBasePath();

        return $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    public function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $name
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepo( $name )
    {
        return $this->getEntityManager()->getRepository( $name );
    }

    /**
     * @param $name
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository( $name )
    {
        return $this->getRepo( $name );
    }

    /**
     * @return mixed
     */
    public function getAppFolder()
    {
        return $this->get( 'kernel' )->getRootDir();
    }

    /**
     * @return string
     */
    public function getRootFolder()
    {
        return implode( DIRECTORY_SEPARATOR, array(
            $this->getAppFolder(),
            '..'
        ) );
    }

    /**
     * @return string
     */
    public function getWebFolder()
    {
        return implode( DIRECTORY_SEPARATOR, array(
            $this->getRootFolder(),
            'web'
        ) );
    }

    /**
     * @return string
     */
    public function getConfigurationFolder()
    {
        return implode( DIRECTORY_SEPARATOR, array(
            $this->getAppFolder(),
            'config'
        ) );
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->getConfiguration();
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        if ( !empty( $this->config ) ) {
            return $this->config;
        }

        return $this->config = $this->getRepo( 'CoreSysCoreBundle:Configuration' )->getConfiguration();
    }

    /**
     * @param $folder
     *
     * @return mixed
     */
    public function verifyFolder( $folder )
    {
        if ( !is_dir( $folder ) ) {
            mkdir( $folder, 0777 );
            chmod( $folder, 0777 );
        }

        return $folder;
    }

    /**
     * @return mixed
     */
    public function getMediaFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getRootFolder(),
                'media'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getPublicMediaFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getWebFolder(),
                'media'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getMediaImagesFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getMediaFolder(),
                'images'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getPublicMediaImagesFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getPublicMediaFolder(),
                'images'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getMediaUploadFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getMediaFolder(),
                'upload'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getPublicMediaUploadFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getPublicMediaFolder(),
                'upload'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getPublicMediaTempFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getPublicMediaFolder(),
                'temp'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getMediaTempFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getMediaFolder(),
                'temp'
            ) )
        );
    }

    /**
     * @param $entity
     *
     * @return $this
     */
    public function persist( $entity )
    {
        $methods = array( 'prepersist', 'Prepersist', 'PrePersist', 'prePersist' );
        foreach ( $methods as $method ) {
            if ( method_exists( $entity, $method ) ) {
                $entity->$method();
                break;
            }
        }
        $this->getEntityManager()->persist( $entity );

        return $this;
    }

    /**
     * @param $entity
     *
     * @return BaseController
     */
    public function persistAndFlush( $entity )
    {
        $this->persist( $entity );

        return $this->flush();
    }

    /**
     * @return $this
     */
    public function flush()
    {
        $this->getEntityManager()->flush();

        return $this;
    }

    /**
     * @param $entity
     *
     * @return $this
     */
    public function remove( $entity )
    {
        $methods = array( 'delete', 'Delete', 'predelete', 'Predelete', 'PreDelete', 'preDelete',
                          'remove', 'Remove', 'preremove', 'Preremove', 'PreRemove', 'preRemove' );
        foreach ( $methods as $method ) {
            if ( method_exists( $entity, $method ) ) {
                $entity->$method();
                break;
            }
        }
        $this->getEntityManager()->remove( $entity );

        return $this;
    }

    /**
     * @param $entity
     *
     * @return BaseController
     */
    public function removeAndFlush( $entity )
    {
        $this->remove( $entity );

        return $this->flush();
    }

    /**
     * @return string
     */
    public function testFunction()
    {
        return 'BaseController Found';
    }

    /**
     * @param string $type
     * @param null   $msg
     *
     * @return $this
     */
    public function setFlashMessage( $type = 'success', $msg = NULL )
    {
        $this->getSession()->getFlashBag()->add( $type === 'danger' ? 'error' : $type, $msg );

        return $this;
    }

    /**
     * @param $msg
     *
     * @return BaseController
     */
    public function setFlashSuccess( $msg ) { return $this->setFlashMessage( 'success', $msg ); }

    /**
     * @param $msg
     *
     * @return BaseController
     */
    public function setFlashWarning( $msg ) { return $this->setFlashMessage( 'warning', $msg ); }

    /**
     * @param $msg
     *
     * @return BaseController
     */
    public function setFlashInfo( $msg ) { return $this->setFlashMessage( 'info', $msg ); }

    /**
     * @param $msg
     *
     * @return BaseController
     */
    public function setFlashDanger( $msg ) { return $this->setFlashMessage( 'danger', $msg ); }

    /**
     * @param $msg
     *
     * @return BaseController
     */
    public function setFlashError( $msg ) { return $this->setFlashMessage( 'danger', $msg ); }

    /**
     * @return Request
     */
    public function getRequest()
    {
        $requestStack = $this->get( 'request_stack' );

        return $requestStack->getCurrentRequest();
    }

    /**
     * @param        $data
     * @param string $type
     * @param bool   $null
     * @param bool   $depthChecks
     * @param null   $groups
     *
     * @return mixed
     */
    public function serializeData( &$data, $type = 'json', $null = TRUE, $depthChecks = TRUE, $groups = NULL )
    {
        $serializer = $this->get( 'jms_serializer' );
        $context    = SerializationContext::create()->setSerializeNull( $null );
        if ( $depthChecks === TRUE ) {
            $context->enableMaxDepthChecks();
        }
        if ( !empty( $groups ) && ( is_array( $groups ) && count( $groups ) > 0 ) ) {
            $context->setGroups( $groups );
        }

        return $serializer->serialize( $data, $type, $context );
    }

    /**
     * @param string $type
     * @param null   $data
     * @param array  $options
     *
     * @return \Symfony\Component\Form\Form
     */
    public function createForm( $type, $data = NULL, array $options = array() )
    {
        return parent::createForm( $type, $data, $options );
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route         The name of the route
     * @param mixed  $parameters    An array of parameters
     * @param int    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateUrl( $route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH )
    {
        return parent::generateUrl( $route, $parameters, $referenceType );
    }

    /**
     * @param string $name
     *
     * @return object
     */
    public function get( $name )
    {
        return parent::get( $name );
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return parent::getUser();
    }

    /**
     * @param mixed $attributes
     * @param null  $object
     *
     * @return bool
     */
    public function isGranted( $attributes, $object = NULL )
    {
        return parent::isGranted( $attributes, $object );
    }

    /**
     * @param string $view
     * @param array  $parameters
     *
     * @return string
     */
    public function renderView( $view, array $parameters = array() )
    {
        return parent::renderView( $view, $parameters );
    }

    /**
     * @return mixed
     */
    public function getPublicMediaChunkFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getPublicMediaFolder(),
                'chunks'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getPublicMediaChunksFolder()
    {
        return $this->getPublicMediaChunkFolder();
    }

    /**
     * @return mixed
     */
    public function getMediaChunkFolder()
    {
        return $this->verifyFolder(
            implode( DIRECTORY_SEPARATOR, array(
                $this->getMediaFolder(),
                'chunks'
            ) )
        );
    }

    /**
     * @return mixed
     */
    public function getMediaChunksFolder()
    {
        return $this->getMediaChunkFolder();
    }

    /**
     * @param $val
     *
     * @return bool
     */
    public function toBool( $val )
    {
        return $val === TRUE || $val === 1 || $val === '1' || $val === 'true' || $val === 'TRUE' || $val == 'True';
    }
}
