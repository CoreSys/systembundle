<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\Image;
use CoreSys\CoreBundle\Form\ImageType;
use CoreSys\CoreBundle\Manager\ImageManager;
use CoreSys\CoreBundle\Manager\ImageSizeManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MediaController
 * @package CoreSys\CoreBundle\Controller
 * @Route("/media", options={"expose"=true})
 */
class MediaController extends BaseController
{

    /**
     * @var ImageManager
     */
    private $manager;

    /**
     * @var ImageSizeManager
     */
    private $sizeManager;

    /**
     * @Route("/image/test")
     * @Template
     */
    public function testImagesAction()
    {
        $images = $this->getRepo( 'CoreSysCoreBundle:Image' )->findAll();

        return array( 'images' => $images );
    }

    /**
     * @return ImageSizeManager|object
     */
    public function getSizeManager()
    {
        if ( isset( $this->sizeManager ) ) {
            return $this->sizeManager;
        }

        return $this->sizeManager = $this->get( 'core_sys_core.manager.image_size' );
    }

    /**
     * @return ImageManager
     */
    public function getImageManager()
    {
        if ( isset( $this->maanager ) ) {
            return $this->manager;
        }

        return $this->manager = $this->get( 'core_sys_core.manager.image' );
    }

    /**
     * @Route("/image/upload/chunk", name="media_image_upload_chunk")
     * @Method({"POST"})
     * @Template()
     */
    public function uploadImageChunkAction()
    {
        $manager = $this->getImageManager();
        $manager->uploadChunk();
        exit;
    }

    /**
     * @Route("/image/upload/raw", name="media_image_upload_raw")
     * @Method({"POST"})
     * @Template()
     */
    public function uploadImageRawAction()
    {
        $manager = $this->getImageManager();
        $image   = $manager->uploadImage();
        if ( !$image instanceof Image ) {
            throw new \Exception( 'Unable to process image' );
        }

        $image->setInProgress( true );
        $this->persistAndFlush( $image );

        header( 'Content-Type: application/json' );
        echo $this->serializeData( $image, 'json' );

        exit;
    }

    /**
     * @Route("/image/upload", name="media_image_upload")
     * @Method({"POST"})
     * @Template()
     * @param Request $request
     */
    public function uploadImageAction( Request $request )
    {
        $manager = $this->getImageManager();
        $image   = new Image();
        $form    = $this->createForm( ImageType::class, $image );
        if ( $request->isMethod( 'post' ) ) {
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $file  = $image->getFile();
                $image = $manager->processUploadedFile( $file, $image, TRUE );
                http_response_code( 201 );
                exit;
            }
        }

        http_response_code( 400 );
        exit;
    }

    /**
     * @Route("/image/smallest/{slug}", name="media_image_get_smallest", defaults={"slug"=null})
     * @param $slug
     */
    public function smallestImageAction( $slug )
    {
        $manager = $this->getSizeManager();
        $size    = $manager->getSmallestSize();

        if ( empty( $size ) ) {
            $size = NULL;
        } else {
            $size = $size->getName();
        }

        $this->imageAction( $size, $slug );
        exit;
    }

    /**
     * Get an image from the system
     *
     * @param null $size
     * @param null $slug
     * @Route("/image/{size}/{slug}", name="media_image_get", defaults={"size"=null, "slug"=null})
     * @Template()
     */
    public function imageAction( $size = NULL, $slug = NULL )
    {
        if ( empty( $slug ) && empty( $size ) ) {
            http_response_code( 404 );
            exit;
        } else if ( empty( $slug ) && !empty( $size ) ) {
            $slug = $size;
            $size = NULL;
        }

        if ( $size !== NULL ) {
            $size = $this->getRepo( 'CoreSysCoreBundle:ImageSize' )->locateSize( $size );
            if ( empty( $size ) ) {
                $size = NULL;
            }
        }

        $image = $this->getRepo( 'CoreSysCoreBundle:Image' )->locateImage( $slug );
        if ( empty( $image ) || empty( $image->getFilename() ) ) {
            http_response_code( 404 );
            exit;
        }

        $manager = $this->getImageManager();
        $manager->showImage( $image, $size, TRUE );

        exit;
    }

    /**
     * @Route("/image/testUpload")
     * @Template()
     */
    public function testUploadImageAction()
    {
        $image = new Image();
        $form  = $this->createForm( ImageType::class, $image );

        return array( 'form' => $form->createView() );
    }
}
