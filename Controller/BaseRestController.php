<?php

namespace CoreSys\CoreBundle\Controller;

use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Annotations\AnnotationReader;

/**
 * Class BaseRestController
 * @package CoreSys\CoreBundle\Controller
 */
class BaseRestController extends BaseController
{

    /**
     * @var string
     */
    protected $repoName;

    /**
     * @var string
     */
    protected $resource = 'core';

    /**
     * @var string|null
     */
    protected $formClass = NULL;

    /**
     * @var AnnotationReader
     */
    protected $reader;

    public function getReader()
    {
        if ( !empty( $this->reader ) ) {
            return $this->reader;
        }

        return $this->reader = new AnnotationReader();
    }

    /**
     * Get datatables results
     *
     * @param Request $request
     * @param         $repo
     *
     * @return mixed
     */
    public function processDatatables( Request $request, $repo )
    {
        $rc      = new \ReflectionClass( $repo->getClassName() );
        $start   = intval( $request->get( 'start' ) );
        $limit   = intval( $request->get( 'length', 25 ) );
        $columns = $request->get( 'columns' );
        $search  = $request->get( 'search' );
        $order   = $request->get( 'order' );
        $draw    = $request->get( 'draw' );

        $count = $repo->createQueryBuilder( 'e' )
                      ->select( 'COUNT(e.id)' )
                      ->getQuery()
                      ->getSingleScalarResult();

        $filteredCount = $count;
        $extra         = array();
        $query         = $repo->createQueryBuilder( 'e' );

        if ( is_array( $search ) && isset( $search[ 'value' ] ) && !empty( $search[ 'value' ] ) ) {
            $extra[]          = 'Search is an array and has a searchable value';
            $searchQueryCount = $repo->createQueryBuilder( 'e' )->select( 'COUNT(e.id)' );
            if ( is_array( $columns ) ) {
                foreach ( $columns as $idx => $columnData ) {
                    if ( TRUE === $this->toBool( $columnData[ 'searchable' ] ) ) {
                        $data = isset( $columnData[ 'data' ] ) ? $columnData[ 'data' ] : NULL;
                        if ( !empty( $data ) ) {
                            $relational = $this->isPropertyRelational( $rc, $data );
                            if ( $relational === FALSE ) {
                                // non relational, so we can continue with this as normal
                                $extra[] = 'Adding e.' . $data . ' : ' . $search[ 'value' ];
                                $searchQueryCount->orWhere( 'e.' . $data . ' LIKE :search' )
                                                 ->setParameter( 'search', '%' . $search[ 'value' ] . '%' );
                                $query->orWhere( 'e.' . $data . ' LIKE :search' )
                                      ->setParameter( 'search', '%' . $search[ 'value' ] . '%' );
                            } else {
                                // this is a relational column, we will require adding join columns
                                $extra[] = 'e.' . $data . ' is a relational column (' . $relational[ 'type' ] . ')';
                                if ( empty( $relational[ 'columns' ] ) ) {
                                    $extra[] = 'e.' . $data . ' has no exposed columns';
                                } else {
                                    // add the join table to the query
                                    $joinId  = $relational[ 'id' ];
                                    $extra[] = 'Joining table e.' . $data . ' as ' . $joinId;
                                    $searchQueryCount->leftJoin( 'e.' . $data, $joinId );
                                    $query->leftJoin( 'e.' . $data, $joinId );
                                    foreach ( $relational[ 'columns' ] as $rcol ) {
                                        $extra[] = 'Adding ' . $joinId . '.' . $rcol . ' : ' . $search[ 'value' ];
                                        $searchQueryCount->orWhere( $joinId . '.' . $rcol . ' LIKE :search' )
                                                         ->setParameter( 'search', '%' . $search[ 'value' ] . '%' );
                                        $query->orWhere( $joinId . '.' . $rcol . ' LIKE :search' )
                                              ->setParameter( 'search', '%' . $search[ 'value' ] . '%' );
                                    }
                                }
                            }
                        }
                    }
                }
//                echo '<hr>';
//                echo '<pre>';
//                var_dump( $extra );
//                echo '</pre>';
//                exit;
            }
            $filteredCount = $searchQueryCount->getQuery()->getSingleScalarResult();
        }

        if ( is_array( $order ) ) {
            foreach ( $order as $o ) {
                $column = $columns[ $o[ 'column' ] ];
                $column = !empty( $column ) && !empty( $column[ 'data' ] ) ? $column[ 'data' ] : NULL;
                if ( !empty( $column ) ) {
                    $dir     = $o[ 'dir' ];
                    $extra[] = 'Order By: ' . $column . ':' . $dir;
                    $query->addOrderBy( 'e.' . $column, $dir );
                }
            }
        }

        $query->setFirstResult( $start );
        if ( intval( $limit ) > 0 ) {
            $query->setMaxResults( $limit );
        }

        $data = $query->getQuery()->getResult();

        // add checkable column
        if ( $columns[ 0 ][ 'data' ] === 'checkable' ) {
            foreach ( $data as $idx => $item ) {
                $item                = json_decode( $this->serializeData( $item ), TRUE );
                $item[ 'checkable' ] = $this->renderView( 'CoreSysCoreBundle:Inputs:checkbox.html.twig', array( 'attrs' => array(
                    'class'   => 'table-master-check',
                    'data-id' => isset( $item[ 'id' ] ) ? $item[ 'id' ] : NULL
                ) ) );
                $data[ $idx ]        = $item;
            }
        }

        $manager = $this->get( 'core_sys_core.manager.datatables' );

        $data = array(
            'extra'           => $extra,
            'draw'            => intval( $draw ),
            'start'           => $start,
            'length'          => $limit,
            'data'            => $data,
            'recordsTotal'    => $count,
            'recordsFiltered' => $filteredCount
        );

        header( 'Content-Type: application/json' );
        echo $this->serializeData( $data, 'json', TRUE, TRUE );
        exit;
    }

    /**
     * @param Request $request
     * @param         $repo
     * @param array   $requirements
     *
     * @return mixed
     */
    public function processDatatablesWithRequirements( Request $request, $repo, array $requirements = array() )
    {
        if ( !is_array( $requirements ) || count( $requirements ) === 0 ) {
            return $this->processDatatables( $request, $repo );
        }

        $start   = intval( $request->get( 'start' ) );
        $limit   = intval( $request->get( 'length', 25 ) );
        $columns = $request->get( 'columns' );
        $search  = $request->get( 'search' );
        $order   = $request->get( 'order' );
        $draw    = $request->get( 'draw' );

        $count = $repo->createQueryBuilder( 'e' )
                      ->select( 'COUNT(e.id)' );
        foreach ( $requirements as $k => $v ) {
            $count->andWhere( 'e.' . $k . ' = :' . $k )
                  ->setParameter( $k, $v );
        }
        $count = $count->getQuery()
                       ->getSingleScalarResult();

        $filteredCount = $count;
        $extra         = array();
        $query         = $repo->createQueryBuilder( 'e' );

        foreach ( $requirements as $k => $v ) {
            $query->andWhere( 'e.' . $k . ' = :' . $k )
                  ->setParameter( $k, $v );
        }

        if ( is_array( $search ) && isset( $search[ 'value' ] ) && !empty( $search[ 'value' ] ) ) {
            $extra[]          = 'Search is an array and has a searchable value';
            $searchQueryCount = $repo->createQueryBuilder( 'e' )->select( 'COUNT(e.id)' );
            if ( is_array( $columns ) ) {
                foreach ( $columns as $idx => $columnData ) {
                    if ( TRUE === $this->toBool( $columnData[ 'searchable' ] ) ) {
                        $data = isset( $columnData[ 'data' ] ) ? $columnData[ 'data' ] : NULL;
                        if ( !empty( $data ) ) {
                            $extra[] = 'Adding e.' . $data . ' : ' . $search[ 'value' ];
                            $searchQueryCount->orWhere( 'e.' . $data . ' LIKE :search' )
                                             ->setParameter( 'search', '%' . $search[ 'value' ] . '%' );
                            $query->orWhere( 'e.' . $data . ' LIKE :search' )
                                  ->setParameter( 'search', '%' . $search[ 'value' ] . '%' );
                        }
                    }
                }
            }
            $filteredCount = $searchQueryCount->getQuery()->getSingleScalarResult();
        }

        if ( is_array( $order ) ) {
            foreach ( $order as $o ) {
                $column = $columns[ $o[ 'column' ] ];
                $column = !empty( $column ) && !empty( $column[ 'data' ] ) ? $column[ 'data' ] : NULL;
                if ( !empty( $column ) ) {
                    $dir     = $o[ 'dir' ];
                    $extra[] = 'Order By: ' . $column . ':' . $dir;
                    $query->addOrderBy( 'e.' . $column, $dir );
                }
            }
        }

        $query->setFirstResult( $start );
        if ( intval( $limit ) > 0 ) {
            $query->setMaxResults( $limit );
        }

        $data = $query->getQuery()->getResult();

        // add checkable column
        if ( $columns[ 0 ][ 'data' ] === 'checkable' ) {
            foreach ( $data as $idx => $item ) {
                $item                = json_decode( $this->serializeData( $item ), TRUE );
                $item[ 'checkable' ] = $this->renderView( 'CoreSysCoreBundle:Inputs:checkbox.html.twig', array( 'attrs' => array(
                    'class'   => 'table-master-check',
                    'data-id' => isset( $item[ 'id' ] ) ? $item[ 'id' ] : NULL
                ) ) );
                $data[ $idx ]        = $item;
            }
        }

        $manager = $this->get( 'core_sys_core.manager.datatables' );

        $data = array(
            'extra'           => $extra,
            'draw'            => intval( $draw ),
            'start'           => $start,
            'length'          => $limit,
            'data'            => $data,
            'recordsTotal'    => $count,
            'recordsFiltered' => $filteredCount
        );

        header( 'Content-Type: application/json' );
        echo $this->serializeData( $data, 'json', TRUE, TRUE );
        exit;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getEntityRepo()
    {
        return $this->getRepo( $this->repoName );
    }

    /**
     * @return string
     */
    public function getEntityClassName()
    {
        return $this->getRepo( $this->repoName )->getClassName();
    }

    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        $className = $this->getEntityClassName();

        return new $className();
    }

    /**
     * @param null  $data
     * @param null  $statusCode
     * @param array $headers
     *
     * @return View
     */
    public function view( $data = NULL, $statusCode = NULL, array $headers = array() )
    {
        return View::create( $data, $statusCode, $headers );
    }

    /**
     * @param       $url
     * @param int   $statusCode
     * @param array $headers
     *
     * @return View
     */
    public function redirectView( $url, $statusCode = Codes::HTTP_FOUND, array $headers = array() )
    {
        return View::createRedirect( $url, $statusCode, $headers );
    }

    /**
     * @param       $route
     * @param array $parameters
     * @param int   $statusCode
     * @param array $headers
     *
     * @return View
     */
    public function routeRedirectView( $route, array $parameters = array(), $statusCode = Codes::HTTP_CREATED, array $headers = array() )
    {
        return View::createRouteRedirect( $route, $parameters, $statusCode, $headers );
    }

    /**
     * @param View $view
     *
     * @return mixed
     */
    public function handleView( View $view )
    {
        return $this->get( 'fos_rest.view_handler' )->handle( $view );
    }

    /**
     * @param     $data
     * @param int $code
     *
     * @return mixed
     */
    public function doView( $data, $code = 200 )
    {
        $view = $this->view( $data, $code );

        return $this->handleView( $view );
    }

    /**
     * @param null  $type
     * @param null  $data
     * @param array $options
     *
     * @return mixed
     */
    public function createForm( $type = NULL, $data = NULL, array $options = array() )
    {
        $options[ 'csrf_protection' ] = FALSE;

        $form = $this->get( 'form.factory' )->createNamed(
            NULL,
            $type,
            $data,
            $options
        );

        return $form;
    }

    /**
     * @param Request $request
     * @param Form    $form
     */
    public function removeExtraFields( Request &$request, Form $form )
    {
        $this->getAllData( $request );
        $data     = $request->request->all();
        $children = $form->all();
        $data     = array_intersect_key( $data, $children );
        $request->request->replace( $data );
    }

    protected function getAllData( Request &$request )
    {
        $content = $request->getContent();
        if ( !empty( $content ) ) {
            try {
                $json = json_decode( $content, TRUE );
            } catch ( \Exception $e ) {
                $json = NULL;
            }

            if ( !empty( $json ) ) {
                $data = $request->request->all();
                foreach ( $json as $key => $value ) {
                    $data[ str_replace( '[]', '', $key ) ] = $value;
                }
                $request->request->replace( $data );
            }
        }
    }

    protected function getErrorMessages( \Symfony\Component\Form\Form $form )
    {
        $errors = array();

        if ( $form->count() > 0 ) {
            foreach ( $form->all() as $child ) {
                if ( !$child->isValid() ) {
                    $errors[ $child->getName() ] = $this->getErrorMessages( $child );
                }
            }
        } else {
            foreach ( $form->getErrors() as $k => $v ) {
                $errors[] = $v->getMessage();
            }
        }

        return $errors;
    }

    private function isPropertyRelational( \ReflectionClass &$rc, $propertyName )
    {
        if ( $rc->hasProperty( $propertyName ) ) {
            $annotations = array(
                'Doctrine\ORM\Mapping\OneToOne',
                'Doctrine\ORM\Mapping\OneToMany',
                'Doctrine\ORM\Mapping\ManyToOne',
                'Doctrine\ORM\Mapping\ManyToMany'
            );

            $property = $rc->getProperty( $propertyName );
            $reader   = $this->getReader();
            foreach ( $annotations as $aname ) {
                $annotation = $reader->getPropertyAnnotation( $property, $aname );
                if ( !empty( $annotation ) ) {
                    $targetEntity = $annotation->targetEntity;
                    if ( !preg_match( '/\\\/', $targetEntity ) ) {
                        // this entity is local to the main entity
                        $temp      = $rc->getName();
                        $tempParts = explode( '\\', $temp );
                        array_pop( $tempParts );
                        $temp         = implode( '\\', $tempParts ) . '\\' . $targetEntity;
                        $targetEntity = $temp;
                    }
                    $result = array(
                        'annotation'   => $annotation,
                        'type'         => $aname,
                        'id'           => 't' . $propertyName,
                        'targetEntity' => $targetEntity
                    );

                    // need to determine the columns we can search on in the join table
                    $columns       = array();
                    $subRc         = new \ReflectionClass( $targetEntity );
                    $subProperties = $subRc->getProperties();
                    foreach ( $subProperties as $prop ) {
                        $test = FALSE;
                        foreach ( $annotations as $anname ) {
                            $testann = $reader->getPropertyAnnotation( $prop, $anname );
                            if ( !empty( $testann ) ) {
                                $test = TRUE;
                                break;
                            }
                        }
                        if ( $test === FALSE ) {
                            $propAnnotation = $reader->getPropertyAnnotation( $prop, 'JMS\Serializer\Annotation\Expose' );
                            $colAnnotation  = $reader->getPropertyAnnotation( $prop, 'Doctrine\ORM\Mapping\Column' );
                            $cont           = TRUE;
                            $docBlock       = $prop->getDocComment();
                            $docBlock       = preg_split( '/[\r\n]/', $docBlock );
                            $notPermitted   = array( 'array', 'datetime', 'object', 'bool' );
                            foreach ( $docBlock as $line ) {
                                if ( strstr( $line, '@var' ) ) {
                                    $pos  = strpos( $line, '@var' );
                                    $line = strtolower( trim( substr( $line, $pos + 4 ) ) );
                                    foreach ( $notPermitted as $nono ) {
                                        if ( strstr( $line, $nono ) ) {
                                            $cont = FALSE;
                                            break;
                                        }
                                    }
                                }
                            }
                            if ( $propAnnotation && $colAnnotation && $cont ) {
                                $columns[] = $prop->getName();
                            }
                        } else {
                            // @todo make this recursive
                        }
                    }

                    $result[ 'columns' ] = $columns;

                    return $result;
                }
            }
        }

        return FALSE;
    }
}