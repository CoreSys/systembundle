Routing
=======

Add the following into your Symfony Routing

```
// app/config/routing.yml
core_sys_core_api:
    type: rest
    resource: "@CoreSysCoreBundle/Resources/config/Routing/rest.yml"

_liip_imagine:
    resource: "@LiipImagineBundle/Resources/config/routing.xml"

NelmioApiDocBundle:
    resource: "@NelmioApiDocBundle/Resources/config/routing.yml"
    prefix:   /api/doc

fos_js_routing:
    resource: "@FOSJsRoutingBundle/Resources/config/routing/routing.xml"

fos_user:
    resource: "@FOSUserBundle/Resources/config/routing/all.xml"

hwi_oauth_redirect:
    resource: "@HWIOAuthBundle/Resources/config/routing/redirect.xml"
    prefix:   /connect

hwi_oauth_login:
    resource: "@HWIOAuthBundle/Resources/config/routing/login.xml"
    prefix:   /connect

github_login:
    path: /connect/login/check-github

facebook_login:
    path: /connect/login/check-facebook

twitter_login:
    path: /connect/login/check-twitter

google_login:
    path: /connect/login/check-google

instagram_login:
    path: /connect/login/instagram

login_facebook:
    path: /connect/facebook

login_twitter:
    path: /connect/twitter

login_google:
    path: /connect/google

login_instagram:
    path: /connect/instagram
```