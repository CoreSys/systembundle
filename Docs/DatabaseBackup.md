Database Backup
===============

The database can be automatically backed up to a file and/or git
There are 2 ways that this can be done

First, backups will ONLY be created at most once an hour.

Go to [SiteRoot]/coreCron/databaseBackup
Or add the above link to your cron manager to run once an hour