Core Setup
==========

Required information for setting up CoreSys CoreBundle

- [Configuration Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Configuration.md)
- [Routing Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Routing.md)
- [Security Documentation](https://bitbucket.org/CoreSys/corebundle/src/master/Docs/Security.md)

### AppKernel
```
// app/AppKernel.php
$bundles = [
    ...
    new Symfony\Bundle\AsseticBundle\AsseticBundle(),
    new Liip\ImagineBundle\LiipImagineBundle(),
    new JMS\SerializerBundle\JMSSerializerBundle(),
    new JMS\AopBundle\JMSAopBundle(),
    new JMS\DiExtraBundle\JMSDiExtraBundle($this),
    new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
    new Nelmio\CorsBundle\NelmioCorsBundle(),
    new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
    new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
    new FOS\UserBundle\FOSUserBundle(),
    new FOS\RestBundle\FOSRestBundle(),
    new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
    new JDecool\Bundle\ImageWorkshopBundle\ImageWorkshopBundle(),
    new CoreSys\CoreBundle\CoreSysCoreBundle(),
    ...
];

if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
    ...
    $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
}
```

### Load initial Roles, Access, and Admin Users
```
php bin/console doctrine:fixtures:load
```

### Update the api access host
- Log into admin
- Navigate to /admin/configuration/access
- Click edit for access with name ^/api
- Update the host name 
- Click save

### Update the admin access role
- Log into admin
- Navigate to /admin/configuration/access
- Click edit for access with name ^/admin
- Update the role to at least "Admin"
- Click save

If Running a fresh version of Symfony
=====================================
On a fresh copy of Symfony, run the following command.
```
bin/console coresys:setup
```
This will copy files required for CoreSys\CoreBundle
- /app/config/access.yml // adds base access rules
- /app/config/roles.yml // adds base roles
- /app/config/config.yml // sets up required configuration
- /app/config/security.yml // adds required security
- /update // quick update command to clear cache and install assets
- /updateDb // quick update (calls ./update) and updates the database
- /updateProd // similar to update, but clears production cache