Core Configuration
==================

### Sample Config File

```
// /app/config/config.yml
imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en

framework:
    #esi:             ~
    #translator:      { fallbacks: ["%locale%"] }
    translator:	~
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale: "%locale%"
    trusted_hosts: ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form_themes:
      - CoreSysCoreBundle:Form:bootstrap_4_layout.html.twig

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        mappings:
            gedmo_translatable:
                type: annotation
                prefix: Gedmo\Translatable\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity"
                alias: GedmoTranslatable # (optional) it will default to the name set for the mapping
                is_bundle: false
            gedmo_translator:
                type: annotation
                prefix: Gedmo\Translator\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translator/Entity"
                alias: GedmoTranslator # (optional) it will default to the name set for the mapping
                is_bundle: false
            gedmo_loggable:
                type: annotation
                prefix: Gedmo\Loggable\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/Entity"
                alias: GedmoLoggable # (optional) it will default to the name set for the mappingmapping
                is_bundle: false
            gedmo_tree:
                type: annotation
                prefix: Gedmo\Tree\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Tree/Entity"
                alias: GedmoTree # (optional) it will default to the name set for the mapping
                is_bundle: false

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

assetic:
    debug:          '%kernel.debug%'
    use_controller: '%kernel.debug%'
    filters:
        cssrewrite:  ~

stof_doctrine_extensions:
    default_locale: en
    translation_fallback: true
    orm:
      default:
        tree: true
        timestampable: true
        sluggable: true

liip_imagine:
    resolvers:
        default:
            web_path: ~
    filter_sets:
        cache: ~
        my_thumb:
            quality: 7
            filters:
                thumbnail: { size: [120,90], mode: outbound }

jms_serializer:
    handlers:
        datetime:
            default_format:     "c"
            default_timezone:   "America/Vancouver"
    metadata:
        auto_detection: true
    visitors:
        json:
            options: 0

jms_di_extra:
    locations:
        all_bundles: false
        bundles: [CoreSysCoreBundle]
        directories: ["%kernel.root_dir%/../src"]

nelmio_api_doc: ~

sensio_framework_extra:
  request:  { converters: true }
  view:     { annotations: false }
  router:   { annotations: true }

nelmio_cors:
    defaults:
      allow_credentials: false
      allow_origin: []
      allow_headers: []
      allow_methods: []
      expose_headers: []
      max_age: 0
    paths:
      '^/api/':
        allow_origin: ['*']
        allow_headers: ['*']
        allow_methods: ['POST','PUT','GET','DELETE','PATCH','LINK','UNLINK','HEAD']
        max_age: 3600

fos_user:
    db_driver:      orm
    firewall_name:  main
    user_class:     CoreSys\CoreBundle\Entity\User

fos_rest:
  routing_loader:
    default_format: json
  param_fetcher_listener: true
  body_listener:
    array_normalizer: fos_rest.normalizer.camel_keys
  serializer:
    serialize_null: true
  body_converter:
    enabled: true
  view:
    view_response_listener: true
    serialize_null: true

hwi_oauth:
  firewall_names: [main]
  fosub:
      username_iterations: 30
      properties:
        facebook: facebook_id
#  connect:
#    account_connector: core_sys_core.provider.oauth
  resource_owners:
    facebook:
      type: facebook
      client_id: ""
      client_secret: ""
      scope: "email, public_profile"
      infos_url: "https://graph.facebook.com/me?fields=id,name,email,picture.type(large)"
      paths:
        email: email
      options:
        display: popup
    twitter:
      type: twitter
      client_id: ""
      client_secret: ""
      options:
        include_email: true
    google:
      type: google
      client_id: ""
      client_secret: ""
      scope: "email profile"
    instagram:
      type: instagram
      client_id: ""
      client_secret: ""

core_sys_core:
    db_prefix: ''
    theme:
      name: CoreSys
      asset_path: '/bundles/coresyscore/theme'
      admin_layout: 'CoreSysCoreBundle:Theme:StartUI/admin.html.twig'
      admin_layout_helpers: 'CoreSysCoreBundle:Theme:StartUI/Helper'
      security_layout: 'CoreSysCoreBundle:Theme:CoreSys/security.html.twig'
      security_layout_helpers: 'CoreSysCoreBundle:Theme:CoreSys/Helper'
      front_layout: 'UkriticThemeBundle::public.html.twig'
      front_layout_helpers: 'UkriticThemeBundle::Helper'
      member_layout: 'UkriticThemeBundle::member.html.twig'
      member_layout_helpers: 'UkriticThemeBundle::Helper'

services:
    jms_serializer.cache_naming_strategy:
        class: JMS\Serializer\Naming\IdenticalPropertyNamingStrategy
```