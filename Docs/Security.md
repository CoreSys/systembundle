Core Security
=============

### Sample Security File

```
// /app/config/security.yml
# To get started with security, check out the documentation:
# http://symfony.com/doc/current/book/security.html
security:

    # http://symfony.com/doc/current/book/security.html#where-do-users-come-from-user-providers
    providers:
        fos_userbundle:
            id: fos_user.user_provider.username_email
        in_memory:
            memory: ~

    firewalls:
        # disables authentication for assets and the profiler, adapt it according to your needs
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false

        main:
            pattern: ^/
            form_login:
                provider:             fos_userbundle
                csrf_token_generator: security.csrf.token_manager
                use_forward:          true
                use_referer:          true
            oauth:
                resource_owners:
                    facebook: "/connect/check-facebook"
                    twitter: "/connect/check-twitter"
                    google: "/connect/check-google"
                    instagram: "/connect/check-instagram"
                login_path: /login
                failure_path: /login
                provider: fos_userbundle
                use_forward: true
                use_referer: true
                oauth_user_provider:
                  service: core_sys_core.provider.oauth
            logout:     true
            anonymous:  true

    encoders:
      FOS\UserBundle\Model\UserInterface: bcrypt

#    role_hierarchy:
#      ROLE_USER:              []
#      ROLE_MEMBER:            ROLE_USER
#      ROLE_MEMBER_FREE:       ROLE_MEMBER
#      ROLE_STAFF_WRITER:      ROLE_MEMBER
#      ROLE_MEMBER_PAID:       ROLE_MEMBER_FREE
#      ROLE_ADMIN:             [ROLE_MEMBER, ROLE_MEMBER_FREE, ROLE_MEMBER_PAID]
#      ROLE_ADMIN_MODERATOR:   ROLE_ADMIN
#      ROLE_SUPERADMIN:        ROLE_ADMIN_MODERATOR
#      ROLE_OWNER:             ROLE_SUPERADMIN
#
#    access_control:
#      - { path: ^/login$,                     roles: IS_AUTHENTICATED_ANONYMOUSLY }
#      - { path: ^/register,                   roles: IS_AUTHENTICATED_ANONYMOUSLY }
#      - { path: ^/resetting,                  roles: IS_AUTHENTICATED_ANONYMOUSLY }
#      - { path: ^/admin/configuration,        roles: ROLE_SUPERADMIN }
#      - { path: ^/admin,                      roles: ROLE_ADMIN }
```