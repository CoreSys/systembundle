<?php

namespace CoreSys\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AccessControlPass
 * @package CoreSys\CoreBundle\DependencyInjection\Compiler
 */
class AccessControlPass implements CompilerPassInterface
{

    /**
     * @var
     */
    private $appFolder;

    /**
     * @var
     */
    private $tcontainer;

    /**
     * @param ContainerBuilder $container
     *
     * @throws InvalidArgumentException
     */
    function process( ContainerBuilder $container )
    {
        if ( !$container->hasDefinition( 'security.access_map' ) ) {
            throw new InvalidArgumentException( "Could not locate access_map" );
        }

        $this->tcontainer    = $container;
        $accessMapDefinition = $container->getDefinition( 'security.access_map' );
        $roleDefinition      = $container->getParameter( 'security.role_hierarchy.roles' );

        $role_yml = Yaml::parse( $this->getRolesFileContents() );
        $role_yml = empty( $role_yml ) ? array() : $role_yml;

        $access_yml = Yaml::parse( $this->getAccessControlFileContents() );
        $access_yml = empty( $access_yml ) ? array() : $access_yml;

        /**
         * Fix the role yml
         * Each entry MUST be an array, in config, if a single role is added, it
         * defaults to a string, we must convert this back to an array
         */
        foreach ( $role_yml as $index => $data ) {
            if ( !is_array( $data ) ) {
                $role_yml[ $index ] = array( $data );
            }
        }

        $container->setParameter( 'security.role_hierarchy.roles', $role_yml );

        foreach ( $access_yml as $idx => $access ) {
            $path    = $ip = $roles = $host = $channel = NULL;
            $methods = array();
            foreach ( array( 'path', 'ip', 'roles', 'host', 'channel', 'methods' ) as $key ) {
                if ( isset( $access[ $key ] ) ) {
                    $$key = $access[ $key ];
                }
            }

            $matcher = $this->createRequestMatcher( $path, $host, $methods, $ip );

            $accessMapDefinition->addMethodCall( 'add', array( $matcher, $roles, $channel ) );
        }
    }

    /**
     * @return string
     */
    public function getAppFolder()
    {
        if ( !empty( $this->appFolder ) ) {
            return $this->appFolder;
        }

        $base   = dirname( __DIR__ );
        $folder = $base . DIRECTORY_SEPARATOR . 'app';
        while ( !is_dir( $folder ) ) {
            $base   = dirname( $base );
            $folder = $base . DIRECTORY_SEPARATOR . 'app';
        }

        return $this->appFolder = $folder;
    }

    /**
     * @return string
     */
    public function getConfigFolder()
    {
        return $this->getAppFolder() . DIRECTORY_SEPARATOR . 'config';
    }

    /**
     * @return null|string
     */
    public function getRolesFileContents()
    {
        $file = $this->getConfigFolder() . DIRECTORY_SEPARATOR . 'roles.yml';

        return $this->getConfigFileContents( $file );
    }

    /**
     * @param $file
     *
     * @return null|string
     */
    public function getConfigFileContents( $file )
    {
        if ( is_file( $file ) ) {
            $contents = file_get_contents( $file );
        } else {
            $contents = NULL;
            touch( $file );
        }

        return $contents;
    }

    /**
     * @return null|string
     */
    public function getAccessControlFileContents()
    {
        $file = $this->getConfigFolder() . DIRECTORY_SEPARATOR . 'access.yml';

        return $this->getConfigFileContents( $file );
    }

    /**
     * @param null  $path
     * @param null  $host
     * @param array $methods
     * @param null  $ip
     * @param array $attributes
     *
     * @return Reference
     */
    public function createRequestMatcher( $path = NULL, $host = NULL, $methods = array(), $ip = NULL, array $attributes = array() )
    {
        $serialized = serialize( array( $path, $host, $methods, $ip, $attributes ) );
        $id         = 'security.request_matcher.' . md5( $serialized ) . sha1( $serialized );

        if ( $methods ) {
            $methods = array_map( 'strtoupper', (array)$methods );
        }

        $arguments = array( $path, $host, $methods, $ip, $attributes );
        while ( count( $arguments ) > 0 && !end( $arguments ) ) {
            array_pop( $arguments );
        }

        $this->tcontainer->register( $id, 'Symfony\Component\HttpFoundation\RequestMatcher' )
                         ->setPublic( FALSE )
                         ->setArguments( $arguments );

        return new Reference( $id );
    }
}