<?php

namespace CoreSys\CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root( 'core_sys_core' );

        $rootNode->children()->scalarNode( 'db_prefix' )->end();
        $rootNode->children()->arrayNode( 'theme' )
                 ->children()
                 ->scalarNode( 'name' )->end()
                 ->scalarNode( 'asset_path' )->defaultValue( '/bundles/coresyscore/theme' )->end()
                 ->scalarNode( 'admin_layout' )->end()
                 ->scalarNode( 'admin_layout_helpers' )->end()
                 ->scalarNode( 'front_layout' )->end()
                 ->scalarNode( 'front_layout_helpers' )->end()
                 ->scalarNode( 'security_layout' )->end()
                 ->scalarNode( 'security_layout_helpers' )->end()
                 ->scalarNode( 'member_layout' )->end()
                 ->scalarNode( 'member_layout_helpers' )->end()
                 ->end()
                 ->end();

        return $treeBuilder;
    }
}
