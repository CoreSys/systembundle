<?php

namespace CoreSys\CoreBundle;

use CoreSys\CoreBundle\DependencyInjection\Compiler\AccessControlPass;
use CoreSys\CoreBundle\DependencyInjection\Compiler\ServicePass;
use CoreSys\CoreBundle\DependencyInjection\Compiler\ValidationPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CoreSysCoreBundle
 * @package CoreSys\CoreBundle
 */
class CoreSysCoreBundle extends Bundle
{

    /**
     * @param ContainerBuilder $container
     */
    public function build( ContainerBuilder $container )
    {
        parent::build( $container );
        $container->addCompilerPass( new AccessControlPass() );
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
