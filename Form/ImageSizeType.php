<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageSizeType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name', TextType::class, array( 'required' => TRUE ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'animate', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'maxWidth', NumberType::class, array( 'required' => FALSE ) )
            ->add( 'maxHeight', NumberType::class, array( 'required' => FALSE ) )
            ->add( 'force', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'crop', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'cropLocation', ChoiceType::class, array( 'required' => FALSE, 'choices' => $this->getCropLocationChoices() ) )
            ->add( 'forceFormat', ChoiceType::class, array( 'required' => FALSE, 'choices' => $this->getForcedFormatOptions() ) )
            ->add( 'fallback', EntityType::class, array( 'required' => FALSE, 'class' => 'CoreSys\CoreBundle\Entity\ImageSize' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class'      => 'CoreSys\CoreBundle\Entity\ImageSize',
                                    'csrf_protection' => FALSE
                                ) );
    }

    public function getForcedFormatOptions()
    {
        return array(
            'JPEG' => 'jpg',
            'GIF'  => 'gif',
            'PNG'  => 'png'
        );
    }

    public function getCropLocationChoices()
    {
        $locations = array();
        foreach ( array( 'top', 'center', 'bottom' ) as $vertical ) {
            foreach ( array( 'left', 'center', 'right' ) as $horizontal ) {
                $key               = $vertical . ' ' . $horizontal;
                $val               = ucwords( $key );
                $locations[ $val ] = str_replace( ' ', '-', $key );
            }
        }

        return $locations;
    }
}
