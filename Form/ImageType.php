<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'title', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Image Title' ) ) )
            ->add( 'description', TextareaType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Image Description' ) ) )
            ->add( 'alt', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Image Alt' ) ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'size' => 'mini' ) ) )
            ->add( 'applyWatermark', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Apply Watermark' ) ) )
            ->add( 'inProgress', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Image in Progress?' ) ) )
            ->add( 'attachedTo', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Entity this image is attached to' ) ) )
            ->add( 'attachedToId', IntegerType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Entity ID this image is attached to' ) ) )
            ->add( 'position', IntegerType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'This images position' ) ) )
            ->add( 'file', FileType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The uploaded image file' ) ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Image'
                                ) );
    }
}
