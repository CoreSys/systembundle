<?php

namespace CoreSys\CoreBundle\Form;

use CoreSys\CoreBundle\Controller\BaseController;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class MenuType
 * @package CoreSys\CoreBundle\Form
 * @DI\FormType
 */
class MenuType extends AbstractType
{

    /**
     * @var BaseController
     *
     * @DI\Inject("core_sys_core.controller.base", required=true)
     */
    public $baseController;

    /**
     * MenuType constructor.
     *
     * @param BaseController $baseController
     * @DI\InjectParams({
     *     "baseController" = @DI\Inject("core_sys_core.controller.base")
     * })
     */
//    public function __construct( BaseController $baseController )
//    {
//        $this->baseController = $baseController;
//    }

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'size' => 'mini' ) ) )
            ->add( 'type', ChoiceType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'Menu or Menu-Item?' ), 'choices' => array( 'Menu' => 'menu', 'Item' => 'item' ) ) )
            ->add( 'display', TextType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'Menu|Menu Item display text' ) ) )
            ->add( 'admin', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Is this an admin menu?' ) ) )
            ->add( 'internal', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Is this an internal system menu link?' ) ) )
            ->add( 'urlName', ChoiceType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Choose an internal link' ), 'choices' => $this->getRouteChoices() ) )
            ->add( 'url', UrlType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Enter an external link. Include http(s)://' ) ) )
            ->add( 'icon', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Enter icon class to use. (optional)' ) ) )
            ->add( 'parameters', CollectionType::class, array(
                'required'      => FALSE,
                'allow_add'     => TRUE,
                'allow_delete'  => TRUE,
                'delete_empty'  => TRUE,
                'entry_type'    => TextType::class,
                'entry_options' => array(
                    'attr' => array( 'help' => 'Enter parameter (var=val' )
                )
            ) )
            ->add( 'parent', EntityType::class, array(
                'required'      => FALSE,
                'class'         => 'CoreSysCoreBundle:Menu',
                'choice_label'  => 'display',
                'query_builder' => function ( EntityRepository $er ) {
                    return $er->createQueryBuilder( 'm' )
                              ->where( 'm.parent IS NULL' )
                              ->orderBy( 'm.position', 'asc' );
                }
            ) )
            ->add( 'role', EntityType::class, array(
                'required'     => FALSE,
                'class'        => 'CoreSysCoreBundle:Role',
                'choice_label' => 'name'
            ) );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Menu'
                                ) );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'coresys_corebundle_menu';
    }

    public function getRouteChoices()
    {
        $return     = array();
        $router     = $this->baseController->get( 'router' );
        $collection = $router->getRouteCollection();
        foreach ( $collection->all() as $route => $params ) {
            if ( strstr( $route, 'admin' ) && substr( $route, 0, 4 ) !== 'api_' ) {
                $name             = $params->getPath() . ' --- [' . $route . ']';
                $return[ $name ] = $route;
            }
        }

        return $return;
    }


}
