<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name', TextType::class, array() )
            ->add( 'mandatory', CheckboxType::class, array( 'required' => FALSE ) )
            ->add( 'color', TextType::class, array( 'required' => FALSE, 'attr' => array( 'rel' => 'colorpickersliders', 'data-trigger' => 'focus' ) ) )
            ->add( 'switch', CheckboxType::class, array( 'required' => FALSE ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'parent', EntityType::class, array( 'required' => FALSE, 'class' => 'CoreSysCoreBundle:Role', 'choice_label' => 'name' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Role'
                                ) );
    }
}
