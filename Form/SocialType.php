<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'facebook', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Facebook ID' ) ) )
            ->add( 'facebookShow', CheckboxType::class, array( 'required' => FALSE, 'attr' => Array( 'help' => 'Show Facebook', 'data-size' => 'mini' ) ) )
            ->add( 'twitter', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Twitter ID' ) ) )
            ->add( 'twitterShow', CheckboxType::class, array( 'required' => FALSE, 'attr' => Array( 'help' => 'Show Twitter', 'data-size' => 'mini' ) ) )
            ->add( 'ebay', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'EBay ID' ) ) )
            ->add( 'ebayShow', CheckboxType::class, array( 'required' => FALSE, 'attr' => Array( 'help' => 'Show Ebay', 'data-size' => 'mini' ) ) )
            ->add( 'instagram', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Instagram ID' ) ) )
            ->add( 'instagramShow', CheckboxType::class, array( 'required' => FALSE, 'attr' => Array( 'help' => 'Show Instagram', 'data-size' => 'mini' ) ) )
            ->add( 'youtube', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'YouTube ID' ) ) )
            ->add( 'youtubeShow', CheckboxType::class, array( 'required' => FALSE, 'attr' => Array( 'help' => 'Show YouTube', 'data-size' => 'mini' ) ) )
            ->add( 'pinterest', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Pinterest ID' ) ) )
            ->add( 'pinterestShow', CheckboxType::class, array( 'required' => FALSE, 'attr' => Array( 'help' => 'Show Pinterest', 'data-size' => 'mini' ) ) );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Social'
                                ) );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'coresys_corebundle_social';
    }


}
