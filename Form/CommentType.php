<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'createdAt', 'datetime' )
            ->add( 'type' )
            ->add( 'complete' )
            ->add( 'updatedAt', 'datetime' )
            ->add( 'title' )
            ->add( 'body' )
            ->add( 'parent' );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Note'
                                ) );
    }
}
