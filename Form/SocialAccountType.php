<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialAccountType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'facebookEmail', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'data-ng-model' => 'user.socialAccount.facebookEmail', 'help' => '(optional) Email used to login with facebook.' ) ) )
            ->add( 'twitterEmail', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'data-ng-model' => 'user.socialAccount.twitterEmail', 'help' => '(optional) Email used to login to twitter.' ) ) )
            ->add( 'twitterScreenName', TextType::class, array( 'required' => FALSE, 'attr' => array( 'data-ng-model' => 'user.socialAccount.twitterScreenName', 'help' => '(optional) Twitter Screen Name. Ie: @MyScreenName' ) ) )
            ->add( 'instagramEmail', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'data-ng-model' => 'user.socialAccount.instagramEmail', 'help' => '(optional) Email used to login to instagram.' ) ) )
            ->add( 'googleEmail', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'data-ng-model' => 'user.socialAccount.googleEmail', 'help' => '(optional) Email used to login to google plus.' ) ) )
            ->add( 'user', EntityType::class, array(
                'multiple' => false,
                'class' => 'CoreSysCoreBundle:User',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\SocialAccount'
                                ) );
    }
}
