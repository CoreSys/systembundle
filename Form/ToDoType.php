<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TodoType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'title', TextType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Title', 'data-ng-model' => 'todo.title' ) ) )
            ->add( 'body', TextareaType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Todo Content', 'data-ng-model' => 'todo.body' ) ) )
            ->add( 'complete', CheckboxType::class, array( 'required' => FALSE ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Note'
                                ) );
    }
}
