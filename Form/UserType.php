<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'gravatar', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Gravatar Email Address', 'help' => 'Email address for Gravatar' ) ) )
            ->add( 'gravatar_use', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'username', TextType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Username', 'help' => 'Desired Username' ) ) )
            ->add( 'email', EmailType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Email Address', 'help' => 'User Email Address' ) ) )
            ->add( 'firstName', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Users First Name' ) ) )
            ->add( 'lastName', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Users Last Name' ) ) )
            ->add( 'image', EntityType::class, array(
                'required'     => FALSE,
                'class'        => 'CoreSysCoreBundle:Image',
                'choice_label' => 'filename',
                'multiple'     => FALSE
            ) )
            ->add( 'plainPassword', RepeatedType::class, array(
                'type'            => PasswordType::class,
                'invalid_message' => 'The passwords must match.',
                'options'         => array( 'attr' => array( 'class' => 'password-field' ) ),
                'required'        => TRUE,
                'first_options'   => array( 'label' => 'Password' ),
                'second_options'  => array( 'label' => 'Confirm Password' )
            ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\User'
                                ) );
    }
}
