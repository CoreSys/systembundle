<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminUserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'gravatar', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Gravatar Email Address', 'help' => 'Email address for Gravatar' ) ) )
            ->add( 'gravatar_use', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Use Gravatar?', 'data-size' => 'mini' ) ) )
            ->add( 'username', TextType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Username', 'help' => 'Desired Username' ) ) )
            ->add( 'enabled', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Is this user enabled?', 'data-size' => 'mini' ) ) )
            ->add( 'email', EmailType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Email Address', 'help' => 'User Email Address' ) ) )
            ->add( 'plainPassword', RepeatedType::class, array(
                'type'            => PasswordType::class,
                'invalid_message' => 'The passwords must match.',
                'options'         => array( 'attr' => array( 'class' => 'password-field' ) ),
                'required'        => FALSE,
                'first_options'   => array( 'label' => 'Password', 'attr' => array( 'help' => 'Enter Password' ) ),
                'second_options'  => array( 'label' => 'Confirm Password', 'attr' => array( 'help' => 'Confirm Password' ) )
            ) )
            ->add( 'image', EntityType::class, array(
                'required'     => FALSE,
                'class'        => 'CoreSysCoreBundle:Image',
                'choice_label' => 'filename',
                'multiple'     => FALSE
            ) )
            ->add( 'firstName', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Users First Name' ) ) )
            ->add( 'lastName', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Users Last Name' ) ) )
            ->add( 'locked', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Is this user locked?' ) ) )
            ->add( 'expired', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Is this user expired?' ) ) )
            ->add( 'sys_roles', EntityType::class, array(
                                  'required' => FALSE,
                                  'class'    => 'CoreSysCoreBundle:Role',
                                  'multiple' => TRUE,
                                  'attr'     => array(
                                      'help' => 'System Roles to Add to User'
                                  )
                              )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\User'
                                ) );
    }
}
