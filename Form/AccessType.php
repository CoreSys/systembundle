<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccessType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'path', TextType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'The path for the access' ) ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'host', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Host to tie this access to' ) ) )
            ->add( 'ip', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The IP Address this access applies to' ) ) )
            ->add( 'anonymous', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Is this an anonymous access?' ) ) )
            ->add( 'methods', ChoiceType::class, array( 'required' => FALSE, 'multiple' => TRUE, 'choices' => $this->getMethodChoices() ) )
            ->add( 'mandatory', CheckboxType::class, array( 'required' => FALSE ) )
            ->add( 'channel', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Access Channel' ) ) )
            ->add( 'roles', EntityType::class, array( 'required' => FALSE, 'multiple' => TRUE, 'class' => 'CoreSysCoreBundle:Role', 'choice_label' => 'name' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Access'
                                ) );
    }

    public function getMethodChoices()
    {
        return array(
            'Get'    => 'GET',
            'Post'   => 'POST',
            'Put'    => 'PUT',
            'Patch'  => 'PATCH',
            'Delete' => 'DELETE',
            'Head'   => 'HEAD',
            'Link'   => 'LINK',
            'Unlink' => 'UNLINK'
        );
    }
}
