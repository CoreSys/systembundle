<?php

namespace CoreSys\CoreBundle\Twig;

use CoreSys\CoreBundle\Entity\Image;
use CoreSys\CoreBundle\Entity\ImageSize;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class CoreTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.media", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class MediaTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_media';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( "cs_getTotalImages", array( $this, 'getTotalImages' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( "cs_getTotalImageSizes", array( $this, 'getTotalImageSizes' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( "cs_getSmallestImage", array( $this, 'getSmallestImage' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( "cs_getSmallestImageUrl", array( $this, 'getSmallestImageUrl' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( "cs_getSmallestImageElement", array( $this, 'getSmallestImageElement' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( "cs_getImageUrl", array( $this, 'getImageUrl' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( "cs_getImageElement", array( $this, 'getImageElement' ), array( 'is_safe' => array( 'html' ) ) )
        );
    }

    /**
     * Get the total count of all images available in the system
     *
     * @return mixed
     */
    public function getTotalImages()
    {
        return $this->getRepo( 'CoreSysCoreBundle:Image' )->getTotalImages();
    }

    /**
     * Get a total count of all the available image sizes in the system
     *
     * @return mixed
     */
    public function getTotalImageSizes()
    {
        return $this->getRepo( 'CoreSysCoreBundle:ImageSize' )->getTotalSizes();
    }

    /**
     * Alias
     * From a specified image, get the smallest available image size URL
     *
     * @param Image $image
     *
     * @return mixed
     */
    public function getSmallestImage( Image $image )
    {
        return $this->getSmallestImageUrl( $image );
    }

    /**
     * From a specified image, get the smallest available image size URL
     *
     * @param Image $image
     *
     * @return mixed
     */
    public function getSmallestImageUrl( Image $image )
    {
        return $this->generateUrl( 'media_get_smallest_image', array( 'slug' => $image->getFilename() ) );
    }

    /**
     * From a specified image, get the smallest available image element.
     * This will return full HTML img tag
     *
     * @param Image $image
     * @param array $options
     *
     * @return string
     */
    public function getSmallestImageElement( Image $image, $options = array() )
    {
        $classes = array( 'img-responsive' );
        if ( !empty( $options[ 'classes' ] ) ) {
            if ( is_array( $options[ 'classes' ] ) ) {
                foreach ( $options[ 'classes' ] as $cls ) {
                    $classes[] = $cls;
                }
            } else {
                $classes[] = $options[ 'classes' ];
            }
            unset( $options[ 'classes' ] );
        }

        $element = array( '<img src="' . $this->getSmallestImageUrl( $image ) . '" ' );
        if ( count( $options ) > 0 ) {
            foreach ( $options as $key => $val ) {
                $element[] = $key . '="' . $val . '" ';
            }
        }
        $element[] = ' class="' . implode( ' ', $classes );
        $element[] = ' />';

        return implode( $element );
    }

    /**
     * From a specified Image and ImageSize, return the image URL
     *
     * @param Image          $image
     * @param ImageSize|NULL $size
     *
     * @return string
     */
    public function getImageUrl( Image $image, ImageSize $size = NULL )
    {
        if ( $size !== NULL ) {
            return $this->getBaseController()->generateUrl( 'media_image_get', array( 'size' => $size->getName(), 'slug' => $image->getFilename() ) );
        } else {
            return $this->generateUrl( 'media_image_get', array( 'size' => $image->getFilename() ) );
        }
    }

    /**
     * From a specified Image and Imagesize, return the image element
     * This will return HTML img tag
     *
     * @param Image          $image
     * @param ImageSize|NULL $size
     * @param array          $options
     *
     * @return string
     */
    public function getImageElement( Image $image, ImageSize $size = NULL, $options = array() )
    {
        $classes = array( 'img-responsive' );
        if ( !empty( $options[ 'classes' ] ) ) {
            if ( is_array( $options[ 'classes' ] ) ) {
                foreach ( $options[ 'classes' ] as $cls ) {
                    $classes[] = $cls;
                }
            } else {
                $classes[] = $options[ 'classes' ];
            }
            unset( $options[ 'classes' ] );
        }

        $element = array( '<img src="' . $this->getImageUrl( $image, $size ) . '" ' );
        if ( count( $options ) > 0 ) {
            foreach ( $options as $key => $val ) {
                $element[] = $key . '="' . $val . '"';
            }
        }
        $element[] = 'class="' . implode( ' ', $classes );
        $element[] = ' />';

        return implode( ' ', $element );
    }
}