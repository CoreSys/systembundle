<?php

namespace CoreSys\CoreBundle\Twig;

use CoreSys\CoreBundle\Manager\AdminMenuManager;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class AdminMenuTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.admin_menu", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class AdminMenuTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_admin_menu';

    /**
     * @var AdminMenuManager
     */
    protected $manager;

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_getAdminMenus', array( $this, 'getAdminMenus' ) )
        );
    }

    /**
     * Get a list of the admin menus available in the system.
     * Optional Parameter $active to only fetch active or inactive menus.
     * Use $active = null to get a list of all admin menus.
     *
     * @param null $active
     *
     * @return array
     */
    public function getAdminMenus( $active = NULL )
    {
        return $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Menu' )->getBaseMenus( $active );
//        $repo = $this->baseController->getRepo( 'CoreSysCoreBundle:AdminMenu' );
//
//        $menus = $repo->findAll( $active );
//        $this->validateMenus( $menus );
//
//        $return = new ArrayCollection();
//        foreach ( $menus as $menu ) {
//            $parent = $menu->getParent();
//            if ( empty( $parent ) ) {
//                $return->add( $menu );
//            }
//        }
//
//        return $return;
    }

    /**
     * @return AdminMenuManager|object
     */
    public function getManager()
    {
        if ( !empty( $this->manager ) ) {
            return $this->manager;
        }

        return $this->manager = $this->baseController->get( 'core_sys_core.manager.admin_menu' );
    }

    /**
     * @param $menus
     */
    protected function validateMenus( &$menus )
    {
        foreach ( $menus as $key => $menu ) {
            $result = $this->validateMenu( $menu );
            if ( !$result ) {
                $this->getBaseController()->removeAndFlush( $menu );
                unset( $menus[ $key ] );
            }
        }
    }

    /**
     * @param $menu
     *
     * @return bool
     */
    protected function validateMenu( &$menu )
    {
        $srcFolder = $this->getBaseController()->getRootFolder() . DIRECTORY_SEPARATOR . 'src';
        $file      = implode( DIRECTORY_SEPARATOR, array(
            $srcFolder,
            $menu->getNamespace(),
            $menu->getBundle(),
            'Resources', 'views', 'AdminMenu',
            $menu->getFile()
        ) );

        return is_file( $file );
    }
}