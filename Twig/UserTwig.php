<?php

namespace CoreSys\CoreBundle\Twig;

use CoreSys\CoreBundle\Entity\User;
use CoreSys\CoreBundle\Form\AdminUserType;
use CoreSys\CoreBundle\Form\UserType;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class UserTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.user", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class UserTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_user';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_getUserForm', array( $this, 'getForm' ) ),
            new \Twig_SimpleFunction( 'cs_getUserRegisterForm', array( $this, 'getRegisterForm' ) ),
            new \Twig_SimpleFunction( 'cs_getUserImage', array( $this, 'getUserImage' ) ),
            new \Twig_SimpleFunction( 'cs_getUserGavatarImage', array( $this, 'getUserGravatarImage' ) ),
            new \Twig_SimpleFunction( 'cs_getUserFacebookImage', array( $this, 'getUserFacebookImage' ) )
        );
    }

    /**
     * Get the profile image for a specified user.
     * User class must be passed.
     * To get current logged in user: {{ app.request.user }}
     * Supply the size, default is 40 .. this will be used for gravatars ..etc
     *
     * @param User $user
     * @param int  $size
     *
     * @return bool|null|string
     */
    public function getUserImage( User $user = NULL, $size = 40 )
    {
        $rootUrl = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();

        if ( empty( $user ) ) {
            $user = $this->getBaseController()->getUser();
            if ( empty( $user ) ) {
                // return default image
                $image = $rootUrl . $this->get( 'assets.packages' )->getUrl( '/bundles/coresyscore/images/defaultUser.jpg', NULL );

                return str_replace( '/web/web', '/web', $image );
            }
        }
        $image = NULL;
        if ( $user->getGravatarUse() ) {
            $image = $this->getUserGravatarImage( $user, $size );
        } else if ( $user->hasSocialdata( 'facebook' ) ) {
            $image = $this->getUserFacebookImage( $user );
        } else {
            $image = $rootUrl . $this->get( 'assets.packages' )->getUrl( '/bundles/coresyscore/images/defaultUser.jpg', NULL );
        }

        return str_replace( '/web/web', '/web', $image );
    }

    /**
     * Fetch the user register form for the system
     *
     * @return string
     */
    public function getRegisterForm( $options = array() )
    {
        $form = $this->getForm( FALSE );

        return $this->baseController->renderView( 'CoreSysCoreBundle:UserRest:registerForm.html.twig', array( 'form' => $form->createView() ) );
    }

    /**
     * Get a user form, either admin or public.
     *
     * @return Form
     */
    public function getForm( $admin = FALSE, $options = array() )
    {
        $user = new User();

        if ( $admin ) {
            $admin = $this->baseController->isGranted( 'ROLE_ADMIN' );
        }

        if ( $admin ) {
            return $this->baseController->createForm( AdminUserType::class, $user, $options );
        } else {
            return $this->baseController->createForm( UserType::class, $user, $options );
        }
    }

    /**
     * If available, fetch a users gravatar image
     *
     * @param User $user
     * @param int  $size
     *
     * @return string
     */
    public function getUserGravatarImage( User $user, $size = 40 )
    {
        $rootUrl = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();

        $default = $rootUrl . $this->baseController->get( 'assets.packages' )->getUrl( '/bundles/coresyscore/images/defaultUser.jpg' );
        $default = str_replace( '/web/web', '/web', $default );
        $default = str_replace( '/app_dev.php', '', $default );
        $default = str_replace( '/app.php', '', $default );

        return $user->getGravatarUrl() . '?' . http_build_query( array(
                                                                     'd' => $default,
                                                                     's' => intval( $size )
                                                                 ) );
    }

    /**
     * If available, fetch a users facebook image
     *
     * @param User $user
     *
     * @return bool|null
     */
    public function getUserFacebookImage( User $user )
    {
        $socialData = $user->getSocialData();
        if ( !empty( $socialData ) ) {
            return $socialData->getFacebookPicture();
        }

        return NULL;
    }
}