<?php

namespace CoreSys\CoreBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class CommonTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.common", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class CommonTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_common';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_isBool', array( $this, 'isBool' ) ),
            new \Twig_SimpleFunction( 'cs_isString', array( $this, 'isString' ) ),
            new \Twig_SimpleFunction( 'cs_isNumeric', array( $this, 'isNumeric' ) ),
            new \Twig_SimpleFunction( 'cs_isArray', array( $this, 'isArray' ) ),
            new \Twig_SimpleFunction( 'cs_isNull', array( $this, 'isNull' ) ),
            new \Twig_SimpleFunction( 'cs_urlEncode', array( $this, 'urlEncode' ), array( 'is_safe' => array( 'html', 'js' ) ) ),
            new \Twig_SimpleFunction( 'cs_removeLineBreaks', array( $this, 'removeLineBreaks' ) )
        );
    }

    /**
     * Encode an URL
     *
     * @param $url
     *
     * @return string
     */
    public function urlEncode( $url )
    {
        return urlencode( $url );
    }

    /**
     * Check if a variable is NULL
     *
     * @param $val
     *
     * @return bool
     */
    public function isNull( $val )
    {
        return $val === NULL;
    }

    /**
     * Remove line breaks from specified content
     *
     * @param $content
     *
     * @return mixed
     */
    public function removeLineBreaks( $content )
    {
        return preg_replace( '/[\r\n]+/', '', $content );
    }

    /**
     * Check if a variable is a boolean
     *
     * @param $val
     *
     * @return bool
     */
    public function isBool( $val )
    {
        return $val === TRUE || $val === FALSE;
    }

    /**
     * Check is a variable is a string
     *
     * @param $val
     *
     * @return bool
     */
    public function isString( $val )
    {
        return is_string( $val );
    }

    /**
     * Check if a variable is numeric
     *
     * @param $val
     *
     * @return bool
     */
    public function isNumeric( $val )
    {
        return is_numeric( $val );
    }

    /**
     * Check if a vairable is an array
     *
     * @param $arr
     *
     * @return bool
     */
    public function isArray( $arr )
    {
        return is_array( $arr );
    }
}