<?php

namespace CoreSys\CoreBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class HelpersTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.helpers", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class HelpersTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_helpers';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_openPanel', array( $this, 'openPanel' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_openCard', array( $this, 'openCard' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_closePanel', array( $this, 'closePanel' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_closeCard', array( $this, 'closeCard' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_helperAngularIncludes', array( $this, 'getAngularIncludes' ), array( 'is_safe' => array( 'html', 'js' ) ) ),
            new \Twig_SimpleFunction( 'cs_helperAngularModule', array( $this, 'getAngularModule' ), array( 'is_safe' => array( 'html', 'js' ) ) ),
        );
    }

    /**
     * Include the required Angular Javascripts to use Angular 1.x on a given page
     *
     * @return mixed
     */
    public function getAngularIncludes()
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:Angular/coreJavascripts.html.twig' );
    }

    /**
     * Write out the required angular module template to the page;
     *
     * @param string $appName
     * @param string $moduleName
     * @param array  $dependencies
     *
     * @return mixed
     */
    public function getAngularModule( $appName = 'CSApp', $moduleName = 'CSApp', $dependencies = array( 'ngResource', 'ui.bootstrap', 'ui.sortable', 'ui.select' ) )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:Angular/angularModule.js.twig', array(
            'appName'      => $appName,
            'moduleName'   => $moduleName,
            'dependencies' => is_array( $dependencies ) ? $dependencies : array( $dependencies )
        ) );
    }

    /**
     * Open a new Panel (HTML)
     *
     * @param string $type
     * @param null   $title
     *
     * @return mixed
     */
    public function openPanel( $type = 'default', $title = NULL )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:openPanel.html.twig', array( 'type' => $type, 'title' => $title ) );
    }

    /**
     * Open a new Card (HTML)
     *
     * @param string $type
     * @param null   $title
     * @param null   $headerBtns
     *
     * @return mixed
     */
    public function openCard( $type = 'default', $title = NULL, $headerBtns = NULL )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:openCard.html.twig', array( 'type' => $type, 'title' => $title, 'headerBtns' => $headerBtns ) );
    }

    /**
     * Close a Panel (HTML)
     *
     * @return mixed
     */
    public function closePanel()
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:closePanel.html.twig' );
    }

    /**
     * Close a Card (HTML)
     *
     * @return mixed
     */
    public function closeCard()
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:closeCard.html.twig' );
    }
}