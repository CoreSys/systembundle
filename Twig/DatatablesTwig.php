<?php

namespace CoreSys\CoreBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\SerializationContext;

/**
 * Class DatatablesTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.datatables")
 * @DI\Tag("twig.extension")
 */
class DatatablesTwig Extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_datatables';

    /**
     * @var array
     */
    protected $dtOptions = array();

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_datatable_options', array( $this, 'getDatatableOptions' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_datatable_html', array( $this, 'getDatatableHtml' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_datatable_js', array( $this, 'getDatatableJs' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_datatableStyles', array( $this, 'includeDatatablesStyles' ), array( 'is_safe' => array( 'html', 'js', 'css' ) ) ),
            new \Twig_SimpleFunction( 'cs_datatableJavascript', array( $this, 'includeDatatablesJavascript' ), array( 'is_safe' => array( 'html', 'js', 'css' ) ) ),
        );
    }

    /**
     * Render all stylesheets required for datatables
     *
     * @return mixed
     */
    public function includeDatatablesStyles()
    {
        return $this->renderView( 'CoreSysCoreBundle:Datatables:datatablesStyles.html.twig' );
    }

    /**
     * Render all javascripts required for datatables
     *
     * @return mixed
     */
    public function includeDatatablesJavascript()
    {
        return $this->renderView( 'CoreSysCoreBundle:Datatables:datatablesJavascript.html.twig' );
    }

    /**
     * Fetch all datatable options for a specified Entity
     *
     * @param $entity
     *
     * @return mixed
     */
    public function getDatatableOptions( $entity )
    {
        $class = get_class( $entity );
        if ( isset( $this->dtOptions[ $class ] ) ) {
            return $this->dtOptions[ $class ];
        }

        $manager = $this->get( 'core_sys_core.manager.datatables' );

        return $this->dtOptions[ $class ] = $manager->getDatatablesOptions( $entity );
    }

    /**
     * Get the html required for building a table for a specified entity
     *
     * @param       $entity
     * @param array $overrides
     *
     * @return mixed
     */
    public function getDatatableHtml( $entity, array $overrides = array() )
    {
        $options = $this->getDatatableOptions( $entity );
        $options = array_merge( $options, $overrides );

        return $this->renderView( 'CoreSysCoreBundle:Datatables:table.html.twig', $options );
    }

    /**
     * Render out the javascript required to control the output of a datatable for a specified Entity
     *
     * @param       $entity
     * @param array $overrides
     *
     * @return string
     */
    public function getDatatableJs( $entity, array $overrides = array() )
    {
        $baseOptions = $this->getDatatableOptions( $entity );
        $options     = array();

        $notDtOptions = array( 'resource', 'renderers', 'title', 'checkable', 'tableActions', 'rowActions', 'hasRowActions', 'hasTableActions', 'createUrl', 'editUrl' );
        $dtOptions    = array();
        foreach ( $baseOptions as $key => $value ) {
            if ( in_array( $key, $notDtOptions ) ) {
                $options[ $key ] = $value;
            } else {
                $dtOptions[ $key ] = $value;
            }
        }

        $this->handleDatatableJsOverrides( $dtOptions, $overrides );
        $this->handleDatatableOptionsOverrides( $options, $overrides );

        $context = new SerializationContext();
        $context->setSerializeNull( TRUE );

        $dtOptions = $this->renderView( 'CoreSysCoreBundle:Datatables:dtOptions.js.twig', array( 'options' => $dtOptions, 'context' => $context ) );

        $content = $this->renderView( 'CoreSysCoreBundle:Datatables:table.js.twig', array(
            'options'   => $options,
            'dtOptions' => $dtOptions,
            'context'   => $context
        ) );

        $lines = array();
        foreach ( preg_split( "/[\n\r]+/", $content ) as $line ) {
            $line = trim( $line );
            if ( !empty( $line ) ) {
                $lines[] = $line;
            }
        }

        return implode( '', $lines );
    }

    /**
     * INTERNAL ONLY
     *
     * @param $dtOptions
     * @param $overrides
     */
    private function handleDatatableJsOverrides( &$dtOptions, &$overrides )
    {
        if ( isset( $overrides[ 'dtOptions' ] ) ) {
            // handle dtOption specific overrides
            $options = $overrides[ 'dtOptions' ];
            if ( isset( $options[ 'ajax' ] ) && isset( $dtOptions[ 'ajax' ] ) ) {
                $dtOptions[ 'ajax' ] = array_merge( $dtOptions[ 'ajax' ], $options[ 'ajax' ] );
            }
        }
    }

    /**
     * INTERNAL ONLY
     *
     * @param $options
     * @param $overrides
     */
    private function handleDatatableOptionsOverrides( &$options, &$overrides )
    {
        if ( isset( $overrides[ 'options' ] ) ) {
            $over = $overrides[ 'options' ];
            if ( isset( $over[ 'tableActions' ] ) && is_array( $over[ 'tableActions' ] ) ) {
                $options[ 'tableActions' ] = $over[ 'tableActions' ];
            }
        }
    }
}