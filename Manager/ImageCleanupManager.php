<?php

namespace CoreSys\CoreBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ImageCleanupManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.image_cleanup", parent="core_sys_core.manager.base")
 */
class ImageCleanupManager extends BaseManager
{

    /**
     * @var array
     */
    private $imageSizeFolders = array();

    /**
     * @var array
     */
    private $images = array();

    /**
     * Perform cleanup
     */
    public function cleanup()
    {
        $this->imageSizeFolders = array();
        $this->images           = array();
        $this->locateSizeFolders();
        $this->validateImageSizeFolders();
        $this->locateOrphanedImages();
        $this->validateOrphanedImages();
    }

    /**
     * Obtain a list of images that are not in the database
     */
    protected function locateOrphanedImages()
    {
        $imagesFolder  = $this->getBaseController()->getMediaImagesFolder();
        $imagesPattern = implode( DIRECTORY_SEPARATOR, array( $imagesFolder, '*', '*', '*', '*', '*.{gif,jpg,png,jpeg}' ) );
        $this->log( 'Images Folder: ' . $imagesFolder );
        $this->log( 'Images Pattern: ' . $imagesPattern );
        $images = glob( $imagesPattern, GLOB_BRACE );

        foreach ( $images as $img ) {
            $ext      = pathinfo( $img, PATHINFO_EXTENSION );
            $filename = pathinfo( $img, PATHINFO_FILENAME ) . '.' . $ext;
            $this->log( 'Filename: ' . $filename );
            $this->images[ $filename ] = $img;
        }
    }

    /**
     * Validate and remove orphaned images
     */
    protected function validateOrphanedImages()
    {
        $repo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Image' );
        $this->log( '<h3>Validating Orphaned Images</h3>' );
        foreach ( $this->images as $fname => $image ) {
            $img = $repo->findOneBy( array( 'filename' => $fname ) );
            if ( empty( $img ) ) {
                $this->log( 'Removing: ' . $fname . ': ' . $image );
                $this->removeOrphanedImage( $image );
            }
        }
    }

    /**
     * @param $filename
     */
    protected function removeOrphanedImage( $filename )
    {
        $ext    = pathinfo( $filename, PATHINFO_EXTENSION );
        $file   = pathinfo( $filename, PATHINFO_FILENAME ) . '.' . $ext;
        $folder = pathinfo( $filename, PATHINFO_DIRNAME );
        $this->log( 'Remove Ext: ' . $ext );
        $this->log( 'Remove File: ' . $file );
        $this->log( 'Remove Folder: ' . $folder );
        $filePattern = implode( DIRECTORY_SEPARATOR, array( $folder, '*', $file ) );
        $files       = glob( $filePattern );
        foreach ( $files as $ifile ) {
            $this->log( 'Unlink: ' . $ifile );
            $this->removeFile( $ifile );
        }

        $this->removeFile( $filename );
    }

    /**
     * @param $folder
     */
    protected function removeFolder( $folder )
    {
        if ( is_dir( $folder ) ) {
            $dir = opendir( $folder );
            if ( $dir ) {
                while ( FALSE !== ( $file = readdir( $dir ) ) ) {
                    if ( $file !== '.' && $file !== '..' ) {
                        $filename = $folder . DIRECTORY_SEPARATOR . $file;
                        if ( is_dir( $filename ) ) {
                            $this->removeFolder( $filename );
                        } else if ( is_file( $filename ) ) {
                            $this->removeFile( $file );
                        }
                    }
                }
                closedir( $dir );
                @rmdir( $folder );
            }
        }
    }

    /**
     * @param $file
     */
    protected function removeFile( $file )
    {
        if ( is_file( $file ) ) {
            @unlink( $file );
        }
    }

    /**
     *
     */
    protected function locateSizeFolders()
    {
        $imagesFolder = $this->getBaseController()->getMediaImagesFolder();
        $this->log( 'Images Folder: ' . $imagesFolder );
        $folderPattern = implode( DIRECTORY_SEPARATOR, array( $imagesFolder, '*', '*', '*', '*', '*' ) );
        $folders       = glob( $folderPattern, GLOB_ONLYDIR );
        foreach ( $folders as $folder ) {
            $dir = pathinfo( $folder, PATHINFO_FILENAME );
            if ( !isset( $this->imageSizeFolders[ $dir ] ) ) {
                $this->imageSizeFolders[ $dir ] = array();
            }
            if ( !in_array( $folder, $this->imageSizeFolders[ $dir ] ) ) {
                $this->imageSizeFolders[ $dir ][] = $folder;
            }
        }
    }

    /**
     * Validate and remove image size folders that are not found in the database
     */
    protected function validateImageSizeFolders()
    {
        $repo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:ImageSize' );
        foreach ( $this->imageSizeFolders as $sizeName => $folders ) {
            $size = $repo->findOneBy( array( 'name' => $sizeName ) );
            if ( empty( $size ) ) {
                $this->log( 'Removing: ' . $sizeName );
                foreach ( $folders as $folder ) {
                    $this->removeFolder( $folder );
                }
            }
        }

    }
}