<?php

namespace CoreSys\CoreBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Process\Process;

/**
 * Class DatabaseManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.database", parent="core_sys_core.manager.base")
 */
class DatabaseManager extends BaseManager
{

    /**
     * @var string
     */
    protected $backupFolder;

    /**
     * @var string
     */
    protected $baseFolder;

    /**
     * @var GitManager
     */
    protected $gitManager;

    /**
     * @return GitManager
     */
    public function getGitManager()
    {
        if ( !empty( $this->gitManager ) ) {
            return $this->gitManager;
        }

        return $this->gitManager = $this->getBaseController()->get( 'core_sys_core.manager.git' );
    }

    /**
     * @return string
     */
    public function getBaseFolder()
    {
        if ( !empty( $this->baseFolder ) ) {
            return $this->baseFolder;
        }

        return $this->baseFolder = dirname( $this->getBaseController()->getAppFolder() );
    }

    /**
     * @return mixed
     */
    public function getBackupFolder()
    {
        if ( !empty( $this->backupFolder ) ) {
            return $this->backupFolder;
        }

        return $this->backupFolder = $this->getBaseController()->verifyFolder(
            $this->baseController->getMediaFolder()
            . DIRECTORY_SEPARATOR . 'backup'
        );
    }

    public function createBackup()
    {
        $filename = date( 'Y-m-d-H' ) . '.sql';
        $file     = $this->getBackupFolder() . DIRECTORY_SEPARATOR . $filename;

        if ( is_file( $file ) ) {
            // the file already exists
            // so this is technically a success
            return TRUE;
        }

        $result = $this->dumpBackupSql( $file );
        if ( $result ) {
            $this->createGitBackup( $filename );
        }

        return $result;
    }

    protected function createGitBackup( $filename )
    {
        $branch  = 'DbBackup' . str_replace( '-', '_', str_replace( '.sql', '', $filename ) );
        $message = 'Added and updated Db backup ' . $filename;

        $manager        = $this->getGitManager();
        $currentBranch  = $manager->currentBranch( FALSE );
        $currentMessage = 'Saving to Database Backup ' . date( 'M d, Y g:i a' );

        // if the branch already exists, remove it
        if ( $manager->hasBranch( $branch ) ) {
            $manager->deleteBranch( $branch, TRUE );
        }

        // add/commit/push the current branch
        $manager->add( '.' );
        $manager->commit( $currentMessage );
        $manager->push( $currentBranch );

        // create new branch, add, commit, push
        $manager->checkout( $branch, TRUE );
        $manager->add( '.' );
        $manager->commit( $message );
        $manager->push( $branch );

        // go back to original branch
        $manager->checkout( $currentBranch );

        // merge the backup branch
        $oBranch = $manager->currentBranch( FALSE );
        if ( $oBranch === $currentBranch ) {
            $manager->merge( $branch );
        }
    }

    protected function dumpBackupSql( $dst )
    {
        $container = $this->getBaseController()->getContainer();
        $dbName    = $container->getParameter( 'database_name' );
        $dbUser    = $container->getParameter( 'database_user' );
        $dbPass    = $container->getParameter( 'database_password' );
        $dbPort    = $container->getParameter( 'database_port' );

        $command = array( 'mysqldump' );
        if ( !empty( $dbName ) ) {
            $command[] = '-u ' . $dbUser;
        }
        if ( !empty( $dbPass ) ) {
            $command[] = '--password=' . $dbPass;
        }
        if ( !empty( $dbName ) ) {
            $command[] = $dbName;
        }
        if ( !empty( $dbPort ) ) {
            $command[] = '--port=' . $dbPort;
        }
        $command[] = ' > ' . $dst;
        $command   = implode( ' ', $command );

        $process = new Process( $command );
        $process->run();

        if ( $process->isSuccessful() ) {
            return TRUE;
        }

        return FALSE;
    }
//
//    /**
//     * @return bool
//     */
//    public function hasCurrentBackupFile()
//    {
//        $filename = $this->getCurrentBackupFile();
//
//        return is_file( $filename );
//    }
//
//    public function createBackup()
//    {
//        $folder     = $this->getBackupFolder();
//        $file       = $this->getCurrentBackupFile();
//        $baseFolder = dirname( $this->getBaseController()->getAppFolder() );
//
//        $dbName     = $this->getBaseController()->getContainer()->getParameter( 'database_name' );
//        $dbUser     = $this->getBaseController()->getContainer()->getParameter( 'database_user' );
//        $dbPassword = $this->getBaseController()->getContainer()->getParameter( 'database_password' );
//        $dbPort     = $this->getBaseController()->getContainer()->getParameter( 'database_port' );
//
//        $dumpCommand = array( 'mysqldump' );
//        if ( !empty( $dbUser ) ) {
//            $dumpCommand[] = '-u ' . $dbUser;
//        }
//        if ( !empty( $dbPassword ) ) {
//            $dumpCommand[] = '--password=' . $dbPassword;
//        }
//        if ( !empty( $dbName ) ) {
//            $dumpCommand[] = $dbName;
//        }
//        if ( !empty( $dbPort ) ) {
//            $dumpCommand[] = '--port=' . $dbPort;
//        }
//        $dumpCommand[] = '> ' . $file;
//        $dumpCommand   = implode( ' ', $dumpCommand );
//
//        if ( !is_file( $file ) ) {
//            echo $dumpCommand;
//            echo '<br>';
//            $dumpResult = $this->dumpSql( $dumpCommand );
//            echo $dumpResult;
//        } else {
//            echo 'Dump file exists<br>';
//        }
//
//        $this->updateGitBackups( $baseFolder );
//    }
//
//    /**
//     * @return mixed
//     */
//    protected function getBackupFolder()
//    {
//        $mediaFolder   = $this->getBaseController()->getMediaFolder();
//        $backupsFolder = $mediaFolder . DIRECTORY_SEPARATOR . 'backups';
//
//        return $this->getBaseController()->verifyFolder( $backupsFolder );
//    }
//
//    /**
//     * @return string
//     */
//    protected function getCurrentBackupFile()
//    {
//        $folder   = $this->getBackupFolder();
//        $filename = date( 'Y-m-d-H' ) . '.backup';
//
//        return $folder . DIRECTORY_SEPARATOR . $filename;
//    }
//
//    protected function getCurrentGitBranch()
//    {
//        $baseFolder = dirname( $this->getBaseController()->getAppFolder() );
//        $process    = new Process( 'cd ' . $baseFolder . ' && git branch' );
//        $process->run();
//        if ( $process->isSuccessful() ) {
//            $branchOut     = $process->getOutput();
//            $branchOut     = preg_split( "/[\r\n]+/", $branchOut );
//            foreach ( $branchOut as $line ) {
//                $line = trim( $line );
//                if ( substr( $line, 0, 1 ) === '*' ) {
//                    $line          = trim( str_replace( '*', '', $line ) );
//                    return $line;
//                }
//            }
//        }
//
//        return NULL;
//    }
//
//    private function dumpSql( $dumpCommand )
//    {
//        $process = new Process( $dumpCommand );
//        $process->run();
//
//        return $process->getOutput();
//    }
//
//    private function updateGitBackups()
//    {
//        $baseFolder   = dirname( $this->getBaseController()->getAppFolder() );
//        $backupFolder = $this->getBackupFolder();
//
//        $currentBranch = $this->getCurrentGitBranch();
//        if ( empty( $currentBranch ) ) {
//            echo 'FETCH CURRENT BRANCH FAILED';
//            return;
//        }
//
//        $saveCurrentBranch = $this->addCommitAndPushCurrentBranch( $currentBranch );
//        if( !$saveCurrentBranch ) {
//            echo 'SAVE CURRENT BRANCH FAILED';
//            return;
//        }
//
//        $result = $this->switchBranchAndSaveDump( $currentBranch );
//        if( !$result ) {
//            echo 'SAVE FAILED';
//            return;
//        }
//
//        echo 'SUCCESS';
//    }
//
//    private function switchBranchAndSaveDump( $branchName )
//    {
//        $baseFolder = dirname( $this->getBaseController()->getAppFolder() );
//        $backupFolder = $this->getBackupFolder();
//        $dumpBranch = 'sqlDump' . date( 'Y_m_d_H_i' );
//        $commands = array(
//            'cd ' . $baseFolder,
//            'git checkout -b ' . $dumpBranch,
//            'git add ' . $backupFolder,
//            'git commit -am "Dumping Sql Data ' . date( 'Y m d, g:i a' ) . '"',
//            'git push -u origin ' . $dumpBranch,
//            'git checkout ' . $branchName,
//            'git merge ' . $dumpBranch
//        );
//        $command = implode( ' && ', $commands );
//        echo '<hr>' . $command . '<hr>';
//        $process = new Process( $command );
//        $process->run();
//        if( $process->isSuccessful() ) {
//            return true;
//        } else {
//            var_dump( $process->getOutput() );
//        }
//
//        return false;
//    }
//
//    private function addCommitAndPushCurrentBranch( $branchName )
//    {
//        $baseFolder = dirname( $this->getBaseController()->getAppFolder() );
//        $process = new Process( 'cd ' . $baseFolder
//                                . ' && git add . && git commit -am "Save for Db Dump ' . date( 'M d, Y g:i a' )
//                                . '" && git push -u origin ' . $branchName );
//        $process->run();
//        if ( $process->isSuccessful() ) {
//            return true;
//        }
//
//        return false;
//    }
}