<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Entity\Role;
use CoreSys\CoreBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\UserBundle\Doctrine\UserManager as FOSUserManager;
use FOS\UserBundle\Util\UserManipulator as FOSUserManipulator;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class UserManager
 *
 * Here we can extend both the FOSUserManager|FosUserManipulator and the BaseManager and have all the functionality of both
 * As well as being able to override or extends the methods contained in FOSUserManager
 *
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.user", parent="core_sys_core.manager.base")
 */
class UserManager extends BaseManager
{

    /**
     * @var FOSUserManager
     */
    private $fosUserManager;

    /**
     * @var FOSUserManipulator
     */
    private $fosUserManipulator;

    /**
     * @var EntityRepository
     */
    private $systemRoleRepo;

    /**
     * Get fosUserManager
     *
     * @return FOSUserManager
     */
    public function getFosUserManager()
    {
        if ( !empty( $this->fosUserManager ) ) {
            return $this->fosUserManager;
        }

        return $this->fosUserManager = $this->getBaseController()->get( 'fos_user.user_manager' );
    }

    /**
     * Get fosUserManipulator
     *
     * @return FOSUserManipulator
     */
    public function getFosUserManipulator()
    {
        if ( !empty( $this->fosUserManipulator ) ) {
            return $this->fosUserManipulator;
        }

        return $this->fosUserManipulator = $this->getBaseController()->get( 'fos_user.util.user_manipulator' );;
    }

    /**
     * @param UserInterface $user
     * @param bool          $andFlush
     *
     * @return UserInterface
     */
    public function updateUser( UserInterface $user, $andFlush = TRUE )
    {
        $this->validateUserSystemRoles( $user );
        $this->validateUserRoles( $user );

        $roles    = $user->getRoles();
        $sysRoles = $user->getSysRoles();

        foreach( $sysRoles as $role ) {
            $roleName = $role->getRoleName();
            $user->addRole( $roleName );
        }

        $this->getFosUserManager()->updateUser( $user, $andFlush );

        return $user;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call( $name, $arguments )
    {
        $this->getFosUserManager();
        $this->getFosUserManipulator();

        if ( is_object( $this->fosUserManager ) && method_exists( $this->fosUserManager, $name ) ) {
            return call_user_func_array( array(
                                             $this->fosUserManager,
                                             $name
                                         ), $arguments );
        } else if ( is_object( $this->fosUserManipulator ) && method_exists( $this->fosUserManipulator, $name ) ) {
            return call_user_func_array( array(
                                             $this->fosUserManipulator,
                                             $name
                                         ), $arguments );
        }

        return parent::__call( $name, $arguments );
    }

    protected function validateUserRoles( UserInterface &$user )
    {
        $roles = $user->getRoles();
        foreach ( $roles as $role ) {
            $has = $user->hasSysRole( $role );
            if ( $has === FALSE ) {
                $sysRole = $this->fetchSystemRole( $role );
                if ( empty( $sysRole ) ) {
                    $user->removeRole( $role );
                } else {
                    $user->addSysRole( $sysRole );
                }
            } else {
                // no such system role
                $user->removeRole( $role );
            }
        }
    }

    protected function getSystemRoleRepo()
    {
        if ( !empty( $this->systemRoleRepo ) ) {
            return $this->systemRoleRepo;
        }

        return $this->systemRoleRepo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Role' );
    }

    protected function fetchSystemRole( $roleName )
    {
        return $this->getSystemRoleRepo()->findOneBy( array( 'roleName' => $roleName ) );
    }

    protected function validateUserSystemRoles( UserInterface &$user )
    {
        $sysRoles = $user->getSysRoles();
        foreach ( $sysRoles as $sysRole ) {
            $roleName = $sysRole->getRoleName();
            if ( !$user->hasRole( $roleName ) ) {
                $user->addRole( $roleName );
            }
        }
    }
}