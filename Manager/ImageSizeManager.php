<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use CoreSys\CoreBundle\Entity\Image;
use CoreSys\CoreBundle\Entity\ImageSize;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImageSizeManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.image_size", parent="core_sys_core.manager.base")
 */
class ImageSizeManager extends BaseManager
{

    /**
     * @var
     */
    private $repo;

    /**
     * @var
     */
    private $image_manager;

    /**
     * ImageSizeManager constructor.
     *
     * @param BaseController|NULL     $baseController
     * @param NULL|ContainerInterface $container
     */
    public function __construct( $baseController, $container )
    {
        parent::__construct( $baseController, $container );
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepo()
    {
        if ( !empty( $this->repo ) ) {
            return $this->repo;
        }

        return $this->repo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:ImageSize' );
    }

    /**
     * @return ImageManager
     */
    public function getImageManager()
    {
        if ( !empty( $this->image_manager ) ) {
            return $this->image_manager;
        }

        return $this->image_manager = $this->getBaseController()->get( 'core_sys_core.manager.image' );
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteByid( $id )
    {
        $size = $this->repo->find( $id );
        if ( !empty( $size ) ) {
            return $this->deleteImageSize( $size );
        }

        return FALSE;
    }

    /**
     * @param null $size
     *
     * @return null
     */
    public function locateSize( $size = NULL )
    {
        if ( $size instanceof ImageSize ) {
            return $size;
        }

        return $this->getRepo()->locateSize( $size );
    }

    /**
     * @param null $size
     * @param bool $flush
     *
     * @return bool
     */
    public function removeSize( $size = NULL, $flush = TRUE )
    {
        $size = $this->locateSize( $size );
        return $this->deleteSize( $size, !!$flush );
    }

    /**
     * @param null $size
     * @param bool $flush
     *
     * @return $this
     */
    public function updateSize( $size = NULL, $flush = TRUE )
    {
        $size    = $this->locateSize( $size );

        $this->persist( $size );

        if ( $flush ) {
            $this->flush();
        }

        $manager = $this->getImageManager();
        $manager->updateImageSize( $size, FALSE );

        return $this;
    }

    /**
     * @param ImageSize $size
     * @param bool      $remove
     *
     * @return bool
     */
    public function deleteSize( ImageSize &$size, $remove = TRUE )
    {
        if ( !empty( $size ) ) {
            $images = $size->getImages();
            foreach ( $images as $image ) {
                echo 'RMS::';
                $this->removeImageSize( $image, $size );
            }

            if ( $remove ) {
                $this->remove( $size );
                $this->flush();
            }

            exit;
            return TRUE;
        } else {
            echo 'WTF';
        }

        exit;
        return FALSE;
    }

    /**
     * @param Image     $image
     * @param ImageSize $size
     *
     * @return mixed
     */
    public function removeImageSize( Image &$image, ImageSize &$size )
    {
        $manager = $this->getImageManager();

        return $manager->removeImageSize( $image, $size, FALSE );
    }

    /**
     * @param ImageSize|NULL $size
     *
     * @return bool
     */
    public function addSize( ImageSize &$size = NULL )
    {
        if ( empty( $size ) ) {
            return FALSE;
        }
        $this->persistAndFlush( $size );

        $manager = $this->getImageManager();
        $result  = $manager->addImageSize( $size, FALSE );

        return $result;
    }

    /**
     * @return null
     */
    public function getSmallestSize()
    {
        $sizes  = $this->getRepo()->findAll();
        $return = NULL;
        $prev   = NULL;
        foreach ( $sizes as $size ) {
            $area = $size->getArea();
            if ( $prev === NULL || $area < intval( $prev ) ) {
                $return = $size;
                $prev   = $area;
            }
        }

        return $return;
    }

    public function getAvailableImageSizes( $sorted = FALSE, $reverse = TRUE )
    {
        $sizes = $this->getRepo()->findBy( array( 'active' => TRUE ) );
        if ( !$sorted ) {
            return $sizes;
        }

        $sorted = array();
        foreach ( $sizes as $size ) {
            $area = $size->getArea();
            if ( !isset( $sorted[ $area ] ) ) {
                $sorted[ $area ] = array();
            }
            $sorted[ $area ][] = $size;
        }

        ksort( $sorted );

        $return = array();
        foreach ( $sorted as $sizes ) {
            foreach ( $sizes as $size ) {
                $return[] = $size;
            }
        }

        if ( $reverse ) {
            $return = array_reverse( $return );
        }

        return $return;
    }

}