<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use CoreSys\CoreBundle\Entity\AdminMenu;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;

/**
 * Class AdminMenuManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.admin_menu", parent="core_sys_core.manager.base")
 */
class AdminMenuManager extends BaseManager
{

    /**
     * @var
     */
    private $adminMenusRepo;

    /**
     * @var int
     */
    private $adminMenusCount = -1;

    /**
     * @var ArrayCollection
     */
    private $adminMenus = array();

    public function __construct( $baseController, $container )
    {
        parent::__construct( $baseController, $container );
        $this->debug = FALSE;
    }

    public function getAdminMenusRepo()
    {
        if ( !empty( $this->adminMenusRepo ) ) {
            return $this->adminMenusRepo;
        }

        return $this->adminMenusRepo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:AdminMenu' );
    }

    public function getAdminMenusCount( $force = FALSE )
    {
        if ( !$force ) {
            if ( intval( $this->adminMenusCount ) > 0 ) {
                return $this->adminMenusCount;
            }
        }

        return $this->adminMenusCount = $this->getAdminMenusRepo()->getMenusCount();
    }

    public function getAdminMenus( $force = FALSE )
    {
        if ( !$force ) {
            if ( !empty( $this->adminMenus ) && count( $this->adminMenus ) > 0 ) {
                return $this->adminMenus;
            }
        }

        return $this->adminMenus = $this->getAdminMenusRepo()->findAll();
    }

    public function getMenuEntityByArray( array &$menu )
    {
        return $this->getMenuEntity( $menu[ 'namespace' ], $menu[ 'bundle' ], $menu[ 'file' ] );
    }

    public function getMenuEntity( $namespace, $bundle, $file )
    {
        $entity = $this->getAdminMenusRepo()->findOneBy( array(
                                                             'namespace' => $namespace,
                                                             'bundle'    => $bundle,
                                                             'file'      => $file
                                                         ) );

        return !empty( $entity ) ? $entity : NULL;
    }

    public function checkAdminMenus()
    {
        $this->debug = false;
        $this->log( 'Checking Admin Menus' );
        $foundMenus = array();
        $this->checkSourceForAdminMenus( $foundMenus );
        $this->log( '<hr />' );
        $this->validateAdminMenus( $foundMenus );
        $this->log( 'Complete Validating' );
    }

    protected function validateAdminMenus( array &$menus )
    {
        $this->log( print_r( $menus, TRUE ) );
        $this->log( '<hr />' );
        foreach ( $menus as $menu ) {
            $entity = $this->getMenuEntityByArray( $menu );
            if ( empty( $entity ) ) {
                $this->addMenuToDatabase( $menu );
            } else {
                $this->log( 'Menu is already in the database' );
                $this->adminMenus[] = $entity;
            }
            $this->log( '<hr />' );
        }

        foreach ( $this->getAdminMenusRepo()->findAll() as $menu ) {
            $menuArray = array(
                'namespace' => $menu->getNamespace(),
                'bundle'    => $menu->getBundle(),
                'file'      => $menu->getFile()
            );
            $entity    = $this->getMenuEntityByArray( $menuArray );
            if ( empty( $entity ) ) {
                // does not exist
                $this->log( 'Menu Entity NOT FOUND' );
                $this->getBaseController()->removeAndFlush( $entity );
            }
        }

        $this->validateMenuParents( $menus );
    }

    protected function validateMenuParents( array &$menus )
    {
        foreach ( $menus as $menu ) {
            if ( isset( $menu[ 'parent' ] ) ) {
                $parent = $menu[ 'parent' ];
                if ( is_array( $parent ) ) {
                    $this->log( 'Processing Parent' );
                    $entity = $this->getMenuEntityByArray( $parent );
                    if ( !empty( $parent ) ) {
                        $this->log( 'Found parent' );
                        $menuEntity = $this->getMenuEntityByArray( $menu );
                        if ( !empty( $menuEntity ) ) {
                            $menuParent = $menuEntity->getParent();
                            if ( $menuParent === NULL ) {
                                $this->log( 'Have menu entity' );
                                $entity->addChild( $menuEntity );
                                $this->getBaseController()->persistAndFlush( $entity );
                            }
                        }
                    }
                }
            }
        }
    }

    protected function addMenuToDatabase( array &$menu )
    {
        $this->log( 'Adding Menu to database' );
        $this->getAdminMenusCount();
        $entity = new AdminMenu();
        $entity->setNamespace( $menu[ 'namespace' ] )
               ->setBundle( $menu[ 'bundle' ] )
               ->setFile( $menu[ 'file' ] )
               ->setPosition( $this->adminMenusCount++ );
        $this->adminMenus[] = $entity;
        $this->getBaseController()->persistAndFlush( $entity );
    }

    protected function hasMenu( array &$menu )
    {

    }

    protected function checkSourceForAdminMenus( array &$menus )
    {
        $srcFolder = $this->getBaseController()->getRootFolder()
                     . DIRECTORY_SEPARATOR . 'src';
        $this->log( 'Source Folder: ' . $srcFolder );
        if ( is_dir( $srcFolder ) ) {
            $dir = opendir( $srcFolder );
            if ( $dir ) {
                while ( FALSE !== ( $namespace = readdir( $dir ) ) ) {
                    if ( $namespace !== '.' && $namespace !== '..' ) {
                        $namespaceFolder = $srcFolder . DIRECTORY_SEPARATOR . $namespace;
                        if ( is_dir( $namespaceFolder ) ) {
                            $this->checkNamespaceForAdminMenus( $namespace, $menus );
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }

    protected function checkNamespaceForAdminMenus( &$namespace, array &$menus )
    {
        $namespaceFolder = implode( DIRECTORY_SEPARATOR, array(
            $this->getBaseController()->getRootFolder(),
            'src', $namespace
        ) );
        $this->log( 'Namespace Folder: ' . $namespaceFolder );
        if ( is_dir( $namespaceFolder ) ) {
            $dir = opendir( $namespaceFolder );
            if ( $dir ) {
                while ( FALSE !== ( $bundle = readdir( $dir ) ) ) {
                    if ( $bundle !== '.' && $bundle !== '..' && strstr( $bundle, 'Bundle' ) ) {
                        $bundleFolder = $namespaceFolder . DIRECTORY_SEPARATOR . $bundle;
                        if ( is_dir( $bundleFolder ) ) {
                            $this->checkBundleForAdminMenus( $namespace, $bundle, $menus );
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }

    protected function checkBundleForAdminMenus( &$namespace, &$bundle, array &$menus )
    {
        $menusFolder = implode( DIRECTORY_SEPARATOR, array(
            $this->getBaseController()->getRootFolder(),
            'src', $namespace, $bundle,
            'Resources', 'views', 'AdminMenu'
        ) );
        $this->log( 'Menus Folder: ' . $menusFolder );
        if ( is_dir( $menusFolder ) ) {
            $dir = opendir( $menusFolder );
            if ( $dir ) {
                while ( FALSE !== ( $file = readdir( $dir ) ) ) {
                    if ( $file !== '.' && $file !== '..' ) {
                        $filename = $menusFolder . DIRECTORY_SEPARATOR . $file;
                        if ( is_File( $filename ) ) {
                            $this->log( 'Menu File: ' . $file );
                            $menu = array(
                                'namespace' => $namespace,
                                'bundle'    => $bundle,
                                'file'      => $file
                            );

                            $contents   = file( $filename );
                            $parentLine = $contents[ 0 ];
                            if ( !empty( $parentLine ) ) {
                                if ( strstr( $parentLine, 'parent' ) ) {
                                    $parent           = preg_replace( '/[\}\{\#\s]+/', '', $parentLine );
                                    $parent           = str_replace( 'parent', '', $parent );
                                    $parts            = explode( ':', $parent );
                                    $menu[ 'parent' ] = array(
                                        'namespace' => $parts[ 0 ],
                                        'bundle'    => $parts[ 1 ],
                                        'file'      => $parts[ 2 ]
                                    );
                                    $this->log( 'FOund Parent: ' . $parts[ 0 ] . ':' . $parts[ 1 ] . ':' . $parts[ 2 ] );
                                }
                            }

                            $menus[] = $menu;
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }
}

class AdminMenuManager2 extends BaseManager
{

    /**
     * @var ObjectRepository
     */
    private $adminMenusRepo;

    /**
     * @var integer
     */
    private $adminMenusCount = -1;

    /**
     * @var array
     */
    private $adminMenus = array();

    public function __construct( $baseController, $container )
    {
        parent::__construct( $baseController, $container );
        $this->debug = TRUE;
    }

    /**
     * @return ObjectRepository
     */
    public function getAdminMenusRepo()
    {
        if ( !empty( $this->adminMenusRepo ) ) {
            return $this->adminMenusRepo;
        }

        return $this->adminMenusRepo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:AdminMenu' );
    }

    /**
     * @param bool $force
     *
     * @return int
     */
    public function getAdminMenusCount( $force = FALSE )
    {
        if ( !$force ) {
            if ( intval( $this->adminMenusCount ) > 0 ) {
                return $this->adminMenusCount;
            }
        }

        return $this->adminMenusCount = $this->getAdminMenusRepo()->getMenusCount();
    }

    /**
     * @return array
     */
    public function getAdminMenus( $force = FALSE )
    {
        if ( !$force ) {
            if ( !empty( $this->adminMenus ) && count( $this->adminMenus ) > 0 ) {
                return $this->adminMenus;
            }
        }

        return $this->adminMenus = $this->getAdminMenusRepo()->findAll();
    }

    /**
     *
     */
    public function checkAdminMenus()
    {
        $this->debug = TRUE;
        $this->log( 'Check Admin Menus' );
        $foundMenus = array();
        $this->checkSourceForAdminMenus( $foundMenus );
        $this->log( 'Complete Checking Source' );
        $this->validateAdminMenus( $foundMenus );
        $this->log( 'Complete Validating' );
        exit;
    }

    /**
     * @param $menu
     *
     * @return bool
     */
    public function isMenuInDatatabase( $menu )
    {
        $menuObj = $this->getAdminMenusRepo()->findBy( $menu );

        return !empty( $menuObj );
    }

    /**
     * @param $menus
     */
    private function validateAdminMenus( &$menus )
    {
        $this->log( '<hr>' );
        $this->log( $menus );
        $this->log( '<hr>' );
        foreach ( $menus as $menu ) {
            $has = $this->isMenuInDatatabase( $menu );
            $this->log( 'Has Menu? ' . ( $has ? 'YES' : 'NO' ) );
            if ( !$has ) {
                $this->addMenuToDatabase( $menu );
            }
        }

        foreach ( $this->getAdminMenus() as $key => $menuObj ) {
            $has = FALSE;
            foreach ( $menus as $menu ) {
                $namespace = $menuObj->getNamespace();
                $bundle    = $menuObj->getBundle();
                if ( $menu[ 'namespace' ] === $namespace && $menu[ 'bundle' ] === $bundle ) {
                    $has = TRUE;
                    break;
                }
                if ( !$has ) {
                    unset( $this->adminMenus[ $key ] );
                    $this->remove( $menuObj );
                }
            }
        }

        $this->flush();

//        $this->validateAdminMenuParents( $menus );
    }

    /**
     * @param $menus
     */
    private function validateAdminMenuParents( &$menus )
    {
        $this->log( "Validating Parents" );
        foreach ( $menus as $menu ) {
            $parent = isset( $menu[ 'parent' ] ) ? $menu[ 'parent' ] : NULL;
            if ( !empty( $parent ) ) {
                $this->log( 'Found Parent: ' . print_r( $parent, TRUE ) );
                $parentEntity = $this->getAdminMenusRepo()->findOneBy( $parent );
                if ( !empty( $parentEntity ) ) {
                    $this->log( 'Parent entity found: ' . $parentEntity->getId() );
                    $entity = $this->getAdminMenusRepo()->findOneBy( array(
                                                                         'namespace' => $menu[ 'namespace' ],
                                                                         'bundle'    => $menu[ 'bundle' ],
                                                                         'file'      => $menu[ 'file' ]
                                                                     ) );
                    if ( !empty( $entity ) ) {
                        $this->log( 'Found Entity: ' . $entity->getId() );
                        $parentEntity->addChild( $entity );
                        $entity->setParent( $parentEntity );
                        $this->getBaseController()->persist( $entity );
                        $this->getBaseController()->persist( $parentEntity );
                        $this->getBaseController()->flush();
                    }
                }
            }
        }
    }

    /**
     * @param $menu
     */
    private function addMenuToDatabase( $menu )
    {
        $this->log( print_r( $menu, TRUE ) );
        $this->getAdminMenusCount();
        $menuObj = new AdminMenu();
        $menuObj->setNamespace( $menu[ 'namespace' ] )
                ->setBundle( $menu[ 'bundle' ] )
                ->setFile( $menu[ 'file' ] )
                ->setPosition( $this->adminMenusCount++ );
        $this->log( 'Added Menu: ' );
        $this->log( $menu );
        $this->getBaseController()->persistAndFlush( $menuObj );
        $this->adminMenus[] = $menuObj;
    }

    /**
     * @param $menus
     */
    private function checkSourceForAdminMenus( &$menus )
    {
        $srcFolder = $this->getBaseController()->getRootFolder()
                     . DIRECTORY_SEPARATOR . 'src';
        $this->log( 'Source Folder: ' . $srcFolder );
        if ( is_dir( $srcFolder ) ) {
            $dir = opendir( $srcFolder );
            if ( $dir ) {
                while ( FALSE !== ( $namespace = readdir( $dir ) ) ) {
                    if ( $namespace !== '.' && $namespace !== '..' ) {
                        $namespaceFolder = $srcFolder . DIRECTORY_SEPARATOR . $namespace;
                        if ( is_dir( $namespaceFolder ) ) {
                            $this->checkNamespaceForAdminMenus( $namespace, $menus );
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }

    /**
     * @param $namespace
     * @param $menus
     */
    private function checkNamespaceForAdminMenus( $namespace, &$menus )
    {
        $namespaceFolder = implode( DIRECTORY_SEPARATOR, array(
            $this->getBaseController()->getRootFolder(),
            'src',
            $namespace
        ) );
        $this->log( 'NamespaceFolder: ' . $namespaceFolder );
        if ( is_dir( $namespaceFolder ) ) {
            $dir = opendir( $namespaceFolder );
            if ( $dir ) {
                while ( FALSE !== ( $bundle = readdir( $dir ) ) ) {
                    if ( $dir !== '.' && $dir !== '..' && strstr( $bundle, 'Bundle' ) ) {
                        $this->checkBundleForAdminMenus( $namespace, $bundle, $menus );
                    }
                }
                closedir( $dir );
            }
        }
    }

    /**
     * @param $namespace
     * @param $bundle
     * @param $menus
     */
    private function checkBundleForAdminMenus( $namespace, $bundle, &$menus )
    {
        $menuFolder = implode( DIRECTORY_SEPARATOR, array(
            $this->getBaseController()->getRootFolder(),
            'src',
            $namespace,
            $bundle,
            'Resources', 'views', 'AdminMenu'
        ) );
        $this->log( 'MenuFolder: ' . $menuFolder );

        if ( is_dir( $menuFolder ) ) {
            $dir = opendir( $menuFolder );
            if ( $dir ) {
                while ( FALSE !== ( $file = readdir( $dir ) ) ) {
                    if ( $file != '.' && $file !== '..' ) {
                        $filename = $menuFolder . DIRECTORY_SEPARATOR . $file;
                        if ( is_file( $filename ) ) {
                            $this->log( 'Found Menu: ' . $file );
                            $menu = array(
                                'namespace' => $namespace,
                                'bundle'    => $bundle,
                                'file'      => $file
                            );

//                            $contents = file( $filename );
//                            $line1    = $contents[ 0 ];
//                            if ( strstr( $line1, 'parent' ) ) {
//                                $parent           = preg_replace( '/[\{\}\#\s]+/', '', $line1 );
//                                $parent           = str_replace( 'parent', '', $parent );
//                                $parts            = explode( ':', $parent );
//                                $menu[ 'parent' ] = array(
//                                    'namespace' => $parts[ 0 ],
//                                    'bundle'    => $parts[ 1 ],
//                                    'file'      => $parts[ 2 ]
//                                );
//                            }

                            $menus[] = $menu;
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }
}