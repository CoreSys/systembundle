<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseManager
 * @package CoreSys\CoreBundle\Manager
 */
class BaseManager
{

    /**
     * @var BaseController
     */
    protected $baseController;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var bool
     */
    protected $debug = FALSE;

    /**
     * BaseManager constructor.
     *
     * @param BaseController|NULL     $baseController
     * @param ContainerInterface|NULL $container
     */
    public function __construct( BaseController $baseController = NULL, ContainerInterface $container = NULL )
    {
        if ( !empty( $baseController ) ) {
            $this->setBaseController( $baseController );
        }
        if ( !empty( $container ) ) {
            $this->setContainer( $container );
        }
        $this->getBaseController();
        $this->getContainer();
    }

    /**
     * Get debug
     *
     * @return boolean
     */
    public function getDebug()
    {
        return $this->debug === TRUE;
    }

    /**
     * Set Debug
     *
     * @param boolean $debug
     *
     * @return BaseManager
     */
    public function setDebug( $debug = TRUE )
    {
        $this->debug = $debug === TRUE;

        return $this;
    }

    public function log( $message )
    {
        if ( $this->debug ) {
            if ( is_array( $message ) || is_object( $message ) ) {
                $message = print_r( $message, TRUE );
            }

            echo '<br>' . $message;
        }
    }

    /**
     * Get container
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set Container
     *
     * @param mixed $container
     *
     * @return BaseManager
     */
    public function setContainer( $container = NULL )
    {
        $this->container = $container;

        return $this;
    }

    /**
     * @return BaseController
     */
    public function getBaseController()
    {
        return $this->baseController;
    }

    /**
     * Set BaseController
     *
     * @param BaseController $baseController
     *
     * @return BaseManager
     */
    public function setBaseController( $baseController = NULL )
    {
        if ( $baseController !== NULL ) {
            $this->baseController = $baseController;
        }

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call( $name, $arguments )
    {
        $this->getBaseController();
        $this->getContainer();
        if ( is_object( $this->baseController ) && method_exists( $this->baseController, $name ) ) {
            return call_user_func_array( array(
                                             $this->baseController,
                                             $name
                                         ), $arguments );
        }

        throw new \Exception( 'Method `' . $name . '` not found.' );
    }

    /**
     * @param     $folder
     * @param int $seconds
     */
    public function cleanupFolder( $folder, $seconds = 600 )
    {
        if ( is_dir( $folder ) ) {
            $check = time() - $seconds;
            $dir   = opendir( $folder );
            while ( FALSE !== ( $file = readdir( $dir ) ) ) {
                if ( $file !== '.' && $file !== '..' ) {
                    $filename = $folder . DIRECTORY_SEPARATOR . $file;
                    if ( is_dir( $filename ) ) {
                        $this->cleanupFolder( $folder, $seconds );
                    } else {
                        $mtime = filemtime( $filename );
                        if ( $mtime < $check ) {
                            unlink( $filename );
                        }
                    }
                }
            }
            closedir( $dir );
        }
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->getBaseController()->getUser();
    }
}