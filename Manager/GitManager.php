<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use JMS\DiExtraBundle\Annotation as DI;
use SensioLabs\Security\Exception\RuntimeException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

/**
 * Class GitManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.git", parent="core_sys_core.manager.base")
 */
class GitManager extends BaseManager
{

    /**
     * @var array
     */
    protected $envopts = array();

    /**
     * @var bool
     */
    protected $bare = FALSE;

    /**
     * @var
     */
    private $repositoryPath;

    /**
     * @var string
     */
    private $remote = 'origin';

    /**
     * @var string
     */
    private $bin = 'git';

    /**
     * @return string
     */
    public function getRemote()
    {
        return $this->remote;
    }

    /**
     * @param $remote
     *
     * @return $this
     */
    public function setRemote( $remote )
    {
        $this->remote = $remote;

        return $this;
    }

    /**
     * @return string
     */
    public function getRepositoryPath()
    {
        if ( !empty( $this->repositoryPath ) ) {
            return $this->repositoryPath;
        }

        return $this->repositoryPath = dirname( $this->getBaseController()->getAppFolder() );
    }

    /**
     * Get bin
     *
     * @return string
     */
    public function getBin()
    {
        return $this->bin;
    }

    /**
     * Set Bin
     *
     * @param string $bin
     *
     * @return GitManager
     */
    public function setBin( $bin = NULL )
    {
        $this->bin = $bin;

        return $this;
    }

    /**
     * @return GitManager
     */
    public function windowsMode()
    {
        return $this->setBin( 'git' );
    }

    /**
     * @return bool
     */
    public function testGit()
    {
        $descriptorSpec = array(
            1 => array( 'pipe', 'w' ),
            2 => array( 'pipe', 'w' )
        );
        $pipes          = array();
        $resource       = proc_open( $this->getBin(), $descriptorSpec, $pipes );

        $stdOut = stream_get_contents( $pipes[ 1 ] );
        $stdErr = stream_get_contents( $pipes[ 2 ] );
        foreach ( $pipes as $pipe ) {
            fclose( $pipe );
        }

        $status = trim( proc_close( $resource ) );

        return $status != 127;
    }

    /**
     * @param $command
     *
     * @return string
     */
    public function run( $command, $callback = NULL )
    {
        return $this->runCommand( $this->getBin() . ' ' . $command, $callback );
    }

    /**
     * @param bool $html
     *
     * @return mixed|string
     */
    public function status( $html = FALSE )
    {
        $msg = $this->run( 'status' );
        if ( !!$html ) {
            $msg = str_replace( "\n", '<br />', $msg );
        }

        return $msg;
    }

    /**
     * @param string $files
     *
     * @return string
     */
    public function add( $files = '*' )
    {
        if ( is_array( $files ) ) {
            $files = '"' . implode( '" "', $files ) . '"';
        }

        return $this->run( 'add ' . $files . ' -v' );
    }

    /**
     * @param string $files
     * @param bool   $cached
     *
     * @return string
     */
    public function remove( $files = '*', $cached = FALSE )
    {
        if ( is_array( $files ) ) {
            $files = '"' . implode( '" "', $files ) . '"';
        }

        return $this->run( 'rm ' . ( !!$cached ? '--cached ' : '' ) . $files );
    }

    /**
     * @param string $message
     * @param bool   $commitAll
     *
     * @return string
     */
    public function commit( $message = 'Update' )
    {
        return $this->run( 'commit -am ' . escapeshellarg( $message ), function ( $output ) {
            // whether or not there was stuff to commit,
            // this needs to pass
            return FALSE;
        } );
    }

    /**
     * @param bool $dirs
     * @param bool $force
     *
     * @return string
     */
    public function clean( $dirs = FALSE, $force = FALSE )
    {
        return $this->run( 'clean' . ( $force ? ' -f' : '' ) . ( $dirs ? ' -d' : '' ) );
    }

    /**
     * @param null $branch
     *
     * @return string
     */
    public function branch( $branch = NULL )
    {
        return $this->run( 'branch ' . $branch );
    }

    /**
     * @param      $branch
     * @param bool $force
     *
     * @return string
     */
    public function deleteBranch( $branch, $force = FALSE )
    {
        return $this->run( 'branch ' . ( $force ? '-D' : '-d' ) . ' ' . $branch );
    }

    /**
     * @param bool $keepAsterisk
     *
     * @return array
     */
    public function branches( $keepAsterisk = FALSE )
    {
        $branches = explode( "\n", $this->run( 'branch' ) );
        foreach ( $branches as $i => &$branch ) {
            $branch = trim( $branch );
            if ( !$keepAsterisk ) {
                $branch = trim( str_replace( '*', '', $branch ) );
            }
            if ( $branch == '' ) {
                unset( $branches[ $i ] );
            }
        }

        return $branches;
    }

    /**
     * @param bool $keepAsterisk
     *
     * @return mixed|string
     */
    public function currentBranch( $keepAsterisk = FALSE )
    {
        $branches = $this->branches( TRUE );
        $current  = preg_grep( '/^\*/', $branches );
        reset( $current );
        if ( $keepAsterisk ) {
            return current( $current );
        } else {
            return trim( str_replace( '*', '', current( $current ) ) );
        }
    }

    /**
     * @param      $branch
     * @param bool $create
     *
     * @return string
     */
    public function checkout( $branch, $create = FALSE )
    {
        return $this->run( 'checkout ' . ( !!$create ? '-b ' : '' ) . $branch );
    }

    /**
     * @param $branch
     *
     * @return string
     */
    public function merge( $branch )
    {
        return $this->run( 'merge ' . $branch . ' --no-ff' );
    }

    /**
     * @return string
     */
    public function fetch()
    {
        return $this->run( 'fetch' );
    }

    /**
     * @param $branch
     *
     * @return string
     */
    public function push( $branch, $first = FALSE )
    {
        if ( $first ) {
            return $this->run( 'push -u ' . $this->getRemote() . ' ' . $branch );
        }

        return $this->run( 'push --tags ' . $this->getRemote() . ' ' . $branch );
    }

    /**
     * @param $branch
     *
     * @return string
     */
    public function pull( $branch )
    {
        return $this->run( 'pull ' . $this->getRemote() . ' ' . $branch );
    }

    /**
     * @param null $format
     *
     * @return string
     */
    public function log( $format = NULL )
    {
        if ( $format === NULL ) {
            return $this->run( 'log' );
        } else {
            return $this->run( 'log --pretty=format:"' . $format . '""' );
        }
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function setEnv( $key, $value )
    {
        $this->envopts[ $key ] = $value;

        return $this;
    }

    public function hasBranch( $branch )
    {
        $branches = $this->branches();

        return in_array( $branch, $branches );
    }

    /**
     * @param $command
     *
     * @return string
     */
    protected function runCommand( $command, $callback = NULL )
    {
        $descriptorSpec = array(
            1 => array( 'pipe', 'w' ),
            2 => array( 'pipe', 'w' )
        );
        $pipes          = array();

        if ( count( $_ENV ) === 0 ) {
            $env = NULL;
            foreach ( $this->envopts as $k => $v ) {
                putenv( sprintf( "%s=%s", $k, $v ) );
            }
        } else {
            $env = array_merge( $_ENV, $this->envopts );
        }

        $cwd      = $this->getRepositoryPath();
        $resource = proc_open( $command, $descriptorSpec, $pipes, $cwd, $env );

        $stdOut = stream_get_contents( $pipes[ 1 ] );
        $stdErr = stream_get_contents( $pipes[ 2 ] );

        foreach ( $pipes as $pipe ) {
            fclose( $pipe );
        }

        $status = trim( proc_close( $resource ) );

        if ( is_callable( $callback ) ) {
            $status = $callback( $stdOut );
        }

        if ( $status ) {
            throw new Exception( $stdErr );
        }

        return $stdOut;
    }
}