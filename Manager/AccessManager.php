<?php
/**
 * Created by PhpStorm.
 * User: jmccreig
 * Date: 6/14/2016
 * Time: 8:44 PM
 */

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use CoreSys\CoreBundle\Entity\Access;
use CoreSys\CoreBundle\Entity\Role;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AccessManager
 * @package CoreSys\CoreBundle\Manager
 */
class AccessManager extends ConfigManager
{

    /**
     * @var string
     */
    protected $file;

    public function __construct( BaseController $baseController, ContainerInterface $container )
    {
        parent::__construct( $baseController, $container );
        $this->file = $this->getConfigurationFile( 'access.yml' );
    }

    /**
     * Add a role to an access entity
     *
     * @param Access $access
     * @param        $role
     */
    public function addRoleToAccess( Access $access, $role )
    {
        if ( !$role instanceof Role ) {
            $role = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Role' )
                         ->findOneBy( array( 'roleName' => $role ) );
        }

        if ( $role instanceof Role ) {
            $access->addRole( $role );
            $this->getBaseController()->persist( $access );
            $this->getBaseController()->persist( $role );
            $this->getBaseController()->flush();
        }
    }

    /**
     * @param bool $return_yml
     *
     * @return array|null
     */
    function getDataStructure( $return_yml = FALSE )
    {
        $return = array();
        $repo   = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Access' );
        foreach ( $repo->findAll() as $access ) {
            if ( $access->getActive() ) {
                $data     = $access->getData();
                $return[] = $data;
            }
        }

        if ( count( $return ) == 0 ) {
            return NULL;
        }

        return $return_yml ? Yaml::dump( $return, 1 ) : $return;
    }
}