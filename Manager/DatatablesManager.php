<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Annotation\Datatables\Action;
use CoreSys\CoreBundle\Annotation\Datatables\Ajax;
use CoreSys\CoreBundle\Annotation\Datatables\Column;
use CoreSys\CoreBundle\Annotation\Datatables\ColumnDef;
use CoreSys\CoreBundle\Annotation\Datatables\Renderer;
use CoreSys\CoreBundle\Annotation\Datatables\Renderer\Actions;
use Doctrine\Common\Annotations\AnnotationReader;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class DatatablesManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.datatables", parent="core_sys_core.manager.base")
 */
class DatatablesManager extends BaseManager
{

    /**
     * @var AnnotationReader
     */
    protected $reader;

    /**
     * @var \ReflectionClass
     */
    protected $reflectionClass;

    /**
     * @var array
     */
    protected $options = array();

    /**
     * @var string
     */
    protected $tableClass = 'CoreSys\CoreBundle\Annotation\Datatables\Table';

    /**
     * @var string
     */
    protected $actionClass = 'CoreSys\CoreBundle\Annotation\Datatables\Action';

    /**
     * @var string
     */
    protected $columnClass = 'CoreSys\CoreBundle\Annotation\Datatables\Column';

    /**
     * @var string
     */
    protected $columnDefClass = 'CoreSys\CoreBundle\Annotation\Datatables\ColumnDef';

    /**
     * @var array
     */
    protected $tableData = array();

    /**
     * @var array
     */
    protected $columnsData = array();

    /**
     * @var array
     */
    protected $columnDefsData = array();

    /**
     * @var array
     */
    protected $tableActions = array();

    /**
     * @var array
     */
    protected $rowActions = array();

    /**
     * @var bool
     */
    protected $hasTableActions = FALSE;

    /**
     * @var bool
     */
    protected $hasRowActions = FALSE;

    /**
     * @var array
     */
    protected $renderers = array();

    /**
     * @return AnnotationReader
     */
    public function getReader()
    {
        if ( !empty( $this->reader ) ) {
            return $this->reader;
        }

        return $this->reader = new AnnotationReader();
    }

    /**
     * @param $entity
     *
     * @return array
     */
    public function getDatatablesOptions( $entity )
    {
        $this->options         = array();
        $this->reflectionClass = new \ReflectionClass( get_class( $entity ) );
        $this->parseDatatablesTable();

        $this->options                      = $this->tableData->getOptions();
        $this->options[ 'hasTableActions' ] = $this->hasTableActions;
        $this->options[ 'hasRowActions' ]   = $this->hasRowActions;
        $this->options[ 'renderers' ]       = $this->renderers;

        return $this->options;
    }

    /**
     *
     */
    public function parseDatatablesTable()
    {
        $this->tableData = array();
        $this->tableData = $this->getReader()->getClassAnnotation( $this->reflectionClass, $this->tableClass );

        if ( !empty( $this->tableData ) ) {

            $apiBase = !empty( $this->tableData->apiBase ) ? $this->tableData->apiBase : 'api_post';

            if ( empty( $this->tableData->createUrl ) ) {
                $this->tableData->createUrl = $apiBase . 'post_' . strtolower( $this->tableData->value );
            }
            if ( empty( $this->tableData->editUrl ) ) {
                $this->tableData->editUrl = $apiBase . 'put_' . strtolower( $this->tableData->value );
            }
            if ( empty( $this->tableData->deleteUrl ) ) {
                $this->tableData->deleteUrl = $apiBase . 'delete_' . strtolower( $this->tableData->value );
            }

            $this->parseDatatablesColumns();
            $this->parseDatatablesColumnDefs();
            $this->parseDatatablesTableActions();
            $this->parseDatatablesRowActions();
            $this->parseAjaxData();
        }

        $this->hasTableActions = count( $this->tableActions ) > 0;
        $this->hasRowActions   = count( $this->rowActions ) > 0;

        if ( $this->hasRowActions ) {
            $this->addRowActionsColumn();
        }
    }

    /**
     * Parse the ajax data
     */
    public function parseAjaxData()
    {
        $this->ajaxData = array();
        if ( !empty( $this->tableData->ajax ) ) {
            if ( $this->tableData->ajax instanceof Ajax ) {
                if ( $this->tableData->ajax->value === TRUE ) {
                    $this->tableData->serverSide = TRUE;
                    $this->ajaxData              = array(
                        'type' => strtoupper( $this->tableData->ajax->type ),
                        'url'  => $this->getBaseController()->generateUrl( $this->tableData->ajax->url )
                    );
                } else {
                    $this->tableData->serverSide = FALSE;
                }
            }
        }
        $this->tableData->ajax = $this->ajaxData;
    }

    /**
     * Add and setup the actions column for the table rows
     */
    public function addRowActionsColumn()
    {
        $column = array(
            'data'       => NULL,
            'title'      => 'Actions',
            'searchable' => FALSE,
            'orderable'  => FALSE,
            'width'      => 150,
            'className'  => 'all text-center text-xs-center action-column',
        );

        $renderer           = new Actions();
        $renderer->resource = $this->tableData->value;
        $rendererParameters = array( 'rowActions' => array() );

        $rendererParameters[ 'tableData' ] = $this->tableData;

        $template                            = $this->getBaseController()->renderView( $renderer->template, $rendererParameters );
        $this->renderers[ $renderer->value ] = $template;
        $column[ 'render' ]                  = $renderer->value;

        $this->tableData->columns[] = $column;
    }

    /**
     *
     */
    public function parseDatatablesColumns()
    {
        $this->columnsData = array();
        if ( is_array( $this->tableData->columns ) ) {
            foreach ( $this->tableData->columns as $column ) {
                if ( $column instanceof Column ) {
                    $this->parseDatatablesColumnRenderer( $column );
                    $this->columnsData[] = $column->getOptions();
                }
            }
            $this->tableData->columns = $this->columnsData;
        } else {
            $this->tableData->columns = NULL;
        }
    }

    public function parseDatatablesColumnRenderer( &$column )
    {
        $renderer = isset( $column->render ) ? $column->render : NULL;
        if ( $renderer instanceof Renderer ) {
            $template   = $renderer->getTemplate();
            $parameters = $renderer->getParameters();

            try {
                $this->renderers[ $renderer->value ] = $this->getBaseController()
                                                            ->renderView( $template, array( 'tableData' => $this->tableData, 'parameters' => $parameters ) );
                $column->render                      = $renderer->value;
            } catch ( \Exception $e ) {
                echo '<br>' . $template . ': ' . $e->getMessage();
                $column->render = NULL;
            }
        }
    }

    /**
     *
     */
    public function parseDatatablesColumnDefs()
    {
        $this->columnDefsData = array();
        if ( is_array( $this->tableData->columnDefs ) ) {
            foreach ( $this->tableData->columnDefs as $columnDef ) {
                if ( $columnDef instanceof ColumnDef ) {
                    $this->columnDefsData[] = $columnDef;
                }
            }
            $this->tableData->columnDefs = $this->columnDefsData;
        } else {
            $this->tableData->columnDefs = NULL;
        }
    }

    /**
     *
     */
    public function parseDatatablesTableActions()
    {
        $this->tableActions = array();
        if ( is_array( $this->tableData->tableActions ) ) {
            foreach ( $this->tableData->tableActions as $action ) {
                if ( $action instanceof Action ) {
                    $action->resource     = $this->tableData->value;
                    $this->tableActions[] = $action->getOptions();
                }
            }
            $this->tableData->tableActions = $this->tableActions;
        } else {
            $this->tableData->tableActions = NULL;
        }
    }

    /**
     *
     */
    public function parseDatatablesRowActions()
    {
        $this->rowActions = array();
        if ( is_array( $this->tableData->rowActions ) ) {
            foreach ( $this->tableData->rowActions as $action ) {
                if ( $action instanceof Action ) {
                    $action->resource   = $this->tableData->value;
                    $this->rowActions[] = $action->getOptions();
                }
            }
            $this->tableData->rowActions = $this->rowActions;
        } else {
            $this->tableData->rowActions = NULL;
        }
    }
}