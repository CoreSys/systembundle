<?php

namespace CoreSys\CoreBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class NoteManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.notes", parent="core_sys_core.manager.base")
 */
class NoteManager extends BaseManager
{

    /**
     * @var
     */
    private $notesRepo;

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getNotesRepo()
    {
        if ( !empty( $this->notesRepo ) ) {
            return $this->notesRepo;
        }

        return $this->notesRepo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Note' );
    }

    /**
     * @return array
     */
    public function getAllTodos()
    {
        return $this->getNotesRepo()->findBy( array( 'type' => 'todo' ), array( 'createdAt' => 'desc' ) );
    }

    /**
     * @return array
     */
    public function getAllComments()
    {
        return $this->getNotesRepo()->findBy( array( 'type' => 'comment' ), array( 'createdAt' => 'desc' ) );
    }

    /**
     * @return array
     */
    public function getAllNotes()
    {
        return $this->getNotesRepo()->findBy( array( 'type' => 'note' ), array( 'createdAt' => 'desc' ) );
    }

    /**
     * @param Note $note
     *
     * @return NoteManager
     */
    public function updateNote( Note $note )
    {
        $note->setType( 'note' );

        return $this->update( $note );
    }

    /**
     * @param Note $note
     *
     * @return NoteManager
     */
    public function updateComment( Note $note )
    {
        $note->setType( 'comment' );

        return $this->update( $note );
    }

    /**
     * @param Note $note
     *
     * @return NoteManager
     */
    public function updateTodo( Note $note )
    {
        $note->setType( 'todo' );

        return $this->update( $note );
    }

    /**
     * @param Note $note
     *
     * @return $this
     */
    protected function update( Note $note )
    {
        $this->getBaseController()->persistAndFlush( $note );

        return $this;
    }
}