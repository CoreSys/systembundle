<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * AdminMenu
 *
 * @ORM\Table(name="core_admin_menu")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\AdminMenuRepository")
 */
class AdminMenu extends BaseEntity
{

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="namespace", type="string", length=64)
     */
    private $namespace;

    /**
     * @var string
     *
     * @ORM\Column(name="bundle", type="string", length=64)
     */
    private $bundle;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=64, nullable=true)
     */
    private $file;

    /**
     * @var AdminMenu
     *
     * @ORM\ManyToOne(targetEntity="AdminMenu", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\AdminMenu")
     * @JMS\MaxDepth(2)
     */
    private $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AdminMenu", mappedBy="parent")
     * @ORM\OrderBy({"position"="asc"})
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\AdminMenu>")
     * @JMS\MaxDepth(2)
     */
    private $children;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=true)
     */
    private $role;

    /**
     * AdminMenu constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setPosition( 0 );
        $this->setChildren( new ArrayCollection() );
    }

    /**
     * @param AdminMenu $menu
     *
     * @return $this
     */
    public function addChild( AdminMenu $menu )
    {
        if ( !$this->children->contains( $menu ) ) {
            $this->children->add( $menu );
            $menu->setParent( $this );
        }

        return $this;
    }

    /**
     * @param AdminMenu $menu
     *
     * @return $this
     */
    public function removeChild( AdminMenu $menu )
    {
        if ( $this->children->contains( $menu ) ) {
            $this->children->removeElement( $menu );
            $menu->setParent( NULL );
        }

        return $this;
    }

    /**
     * Get parent
     *
     * @return AdminMenu
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set Parent
     *
     * @param AdminMenu $parent
     *
     * @return AdminMenu
     */
    public function setParent( $parent = NULL )
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set Children
     *
     * @param ArrayCollection $children
     *
     * @return AdminMenu
     */
    public function setChildren( $children = NULL )
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get file
     *
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set File
     *
     * @param mixed $file
     *
     * @return AdminMenu
     */
    public function setFile( $file = NULL )
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return AdminMenu
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return intval( $this->position );
    }

    /**
     * Set Position
     *
     * @param int $position
     *
     * @return AdminMenu
     */
    public function setPosition( $position = 0 )
    {
        $this->position = intval( $position );

        return $this;
    }

    /**
     * Get namespace
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set Namespace
     *
     * @param string $namespace
     *
     * @return AdminMenu
     */
    public function setNamespace( $namespace = NULL )
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Get bundle
     *
     * @return string
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Set Bundle
     *
     * @param string $bundle
     *
     * @return AdminMenu
     */
    public function setBundle( $bundle = NULL )
    {
        $this->bundle = $bundle;

        return $this;
    }

    /**
     * Get role
     *
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set Role
     *
     * @param Role $role
     *
     * @return AdminMenu
     */
    public function setRole( $role = NULL )
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTemplatePath();
    }

    /**
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->getNamespace()
               . $this->getBundle()
               . ':'
               . 'AdminMenu'
               . ':'
               . $this->getFile();
    }
}

