<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Configuration
 *
 * @ORM\Table(name="core_configuration")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\ConfigurationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Configuration extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="site_name", type="string", length=128)
     */
    private $siteName;

    /**
     * @var string
     *
     * @ORM\Column(name="site_slogan", type="string", length=255, nullable=true)
     */
    private $siteSlogan;

    /**
     * @var string
     *
     * @ORM\Column(name="site_title", type="string", length=64)
     */
    private $siteTitle;

    /**
     * @var array
     *
     * @ORM\Column(name="meta_keywords", type="array", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_author", type="string", length=64, nullable=true)
     */
    private $metaAuthor;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", length=128, nullable=true)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="support_email", type="string", length=128, nullable=true)
     */
    private $supportEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=128, nullable=true)
     */
    private $company_name;

    /**
     * @var bool
     *
     * @ORM\Column(name="social_login", type="boolean", nullable=true)
     */
    private $socialLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="facebook_login", type="boolean", nullable=true)
     */
    private $facebookLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="twitter_login", type="boolean", nullable=true)
     */
    private $twitterLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="google_login", type="boolean", nullable=true)
     */
    private $googleLogin;

    /**
     * @var bool
     *
     * @ORM\Column(name="instagram_login", type="boolean", nullable=true)
     */
    private $instagramLogin;

    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="CoreSys\CoreBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="logo_id", nullable=true, onDelete="SET NULL")
     */
    private $logo;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Image")
     * @ORM\JoinTable(name="core_configuration_images",
     *     joinColumns={@ORM\JoinColumn(name="config_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")})
     * @ORM\OrderBy({"position"="asc"})
     */
    private $images;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinTable(name="core_configuration_sliders",
     *     joinColumns={@ORM\JoinColumn(name="config_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="slider_id", referencedColumnName="id")})
     * @ORM\OrderBy({"position"="asc"})
     */
    private $sliders;

    /**
     * @var Social
     *
     * @ORM\ManyToOne(targetEntity="Social", cascade={"persist"})
     * @ORM\JoinColumn(name="social_id", nullable=true, onDelete="SET NULL")
     */
    private $social;

    /**
     * Configuration constructor.
     */
    public function __construct()
    {
        $this->setSiteName( 'Site Name' );
        $this->setSiteSlogan( 'Site Slogan' );
        $this->setSiteTitle( 'Site Title' );
        $this->setSocialLogin( TRUE );
        $this->setFacebookLogin( TRUE );
        $this->setTwitterLogin( TRUE );
        $this->setGoogleLogin( TRUE );
        $this->setInstagramLogin( TRUE );
        $this->setImages( new ArrayCollection() );
        $this->setSliders( new ArrayCollection() );
    }

    /**
     * Get sliders
     *
     * @return ArrayCollection
     */
    public function getSliders()
    {
        return $this->sliders;
    }

    /**
     * Set Sliders
     *
     * @param ArrayCollection $sliders
     *
     * @return Configuration
     */
    public function setSliders( $sliders = NULL )
    {
        $this->sliders = $sliders;

        return $this;
    }

    /**
     * Get social
     *
     * @return Social
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * Set Social
     *
     * @param Social $social
     *
     * @return Configuration
     */
    public function setSocial( $social = NULL )
    {
        $this->social = $social;

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return $this
     */
    public function addImage( Image $image )
    {
        if ( !$this->images->contains( $image ) ) {
            $this->images->add( $image );
            $image->setInProgress( FALSE )
                  ->setType( 'System Image' )
                  ->setPosition( $this->getImagesCount() )
                  ->setAttachedTo( 'CoreSysCoreBundle:Configuration' )
                  ->setAttachedToId( 1 );
        }

        return $this;
    }

    /**
     * @return int
     * @JMS\VirtualProperty
     */
    public function getImagesCount()
    {
        $images = $this->getImages();

        return count( $images );
    }

    /**
     * Get images
     *
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set Images
     *
     * @param ArrayCollection $images
     *
     * @return Configuration
     */
    public function setImages( $images = NULL )
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return $this
     */
    public function removeImage( Image $image )
    {
        if ( $this->images->contains( $image ) ) {
            $this->images->removeElement( $image );
            $image->setType( 'DETATCHED' )
                  ->setInProgress( TRUE )
                  ->setAttachedTo( NULL )
                  ->setAttachedToId( NULL );
        }

        return $this;
    }

    /**
     * Get logo
     *
     * @return Image
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set Logo
     *
     * @param Image $logo
     *
     * @return Configuration
     */
    public function setLogo( $logo = NULL )
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get socialLogin
     *
     * @return boolean
     */
    public function getSocialLogin()
    {
        return $this->socialLogin === TRUE;
    }

    /**
     * Set SocialLogin
     *
     * @param boolean $socialLogin
     *
     * @return Configuration
     */
    public function setSocialLogin( $socialLogin = TRUE )
    {
        $this->socialLogin = $socialLogin === TRUE;

        return $this;
    }

    /**
     * Get facebookLogin
     *
     * @return boolean
     */
    public function getFacebookLogin()
    {
        return $this->facebookLogin === TRUE;
    }

    /**
     * Set FacebookLogin
     *
     * @param boolean $facebookLogin
     *
     * @return Configuration
     */
    public function setFacebookLogin( $facebookLogin = TRUE )
    {
        $this->facebookLogin = $facebookLogin === TRUE;

        return $this;
    }

    /**
     * Get twitterLogin
     *
     * @return boolean
     */
    public function getTwitterLogin()
    {
        return $this->twitterLogin === TRUE;
    }

    /**
     * Set TwitterLogin
     *
     * @param boolean $twitterLogin
     *
     * @return Configuration
     */
    public function setTwitterLogin( $twitterLogin = TRUE )
    {
        $this->twitterLogin = $twitterLogin === TRUE;

        return $this;
    }

    /**
     * Get googleLogin
     *
     * @return boolean
     */
    public function getGoogleLogin()
    {
        return $this->googleLogin === TRUE;
    }

    /**
     * Set GoogleLogin
     *
     * @param boolean $googleLogin
     *
     * @return Configuration
     */
    public function setGoogleLogin( $googleLogin = TRUE )
    {
        $this->googleLogin = $googleLogin === TRUE;

        return $this;
    }

    /**
     * Get instagramLogin
     *
     * @return boolean
     */
    public function getInstagramLogin()
    {
        return $this->instagramLogin === TRUE;
    }

    /**
     * Set InstagramLogin
     *
     * @param boolean $instagramLogin
     *
     * @return Configuration
     */
    public function setInstagramLogin( $instagramLogin = TRUE )
    {
        $this->instagramLogin = $instagramLogin === TRUE;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set CompanyName
     *
     * @param string $company_name
     *
     * @return Configuration
     */
    public function setCompanyName( $company_name = NULL )
    {
        $this->company_name = $company_name;

        return $this;
    }

    /**
     * Get siteName
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * Set SiteName
     *
     * @param string $siteName
     *
     * @return Configuration
     */
    public function setSiteName( $siteName = NULL )
    {
        $this->siteName = $siteName;

        return $this;
    }

    /**
     * Get siteSlogan
     *
     * @return string
     */
    public function getSiteSlogan()
    {
        return $this->siteSlogan;
    }

    /**
     * Set SiteSlogan
     *
     * @param string $siteSlogan
     *
     * @return Configuration
     */
    public function setSiteSlogan( $siteSlogan = NULL )
    {
        $this->siteSlogan = $siteSlogan;

        return $this;
    }

    /**
     * Get siteTitle
     *
     * @return string
     */
    public function getSiteTitle()
    {
        return $this->siteTitle;
    }

    /**
     * Set SiteTitle
     *
     * @param string $siteTitle
     *
     * @return Configuration
     */
    public function setSiteTitle( $siteTitle = NULL )
    {
        $this->siteTitle = $siteTitle;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return array
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set MetaKeywords
     *
     * @param array $metaKeywords
     *
     * @return Configuration
     */
    public function setMetaKeywords( $metaKeywords = NULL )
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set MetaDescription
     *
     * @param string $metaDescription
     *
     * @return Configuration
     */
    public function setMetaDescription( $metaDescription = NULL )
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaAuthor
     *
     * @return string
     */
    public function getMetaAuthor()
    {
        return $this->metaAuthor;
    }

    /**
     * Set MetaAuthor
     *
     * @param string $metaAuthor
     *
     * @return Configuration
     */
    public function setMetaAuthor( $metaAuthor = NULL )
    {
        $this->metaAuthor = $metaAuthor;

        return $this;
    }

    /**
     * Get adminEmail
     *
     * @return string
     */
    public function getAdminEmail()
    {
        return $this->adminEmail;
    }

    /**
     * Set AdminEmail
     *
     * @param string $adminEmail
     *
     * @return Configuration
     */
    public function setAdminEmail( $adminEmail = NULL )
    {
        $this->adminEmail = $adminEmail;

        return $this;
    }

    /**
     * Get supportEmail
     *
     * @return string
     */
    public function getSupportEmail()
    {
        return $this->supportEmail;
    }

    /**
     * Set SupportEmail
     *
     * @param string $supportEmail
     *
     * @return Configuration
     */
    public function setSupportEmail( $supportEmail = NULL )
    {
        $this->supportEmail = $supportEmail;

        return $this;
    }

    /**
     * @ORM\Prepersist
     */
    public function prepersist()
    {
        $siteName = $this->getSiteName();
        $siteName = trim( preg_replace( '/[\r\n]+/', '', $siteName ) );
        $this->setSiteName( $siteName );

        $siteTitle = $this->getSiteTitle();
        $siteTitle = trim( preg_replace( '/[\r\n]+/', '', $siteTitle ) );
        $this->setSiteTitle( $siteTitle );

        $baseController = $this->getBaseController();

        $logo = $this->getLogo();
        if ( !empty( $logo ) ) {
            $logo->setInProgress( FALSE );
            $logo->setAttachedTo( 'CoreSysCoreBundle:Configuration' );
            $logo->setAttachedToId( 1 );
            $logo->setType( 'System Logo' );

            if ( !empty( $baseController ) ) {
                $baseController->persist( $logo );
            }
        }

        $images = $this->getImages();
        foreach ( $images as $image ) {
            $image->setInProgress( FALSE )
                  ->setAttachedTo( 'CoreSysCoreBundle:Configuration' )
                  ->setAttachedToId( 1 )
                  ->setAttachedToRemoveMethod( 'removeImage' )
                  ->setType( 'System Image' );

            if ( !empty( $baseController ) ) {
                $baseController->persist( $image );
            }
        }

        $sliders = $this->getSliders();
        foreach ( $sliders as $slider ) {
            $slider->setInProgress( FALSE )
                   ->setAttachedTo( 'CoreSysCoreBundle:Configuration' )
                   ->setAttachedToId( 1 )
                   ->setAttachedToRemoveMethod( 'removeSlider' )
                   ->setType( 'System Slider' );

            if ( !empty( $baseController ) ) {
                $baseController->persist( $slider );
            }
        }
    }

    /**
     * @param Image $image
     *
     * @return $this
     */
    public function addSlider( Image $image )
    {
        if ( !$this->sliders->contains( $image ) ) {
            $this->sliders->add( $image );
        }

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return $this
     */
    public function removeSlider( Image $image )
    {
        if ( $this->sliders->contains( $image ) ) {
            $this->sliders->removeElement( $image );
            $image->setType( 'DETATCHED' )
                ->setInProgress( TRUE )
                  ->setAttachedTo( NULL )
                  ->setAttachedToId( NULL );
        }

        return $this;
    }
}

