<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * ImageSize
 *
 * @ORM\Table(name="core_image_size")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\ImageSizeRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("ImageSize",
 *     title="Image Sizes Management",
 *     responsive=true,
 *     checkable=false,
 *     ajax=@DT\Ajax(true, url="api_post_imagesize_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("slug", title="Slug", className="all"),
 *          @DT\Column("fallback", title="Fallback", className="", render=@DT\Renderer("renderImageFallback", template="CoreSysCoreBundle:Renderers:renderImageFallback.js.twig")),
 *          @DT\Column("maxWidth", title="Width", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("maxHeight", title="Height", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("imagesCount", title="Images", className=""),
 *          @DT\Column("crop", searchable=false, title="Crop", className="text-xs-center text-center", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("force", searchable=false, title="Force", className="text-xs-center text-center", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("active", searchable=false, title="Active", className="text-xs-center text-center", render=@DT\Renderer\CheckboxRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_new_imagesize", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Image Size")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_edit_imagesize", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_delete_imagesize", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class ImageSize extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     * @JMS\Expose
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="max_width", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $maxWidth;

    /**
     * @var int
     *
     * @ORM\Column(name="max_height", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $maxHeight;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * @var bool
     * @ORM\Column(name="force_create", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $force;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CoreSys\CoreBundle\Entity\Image", mappedBy="sizes", cascade={"persist"})
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\Image>")
     * @JMS\MaxDepth(2)
     */
    private $images;

    /**
     * @var bool
     *
     * @ORM\Column(name="crop", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $crop;

    /**
     * @var string
     *
     * @ORM\Column(name="crop_location", type="string", length=32, nullable=true)
     * @JMS\Expose
     */
    private $cropLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     * @JMS\Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=64, unique=true, nullable=true)
     * @Gedmo\Slug(fields={"name"}, separator="_")
     * @JMS\Expose
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="image_count", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $imageCount;

    /**
     * @var ImageSize
     *
     * @ORM\ManyToOne(targetEntity="CoreSys\CoreBundle\Entity\ImageSize", cascade={"persist"})
     * @ORM\JoinColumn(name="fallback_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\ImageSize")
     * @JMS\MaxDepth(2)
     */
    private $fallback;

    /**
     * @var string
     * @ORM\Column(type="string", length=12, nullable=true)
     * @JMS\Expose
     */
    private $forceFormat;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $animate;

    /**
     * ImageSize constructor.
     */
    public function __construct()
    {
        $this->setImages( new ArrayCollection() );
        $this->setActive( TRUE );
        $this->setCrop( TRUE );
        $this->setCropLocation( 'center-center' );
        $this->setImageCount( 0 );
        $this->setFallback( NULL );
        $this->setAnimate( TRUE );
        $this->setForce( FALSE );
    }

    /**
     * Get force
     *
     * @return boolean
     */
    public function getForce()
    {
        return $this->force === TRUE;
    }

    /**
     * Set Force
     *
     * @param boolean $force
     *
     * @return ImageSize
     */
    public function setForce( $force = TRUE )
    {
        $this->force = $force === TRUE;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasFallback()
    {
        return !empty( $this->fallback );
    }

    /**
     * @return int
     */
    public function getArea()
    {
        $width  = $this->getMaxWidth();
        $height = $this->getMaxHeight();

        if ( empty( $height ) ) {
            $height = $width * 0.75;
        } else if ( empty( $width ) ) {
            $width = $height * 1.333333;
        }

        return $width * $height;
    }

    /**
     * Get forceFormat
     *
     * @return string
     */
    public function getForceFormat()
    {
        return $this->forceFormat;
    }

    /**
     * Set ForceFormat
     *
     * @param string $forceFormat
     *
     * @return ImageSize
     */
    public function setForceFormat( $forceFormat = NULL )
    {
        $this->forceFormat = $forceFormat;

        return $this;
    }

    /**
     * Get animate
     *
     * @return boolean
     */
    public function getAnimate()
    {
        return $this->animate === TRUE;
    }

    /**
     * Set Animate
     *
     * @param boolean $animate
     *
     * @return ImageSize
     */
    public function setAnimate( $animate = TRUE )
    {
        $this->animate = $animate === TRUE;

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return $this
     */
    public function addImage( Image $image )
    {
        if ( !$this->images->contains( $image ) ) {
            $this->images->add( $image );
        }

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return $this
     */
    public function removeImage( Image $image )
    {
        if ( $this->images->contains( $image ) ) {
            $this->images->removeElement( $image );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName() . ' (' . $this->getMaxWidth() . 'x' . $this->getMaxHeight() . ')';
    }

    /**
     * @ORM\PrePersist()
     */
    public function prepersist()
    {
        $images = $this->getImages();
        $count  = count( $images );
        $this->setImageCount( $count );

//        $slug = $this->getSlug();
//        if ( empty( $slug ) ) {
//            $slug = $this->getName();
//        }
//
//        $slug = str_replace( '-', '_', $slug );
//        $slug = str_replace( ' ', '_', $slug );
//        for ( $i = 0; $i <= 3; $i++ ) {
//            $slug = str_replace( '__', '_', $slug );
//        }
//        $this->setSlug( $slug );
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return ImageSize
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get maxWidth
     *
     * @return int
     */
    public function getMaxWidth()
    {
        return intval( $this->maxWidth );
    }

    /**
     * Set MaxWidth
     *
     * @param int $maxWidth
     *
     * @return ImageSize
     */
    public function setMaxWidth( $maxWidth = 0 )
    {
        $this->maxWidth = intval( $maxWidth );

        return $this;
    }

    /**
     * Get maxHeight
     *
     * @return int
     */
    public function getMaxHeight()
    {
        return intval( $this->maxHeight );
    }

    /**
     * Set MaxHeight
     *
     * @param int $maxHeight
     *
     * @return ImageSize
     */
    public function setMaxHeight( $maxHeight = 0 )
    {
        $this->maxHeight = intval( $maxHeight );

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return ImageSize
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get images
     *
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set Images
     *
     * @param ArrayCollection $images
     *
     * @return ImageSize
     */
    public function setImages( $images = NULL )
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get crop
     *
     * @return boolean
     */
    public function getCrop()
    {
        return $this->crop === TRUE;
    }

    /**
     * Set Crop
     *
     * @param boolean $crop
     *
     * @return ImageSize
     */
    public function setCrop( $crop = TRUE )
    {
        $this->crop = $crop === TRUE;

        return $this;
    }

    /**
     * Get cropLocation
     *
     * @return string
     */
    public function getCropLocation()
    {
        return $this->cropLocation;
    }

    /**
     * Set CropLocation
     *
     * @param string $cropLocation
     *
     * @return ImageSize
     */
    public function setCropLocation( $cropLocation = NULL )
    {
        $this->cropLocation = $cropLocation;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return ImageSize
     */
    public function setType( $type = NULL )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set Slug
     *
     * @param string $slug
     *
     * @return ImageSize
     */
    public function setSlug( $slug = NULL )
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get imageCount
     *
     * @return int
     */
    public function getImageCount()
    {
        return intval( $this->imageCount );
    }

    /**
     * Set ImageCount
     *
     * @param int $imageCount
     *
     * @return ImageSize
     */
    public function setImageCount( $imageCount = 0 )
    {
        $this->imageCount = intval( $imageCount );

        return $this;
    }

    /**
     * Get fallback
     *
     * @return ImageSize
     */
    public function getFallback()
    {
        return $this->fallback;
    }

    /**
     * Set Fallback
     *
     * @param ImageSize $fallback
     *
     * @return ImageSize
     */
    public function setFallback( $fallback = NULL )
    {
        $this->fallback = $fallback;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @return int
     */
    public function getImagesCount()
    {
        $images = $this->getImages();

        return count( $images );
    }
}

