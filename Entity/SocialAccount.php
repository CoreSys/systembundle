<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocialAccount
 *
 * @ORM\Table(name="core_social_account")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\SocialAccountRepository")
 */
class SocialAccount extends BaseEntity
{

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="socialAccount", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_email", type="string", length=128, nullable=true)
     */
    private $facebookEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_email", type="string", length=128, nullable=true)
     */
    private $twitterEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_screen_name", type="string", length=64, nullable=true)
     */
    private $twitterScreenName;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram_email", type="string", length=128, nullable=true)
     */
    private $instagramEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="google_email", type="string", length=128, nullable=true)
     */
    private $googleEmail;

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return SocialAccount
     */
    public function setUser( $user = NULL )
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get facebookEmail
     *
     * @return string
     */
    public function getFacebookEmail()
    {
        return $this->facebookEmail;
    }

    /**
     * Set FacebookEmail
     *
     * @param string $facebookEmail
     *
     * @return SocialAccount
     */
    public function setFacebookEmail( $facebookEmail = NULL )
    {
        $this->facebookEmail = $facebookEmail;

        return $this;
    }

    /**
     * Get twitterEmail
     *
     * @return string
     */
    public function getTwitterEmail()
    {
        return $this->twitterEmail;
    }

    /**
     * Set TwitterEmail
     *
     * @param string $twitterEmail
     *
     * @return SocialAccount
     */
    public function setTwitterEmail( $twitterEmail = NULL )
    {
        $this->twitterEmail = $twitterEmail;

        return $this;
    }

    /**
     * Get twitterScreenName
     *
     * @return string
     */
    public function getTwitterScreenName()
    {
        return $this->twitterScreenName;
    }

    /**
     * Set TwitterScreenName
     *
     * @param string $twitterScreenName
     *
     * @return SocialAccount
     */
    public function setTwitterScreenName( $twitterScreenName = NULL )
    {
        $this->twitterScreenName = $twitterScreenName;

        return $this;
    }

    /**
     * Get instagramEmail
     *
     * @return string
     */
    public function getInstagramEmail()
    {
        return $this->instagramEmail;
    }

    /**
     * Set InstagramEmail
     *
     * @param string $instagramEmail
     *
     * @return SocialAccount
     */
    public function setInstagramEmail( $instagramEmail = NULL )
    {
        $this->instagramEmail = $instagramEmail;

        return $this;
    }

    /**
     * Get googleEmail
     *
     * @return string
     */
    public function getGoogleEmail()
    {
        return $this->googleEmail;
    }

    /**
     * Set GoogleEmail
     *
     * @param string $googleEmail
     *
     * @return SocialAccount
     */
    public function setGoogleEmail( $googleEmail = NULL )
    {
        $this->googleEmail = $googleEmail;

        return $this;
    }
}

