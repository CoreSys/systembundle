<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Social
 *
 * @ORM\Table(name="core_social")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\SocialRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Social extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=64, nullable=true)
     */
    private $facebook;

    /**
     * @var bool
     *
     * @ORM\Column(name="facebookShow", type="boolean", nullable=true)
     */
    private $facebookShow;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=64, nullable=true)
     */
    private $twitter;

    /**
     * @var bool
     *
     * @ORM\Column(name="twitterShow", type="boolean", nullable=true)
     */
    private $twitterShow;

    /**
     * @var string
     *
     * @ORM\Column(name="ebay", type="string", length=64, nullable=true)
     */
    private $ebay;

    /**
     * @var bool
     *
     * @ORM\Column(name="ebayShow", type="boolean", nullable=true)
     */
    private $ebayShow;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", length=64, nullable=true)
     */
    private $instagram;

    /**
     * @var bool
     *
     * @ORM\Column(name="instagramShow", type="boolean", nullable=true)
     */
    private $instagramShow;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=64, nullable=true)
     */
    private $youtube;

    /**
     * @var bool
     *
     * @ORM\Column(name="youtubeShow", type="boolean", nullable=true)
     */
    private $youtubeShow;

    /**
     * @var string
     *
     * @ORM\Column(name="pinterest", type="string", length=64, nullable=true)
     */
    private $pinterest;

    /**
     * @var bool
     *
     * @ORM\Column(name="pinterestShow", type="boolean", nullable=true)
     */
    private $pinterestShow;

    /**
     * Social constructor.
     */
    public function __construct()
    {

    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set Facebook
     *
     * @param string $facebook
     *
     * @return Social
     */
    public function setFacebook( $facebook = NULL )
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebookShow
     *
     * @return boolean
     */
    public function getFacebookShow()
    {
        return $this->facebookShow === TRUE;
    }

    /**
     * Set FacebookShow
     *
     * @param boolean $facebookShow
     *
     * @return Social
     */
    public function setFacebookShow( $facebookShow = TRUE )
    {
        $this->facebookShow = $facebookShow === TRUE;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set Twitter
     *
     * @param string $twitter
     *
     * @return Social
     */
    public function setTwitter( $twitter = NULL )
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitterShow
     *
     * @return boolean
     */
    public function getTwitterShow()
    {
        return $this->twitterShow === TRUE;
    }

    /**
     * Set TwitterShow
     *
     * @param boolean $twitterShow
     *
     * @return Social
     */
    public function setTwitterShow( $twitterShow = TRUE )
    {
        $this->twitterShow = $twitterShow === TRUE;

        return $this;
    }

    /**
     * Get ebay
     *
     * @return string
     */
    public function getEbay()
    {
        return $this->ebay;
    }

    /**
     * Set Ebay
     *
     * @param string $ebay
     *
     * @return Social
     */
    public function setEbay( $ebay = NULL )
    {
        $this->ebay = $ebay;

        return $this;
    }

    /**
     * Get ebayShow
     *
     * @return boolean
     */
    public function getEbayShow()
    {
        return $this->ebayShow === TRUE;
    }

    /**
     * Set EbayShow
     *
     * @param boolean $ebayShow
     *
     * @return Social
     */
    public function setEbayShow( $ebayShow = TRUE )
    {
        $this->ebayShow = $ebayShow === TRUE;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Set Instagram
     *
     * @param string $instagram
     *
     * @return Social
     */
    public function setInstagram( $instagram = NULL )
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagramShow
     *
     * @return boolean
     */
    public function getInstagramShow()
    {
        return $this->instagramShow === TRUE;
    }

    /**
     * Set InstagramShow
     *
     * @param boolean $instagramShow
     *
     * @return Social
     */
    public function setInstagramShow( $instagramShow = TRUE )
    {
        $this->instagramShow = $instagramShow === TRUE;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set Youtube
     *
     * @param string $youtube
     *
     * @return Social
     */
    public function setYoutube( $youtube = NULL )
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtubeShow
     *
     * @return boolean
     */
    public function getYoutubeShow()
    {
        return $this->youtubeShow === TRUE;
    }

    /**
     * Set YoutubeShow
     *
     * @param boolean $youtubeShow
     *
     * @return Social
     */
    public function setYoutubeShow( $youtubeShow = TRUE )
    {
        $this->youtubeShow = $youtubeShow === TRUE;

        return $this;
    }

    /**
     * Get pinterest
     *
     * @return string
     */
    public function getPinterest()
    {
        return $this->pinterest;
    }

    /**
     * Set Pinterest
     *
     * @param string $pinterest
     *
     * @return Social
     */
    public function setPinterest( $pinterest = NULL )
    {
        $this->pinterest = $pinterest;

        return $this;
    }

    /**
     * Get pinterestShow
     *
     * @return boolean
     */
    public function getPinterestShow()
    {
        return $this->pinterestShow === TRUE;
    }

    /**
     * Set PinterestShow
     *
     * @param boolean $pinterestShow
     *
     * @return Social
     */
    public function setPinterestShow( $pinterestShow = TRUE )
    {
        $this->pinterestShow = $pinterestShow === TRUE;

        return $this;
    }
}

