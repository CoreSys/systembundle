<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ImageConfig
 *
 * @ORM\Table(name="core_image_config")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\ImageConfigRepository")
 * @ORM\HasLifecycleCallbacks()
 * @JMS\ExclusionPolicy("all")
 */
class ImageConfig extends BaseEntity
{

    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="watermark_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Expose()
     * @JMS\MaxDepth(2)
     * @JMS\Type("CoreSys/CoreBundle/Entity/Image")
     *
     */
    private $watermark;

    /**
     * @var string
     *
     * @ORM\Column(name="watermark_location", type="string", length=32)
     * @JMS\Expose
     */
    private $watermarkLocation;

    /**
     * @var bool
     *
     * @ORM\Column(name="watermark_apply", type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $watermarkApply;

    /**
     * @var bool
     *
     * @ORM\Column(name="watermark_apply_to_all", type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $watermarkApplyToAll;

    /**
     * @var int
     *
     * @ORM\Column(name="watermark_max_width", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $watermarkMaxWidth;

    /**
     * @var int
     *
     * @ORM\Column(name="watermark_max_height", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $watermarkMaxHeight;

    /**
     * @var int
     *
     * @ORM\Column(name="watermark_horizontal_offset", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $watermarkHorizontalOffset;

    /**
     * @var int
     *
     * @ORM\Column(name="watermark_vertical_offset", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $watermarkVerticalOffset;

    /**
     * ImageConfig constructor.
     */
    public function __construct()
    {
        $this->setWatermarkApply( FALSE );
        $this->setWatermarkApplyToAll( FALSE );
        $this->setWatermarkMaxWidth( 100 );
        $this->setWatermarkMaxHeight( 100 );
        $this->setWatermarkHorizontalOffset( 0 );
        $this->setWatermarkVerticalOffset( 0 );
    }

    /**
     * Get watermark
     *
     * @return Image|null
     */
    public function getWatermark()
    {
        return $this->watermark;
    }

    /**
     * Set Watermark
     *
     * @param Image $watermark
     *
     * @return ImageConfig
     */
    public function setWatermark( $watermark = NULL )
    {
        $this->watermark = $watermark;

        return $this;
    }

    /**
     * Get watermarkLocation
     *
     * @return string
     */
    public function getWatermarkLocation()
    {
        return $this->watermarkLocation;
    }

    /**
     * Set WatermarkLocation
     *
     * @param string $watermarkLocation
     *
     * @return ImageConfig
     */
    public function setWatermarkLocation( $watermarkLocation = NULL )
    {
        $this->watermarkLocation = $watermarkLocation;

        return $this;
    }

    /**
     * Get watermarkApply
     *
     * @return boolean
     */
    public function getWatermarkApply()
    {
        return $this->watermarkApply === TRUE;
    }

    /**
     * Set WatermarkApply
     *
     * @param boolean $watermarkApply
     *
     * @return ImageConfig
     */
    public function setWatermarkApply( $watermarkApply = TRUE )
    {
        $this->watermarkApply = $watermarkApply === TRUE;

        return $this;
    }

    /**
     * Get watermarkApplyToAll
     *
     * @return boolean
     */
    public function getWatermarkApplyToAll()
    {
        return $this->watermarkApplyToAll === TRUE;
    }

    /**
     * Set WatermarkApplyToAll
     *
     * @param boolean $watermarkApplyToAll
     *
     * @return ImageConfig
     */
    public function setWatermarkApplyToAll( $watermarkApplyToAll = TRUE )
    {
        $this->watermarkApplyToAll = $watermarkApplyToAll === TRUE;

        return $this;
    }

    /**
     * Get watermarkMaxWidth
     *
     * @return int
     */
    public function getWatermarkMaxWidth()
    {
        return intval( $this->watermarkMaxWidth );
    }

    /**
     * Set WatermarkMaxWidth
     *
     * @param int $watermarkMaxWidth
     *
     * @return ImageConfig
     */
    public function setWatermarkMaxWidth( $watermarkMaxWidth = 0 )
    {
        $this->watermarkMaxWidth = intval( $watermarkMaxWidth );

        return $this;
    }

    /**
     * Get watermarkMaxHeight
     *
     * @return int
     */
    public function getWatermarkMaxHeight()
    {
        return intval( $this->watermarkMaxHeight );
    }

    /**
     * Set WatermarkMaxHeight
     *
     * @param int $watermarkMaxHeight
     *
     * @return ImageConfig
     */
    public function setWatermarkMaxHeight( $watermarkMaxHeight = 0 )
    {
        $this->watermarkMaxHeight = intval( $watermarkMaxHeight );

        return $this;
    }

    /**
     * Get watermarkHorizontalOffset
     *
     * @return int
     */
    public function getWatermarkHorizontalOffset()
    {
        return intval( $this->watermarkHorizontalOffset );
    }

    /**
     * Set WatermarkHorizontalOffset
     *
     * @param int $watermarkHorizontalOffset
     *
     * @return ImageConfig
     */
    public function setWatermarkHorizontalOffset( $watermarkHorizontalOffset = 0 )
    {
        $this->watermarkHorizontalOffset = intval( $watermarkHorizontalOffset );

        return $this;
    }

    /**
     * Get watermarkVerticalOffset
     *
     * @return int
     */
    public function getWatermarkVerticalOffset()
    {
        return intval( $this->watermarkVerticalOffset );
    }

    /**
     * Set WatermarkVerticalOffset
     *
     * @param int $watermarkVerticalOffset
     *
     * @return ImageConfig
     */
    public function setWatermarkVerticalOffset( $watermarkVerticalOffset = 0 )
    {
        $this->watermarkVerticalOffset = intval( $watermarkVerticalOffset );

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prepersist()
    {
        $watermark = $this->getWatermark();

        if ( $this->getWatermarkApply() ) {
            if ( empty( $watermark ) ) {
                $this->setWatermarkApply( FALSE );
            }
        }

        $width  = $this->getWatermarkMaxWidth();
        $height = $this->getWatermarkMaxHeight();
        $this->setWatermarkMaxWidth( $this->rangeCheck( $width, 0, 500 ) );
        $this->setWatermarkHeightWidth( $this->rangeCheck( $height, 0, 500 ) );

        $hor = $this->getWatermarkHorizontalOffset();
        $ver = $this->getWatermarkVerticalOffset();
        $this->setWatermarkHorizontalOffset( $this->rangeCheck( $hor, 0, 100 ) );
        $this->setWatermarkVerticalOffset( $this->rangeCheck( $ver, 0, 100 ) );

        if ( !empty( $watermark ) ) {
            if ( empty( $watermark->getId() ) ) {
                $this->setWatermark( NULL );
            } else {
                if ( !empty( $this->getBaseController() ) ) {
                    if ( method_exists( $watermark, 'setBaseController' ) ) {
                        $watermark->setBaseController( $this->getBaseController() );
                    }
                }
                $watermark->setType( 'watermark' );
                $watermark->setAttachedTo( 'CoreSysCoreBundle:ImageConfig' );
                $watermark->setAttachedToId( 1 );
                $watermark->setAttachedToRemoveMethod( NULL );
                $watermark->setInProgress( FALSE );
            }
        }
    }
}

