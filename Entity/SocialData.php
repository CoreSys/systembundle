<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocialData
 *
 * @ORM\Table(name="core_social_data")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\SocialDataRepository")
 */
class SocialData extends BaseEntity
{

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="socialData", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var array
     *
     * @ORM\Column(name="facebookData", type="array", nullable=true)
     */
    private $facebookData;

    /**
     * @var array
     *
     * @ORM\Column(name="twitterData", type="array", nullable=true)
     */
    private $twitterData;

    /**
     * @var array
     *
     * @ORM\Column(name="googlePlusData", type="array", nullable=true)
     */
    private $googlePlusData;

    /**
     * @var array
     *
     * @ORM\Column(name="instagramData", type="array", nullable=true)
     */
    private $instagramData;

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return SocialData
     */
    public function setUser( $user = NULL )
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get facebookData
     *
     * @return array
     */
    public function getFacebookData()
    {
        return is_array( $this->facebookData ) ? $this->facebookData : array();
    }

    /**
     * Set FacebookData
     *
     * @param array $facebookData
     *
     * @return SocialData
     */
    public function setFacebookData( $facebookData = NULL )
    {
        $this->facebookData = $facebookData;

        return $this;
    }

    /**
     * Get twitterData
     *
     * @return array
     */
    public function getTwitterData()
    {
        return is_array( $this->twitterData ) ? $this->twitterData : array();
    }

    /**
     * Set TwitterData
     *
     * @param array $twitterData
     *
     * @return SocialData
     */
    public function setTwitterData( $twitterData = NULL )
    {
        $this->twitterData = $twitterData;

        return $this;
    }

    /**
     * Get googlePlusData
     *
     * @return array
     */
    public function getGooglePlusData()
    {
        return is_array( $this->googlePlusData ) ? $this->googlePlusData : array();
    }

    /**
     * Set GooglePlusData
     *
     * @param array $googlePlusData
     *
     * @return SocialData
     */
    public function setGooglePlusData( $googlePlusData = NULL )
    {
        $this->googlePlusData = $googlePlusData;

        return $this;
    }

    /**
     * Get instagramData
     *
     * @return array
     */
    public function getInstagramData()
    {
        return is_array( $this->instagramData ) ? $this->instagramData : array();
    }

    /**
     * Set InstagramData
     *
     * @param array $instagramData
     *
     * @return SocialData
     */
    public function setInstagramData( $instagramData = NULL )
    {
        $this->instagramData = $instagramData;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFacebookPicture()
    {
        $facebookData = $this->getFacebookData();
        $url          = NULL;
        if ( !empty( $facebookData ) ) {
            if ( isset( $facebookData[ 'picture' ] ) ) {
                if ( isset( $facebookData[ 'picture' ][ 'data' ] ) ) {
                    if ( isset( $facebookData[ 'picture' ][ 'data' ][ 'url' ] ) ) {
                        return $facebookData[ 'picture' ][ 'data' ][ 'url' ];
                    }
                }
            }
        }

        return $url;
    }
}

