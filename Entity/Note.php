<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="core_note")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\NoteRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Note extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(name="type", length=32)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $complete;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var Note
     *
     * @ORM\ManyToOne(targetEntity="Note", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Note", mappedBy="parent", cascade={"all"})
     */
    private $children;

    /**
     * Note constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setParent( NULL );
        $this->setChildren( new ArrayCollection() );
        $this->setType( 'comment' );
        $this->setComplete( FALSE );
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return Note
     */
    public function setType( $type = NULL )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get complete
     *
     * @return boolean
     */
    public function getComplete()
    {
        return $this->complete === TRUE;
    }

    /**
     * Set Complete
     *
     * @param boolean $complete
     *
     * @return Note
     */
    public function setComplete( $complete = TRUE )
    {
        $this->complete = $complete === TRUE;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * @param Note $note
     *
     * @return $this
     */
    public function addChild( Note $note )
    {
        if ( !$this->children->contains( $note ) ) {
            $note->setParent( $this );
            $this->children->add( $note );
        }

        return $this;
    }

    /**
     * @param Note $note
     *
     * @return $this
     */
    public function removeChild( Note $note )
    {
        if ( $this->children->contains( $note ) ) {
            $this->children->removeElement( $note );
        }

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Note
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Note
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return Note
     */
    public function setTitle( $title = NULL )
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set Body
     *
     * @param string $body
     *
     * @return Note
     */
    public function setBody( $body = NULL )
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Note
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set Parent
     *
     * @param Note $parent
     *
     * @return Note
     */
    public function setParent( $parent = NULL )
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set Children
     *
     * @param ArrayCollection $children
     *
     * @return Note
     */
    public function setChildren( $children = NULL )
    {
        $this->children = $children;

        return $this;
    }

}

