<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * User
 *
 * @ORM\Table(name="core_user")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\UserRepository")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 * @DT\Table("User",
 *     title="User Management",
 *     responsive=true,
 *     checkable=true,
 *     ajax=@DT\Ajax(true, url="api_post_user_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("image", title="IMG", searchable=false, orderable=false, className="text-xs-center", render=@DT\Renderer("renderUserImage", template="CoreSysCoreBundle:Renderers:renderUserImage.js.twig") ),
 *          @DT\Column("username", title="Username", className="all"),
 *          @DT\Column("firstName", title="First Name", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("lastName", title="Last Name", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("email", title="Email", className=""),
 *          @DT\Column("created_at", title="Created", className="none", render=@DT\Renderer\DateRenderer()),
 *          @DT\Column("updated_at", title="Last Updated", className="none", render=@DT\Renderer\DateRenderer()),
 *          @DT\Column("lastLogin", title="Last Login", className="", render=@DT\Renderer\DateRenderer()),
 *          @DT\Column("sys_roles", title="Roles", className="", render=@DT\Renderer("renderUserRoles", template="CoreSysCoreBundle:Renderers:renderUserRoles.js.twig") ),
 *          @DT\Column("enabled", searchable=false, title="Enabled", className="text-xs-center all", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("locked", searchable=false, title="Locked", className="text-xs-center all", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("facebook_id", title="FacebookID", className="text-xs-center none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("twitter_id", title="TwitterID", className="text-xs-center none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("instagram_id", title="InstagramID", className="text-xs-center none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("google_id", title="GoogleID", className="text-xs-center none", render=@DT\Renderer\BlankRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_new_user", buttonType="success", iconClass="fa fa-plus-circle", text="Create New User")
 *     },
 *     rowActions={
 *          @DT\Action("view", type="link", internal=true, url="admin_profile_user", urlParams={"entity"="id"}, buttonType="info btn-sm", iconClass="fa fa-search", tooltip="View"),
 *          @DT\Action("edit", internal=true, url="api_edit_user", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_delete_user", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class User extends BaseUser
{

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    protected $created_at;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    protected $updated_at;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", length=128, nullable=true)
     */
    protected $apiKey;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     * @JMS\Expose
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     * @JMS\Expose
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="api_secret", length=128, nullable=true)
     */
    protected $apiSecret;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="core_user_roles")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\Role>")
     * @JMS\MaxDepth(2)
     */
    protected $sys_roles;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    protected $gravatar;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    protected $gravatar_use;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", length=64, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    protected $facebook_id;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    protected $facebook_access_token;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_id", type="string", length=64, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    protected $twitter_id;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_access_token", type="string", length=255, nullable=true)
     */
    protected $twitter_access_token;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    protected $google_id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $google_access_token;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    protected $instagram_id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $instagram_access_token;

    /**
     * @var SocialData
     *
     * @ORM\OneToOne(targetEntity="SocialData", mappedBy="user", cascade={"persist","remove"})
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\SocialData")
     */
    protected $socialData;

    /**
     * @var SocialAccount
     *
     * @ORM\OneToOne(targetEntity="SocialAccount", mappedBy="user", cascade={"persist","remove"})
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\SocialAccount")
     */
    protected $socialAccount;

    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", nullable=true, onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\Image")
     * @JMS\MaxDepth(2)
     */
    protected $image;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setGravatarUse( FALSE );
        $this->setGravatar( NULL );
        $this->setEnabled( TRUE );
        $this->setSysRoles( new ArrayCollection() );
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setLastLogin( new \DateTime() );

        $socialAccount = new SocialAccount();
        $socialAccount->setUser( $this );
        $this->setSocialAccount( $socialAccount );
    }

    /**
     * Get image
     *
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set Image
     *
     * @param Image $image
     *
     * @return User
     */
    public function setImage( $image = NULL )
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set FirstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName( $firstName = NULL )
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set LastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName( $lastName = NULL )
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set ApiKey
     *
     * @param string $apiKey
     *
     * @return User
     */
    public function setApiKey( $apiKey = NULL )
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiSecret
     *
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * Set ApiSecret
     *
     * @param string $apiSecret
     *
     * @return User
     */
    public function setApiSecret( $apiSecret = NULL )
    {
        $this->apiSecret = $apiSecret;

        return $this;
    }

    /**
     * Get socialAccount
     *
     * @return mixed
     */
    public function getSocialAccount()
    {
        if ( empty( $this->socialAccount ) ) {
            $this->socialAccount = new SocialAccount();
            $this->socialAccount->setUser( $this );
        }

        return $this->socialAccount;
    }

    /**
     * Set SocialAccount
     *
     * @param mixed $socialAccount
     *
     * @return User
     */
    public function setSocialAccount( $socialAccount = NULL )
    {
        $this->socialAccount = $socialAccount;

        return $this;
    }

    /**
     * Get twitter_id
     *
     * @return mixed
     */
    public function getTwitterId()
    {
        return $this->twitter_id;
    }

    /**
     * Set TwitterId
     *
     * @param mixed $twitter_id
     *
     * @return User
     */
    public function setTwitterId( $twitter_id = NULL )
    {
        $this->twitter_id = $twitter_id;

        return $this;
    }

    /**
     * Get twitter_access_token
     *
     * @return mixed
     */
    public function getTwitterAccessToken()
    {
        return $this->twitter_access_token;
    }

    /**
     * Set TwitterAccessToken
     *
     * @param mixed $twitter_access_token
     *
     * @return User
     */
    public function setTwitterAccessToken( $twitter_access_token = NULL )
    {
        $this->twitter_access_token = $twitter_access_token;

        return $this;
    }

    /**
     * Get google_id
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set GoogleId
     *
     * @param string $google_id
     *
     * @return User
     */
    public function setGoogleId( $google_id = NULL )
    {
        $this->google_id = $google_id;

        return $this;
    }

    /**
     * Get google_access_token
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

    /**
     * Set GoogleAccessToken
     *
     * @param string $google_access_token
     *
     * @return User
     */
    public function setGoogleAccessToken( $google_access_token = NULL )
    {
        $this->google_access_token = $google_access_token;

        return $this;
    }

    /**
     * Get instagram_id
     *
     * @return string
     */
    public function getInstagramId()
    {
        return $this->instagram_id;
    }

    /**
     * Set InstagramId
     *
     * @param string $instagram_id
     *
     * @return User
     */
    public function setInstagramId( $instagram_id = NULL )
    {
        $this->instagram_id = $instagram_id;

        return $this;
    }

    /**
     * Get instagram_access_token
     *
     * @return string
     */
    public function getInstagramAccessToken()
    {
        return $this->instagram_access_token;
    }

    /**
     * Set InstagramAccessToken
     *
     * @param string $instagram_access_token
     *
     * @return User
     */
    public function setInstagramAccessToken( $instagram_access_token = NULL )
    {
        $this->instagram_access_token = $instagram_access_token;

        return $this;
    }

    /**
     * Get socialData
     *
     * @return SocialData
     */
    public function getSocialData()
    {
        return $this->socialData;
    }

    /**
     * Set SocialData
     *
     * @param SocialData $socialData
     *
     * @return User
     */
    public function setSocialData( $socialData = NULL )
    {
        $this->socialData = $socialData;
        if ( !empty( $this->socialData ) ) {
            $this->socialData->setUser( $this );
        }

        return $this;
    }

    /**
     * Get facebook_access_token
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * Set FacebookAccessToken
     *
     * @param string $facebook_access_token
     *
     * @return User
     */
    public function setFacebookAccessToken( $facebook_access_token = NULL )
    {
        $this->facebook_access_token = $facebook_access_token;

        return $this;
    }

    /**
     * @param Role $role
     *
     * @return $this
     * @return $this
     */
    public function addSysRole( Role $role )
    {
        if ( !$this->sys_roles->contains( $role ) ) {
            $role->addUser( $this );
            $this->sys_roles->add( $role );
            $this->addRole( $role->getRoleName() );
        }

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set FacebookId
     *
     * @param string $facebook_id
     *
     * @return User
     */
    public function setFacebookId( $facebook_id = NULL )
    {
        $this->facebook_id = $facebook_id;

        return $this;
    }

    /**
     * @param null $role
     *
     * @return bool|mixed|null
     */
    public function hasSysRole( $role = NULL )
    {
        if ( $role instanceof Role ) {
            if ( $this->sys_roles->contains( $role ) ) {
                return $role;
            }

            return FALSE;
        }

        foreach ( $this->getSysRoles() as $sysRole ) {
            if ( $sysRole->getName() === $role || $sysRole->getRoleName() === $role ) {
                return $sysRole;
            }
        }

        return FALSE;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );

        $apiKey    = $this->getApiKey();
        $apiSecret = $this->getApiSecret();

        if ( empty( $apiSecret ) ) {
            $this->setApiSecret( intval( str_replace( '.', '', microtime( TRUE ) ) ) );
            $apiSecret = $this->getApiSecret();
        }

        if ( empty( $apiKey ) ) {
            $this->setApiKey( $this->randUniqueId( $apiSecret ) );
        }

        if ( $this->getGravatarUse() === TRUE ) {
            $email = $this->getGravatar();
            if ( empty( $email ) ) {
                $this->setGravatar( $this->getEmail() );
            }
        }

        if ( empty( $this->created_at ) ) {
            $this->setCreatedAt( new \DateTime() );
        }

        $this->prepersistSocialAccounts();
    }

    public function randUniqueId( $in, $toNum = FALSE, $padUp = 9, $passKey = NULL )
    {
        if ( empty( $passKey ) ) {
            $passKey = $this->getSalt();
        }
        $i     = array();
        $p     = array();
        $index = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ( $passKey !== NULL ) {
            for ( $n = 0; $n < strlen( $index ); $n++ ) {
                $i[] = substr( $index, $n, 1 );
            }

            $passhash = hash( 'sha256', $passKey );
            $passhash = ( strlen( $passhash ) < strlen( $index ) )
                ? hash( 'sha512', $passKey )
                : $passhash;

            for ( $n = 0; $n < strlen( $index ); $n++ ) {
                $p[] = substr( $passhash, $n, 1 );
            }

            array_multisort( $p, SORT_DESC, $i );
            $index = implode( $i );
        }

        $base = strlen( $index );

        if ( $toNum ) {
            $in  = strrev( $in );
            $out = 0;
            $len = strlen( $in ) - 1;
            for ( $t = 0; $t <= $len; $t++ ) {
                $bcpow = bcpow( $base, $len - $t );
                $out   = $out + strpos( $index, substr( $in, $t, 1 ) ) * $bcpow;
            }

            if ( is_numeric( $padUp ) ) {
                $padUp--;
                if ( $padUp > 0 ) {
                    $out -= pow( $base, $padUp );
                }
            }

            $out = sprintf( '%F', $out );
            $out = substr( $out, 0, strpos( $out, '.' ) );
        } else {
            if ( is_numeric( $padUp ) ) {
                $padUp--;
                if ( $padUp > 0 ) {
                    $in += pow( $base, $padUp );
                }
            }

            $out = '';
            for ( $t = floor( log( $in, $base ) ); $t >= 0; $t-- ) {
                $bcp = bcpow( $base, $t );
                $a   = floor( $in / $bcp ) % $base;
                $out = $out . substr( $index, $a, 1 );
                $in  = $in - ( $a * $bcp );
            }
            $out = strrev( $out );
        }

        return $out;
    }

    public function prepersistSocialAccounts()
    {
        $socialAccount = $this->getSocialAccount();
        if ( empty( $socialAccount ) ) {
            $socialAccount = new SocialAccount();
            $socialAccount->setUser( $this );
        }

        $socialData = $this->getSocialData();

        if ( !empty( $socialData ) ) {
            // facebook
            if ( $this->hasSocialData( 'facebook' ) ) {
                $data         = $socialData->getFacebookData();
                $accountEmail = $socialAccount->getFacebookEmail();
                if ( empty( $accountEmail ) && !empty( $data[ 'email' ] ) ) {
                    $socialAccount->setFacebookEmail( $data[ 'email' ] );
                }
            }

            // twitter
            if ( $this->hasSocialData( 'twitter' ) ) {
                $data            = $socialData->getTwitterData();
                $data[ 'email' ] = isset( $data[ 'email' ] ) ? $data[ 'email' ] : NULL;
                $accountEmail    = isset( $data[ 'email' ] ) ? $data[ 'email' ] : NULL;
                $accountName     = $data[ 'screen_name' ];

                if ( empty( $accountEmail ) && !empty( $data[ 'email' ] ) ) {
                    $socialAccount->setTwitterEmail( $data[ 'email' ] );
                }

                if ( empty( $accountName ) && !empty( $data[ 'screen_name' ] ) ) {
                    $socialAccount->setTwitterScreenName( $data[ 'screen_name' ] );
                }
            }

            // google plus
            if ( $this->hasSocialData( 'google' ) ) {
                $data = $socialData->getGooglePlusData();
                // @todo - when auth is working, implement this
            }

            // instagram
            if ( $this->hasSocialData( 'instagram' ) ) {
                $data = $socialData->getInstagramData();
                // @todo - when auth is working, implement this
            }
        }

        $this->setSocialAccount( $socialAccount );
    }

    /**
     * @return null|string
     */
    public function getGravatarUrl2()
    {
        if ( !$this->getGravatarUse() ) {
            return NULL;
        }

        $email = $this->getGravatar();
        if ( empty( $email ) ) {
            $email = $this->getEmail();
        }

        return 'http://www.gravatar.com/avatar/' . md5( strtlower( trim( $email ) ) );
    }

    /**
     * Remove all roles and sys roles from the user account
     */
    public function clearRoles()
    {
        foreach ( $this->getSysRoles() as $role ) {
            $this->removeSysRole( $role );
        }

        foreach ( $this->getRoles() as $role ) {
            $this->removeRole( $role );
        }
    }

    /**
     * @param Role $role
     *
     * @return $this
     */
    public function removeSysRole( Role $role )
    {
        if ( $this->sys_roles->contains( $role ) ) {
            $role->removeUser( $this );
            $this->sys_roles->removeElement( $role );
            $this->removeRole( $role->getRoleName() );
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->getEnabled();
    }

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive( $active = TRUE )
    {
        $this->setEnabled( !!$active );

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        if ( empty( $this->created_at ) ) {
            $this->created_at = new \DateTime();
        }

        return $this->created_at;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $created_at
     *
     * @return User
     */
    public function setCreatedAt( $created_at = NULL )
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        if ( empty( $this->updated_at ) ) {
            $this->updated_at = new \DateTime();
        }

        return $this->updated_at;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updated_at
     *
     * @return User
     */
    public function setUpdatedAt( $updated_at = NULL )
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get sys_roles
     *
     * @return ArrayCollection
     */
    public function getSysRoles()
    {
        return $this->sys_roles;
    }

    /**
     * Set SysRoles
     *
     * @param ArrayCollection $sys_roles
     *
     * @return User
     */
    public function setSysRoles( $sys_roles = NULL )
    {
        $this->sys_roles = $sys_roles;

        return $this;
    }

    /**
     * Get gravatar
     *
     * @return string
     */
    public function getGravatar()
    {
        return $this->gravatar;
    }

    /**
     * Set Gravatar
     *
     * @param string $gravatar
     *
     * @return User
     */
    public function setGravatar( $gravatar = NULL )
    {
        $this->gravatar = $gravatar;

        return $this;
    }

    /**
     * Get gravatar_use
     *
     * @return boolean
     */
    public function getGravatarUse()
    {
        return $this->gravatar_use === TRUE;
    }

    /**
     * Set GravatarUse
     *
     * @param boolean $gravatar_use
     *
     * @return User
     */
    public function setGravatarUse( $gravatar_use = TRUE )
    {
        $this->gravatar_use = $gravatar_use === TRUE;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("gravatarUrl")
     */
    public function getGravatarUrl()
    {
        if ( !$this->getGravatarUse() ) {
            return NULL;
        }

        $email = $this->getGravatar();
        if ( empty( $email ) ) {
            $email = $this->getEmail();
        }

        return 'https://www.gravatar.com/avatar/' . md5( strtolower( trim( $email ) ) );
    }

    public function hasSocialData( $type = NULL )
    {
        $socialData = $this->getSocialData();
        if ( empty( $type ) || empty( $socialData ) ) {
            return FALSE;
        }

        switch ( strtolower( trim( $type ) ) ) {
            case 'facebook':
                return !empty( $socialData->getFacebookData() );
            case 'twitter':
                return !empty( $socialData->getTwitterData() );
            case 'google':
            case 'google_plus':
            case 'googleplus':
                return !empty( $socialData->getGooglePlusData() );
            case 'instagram':
                return !empty( $socialData->getInstagramData() );
            default:
                return FALSE;
        }

        return FALSE;
    }
}

