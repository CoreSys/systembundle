<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Image
 *
 * @ORM\Table(name="core_image")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\ImageRepository")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 * @DT\Table("Image",
 *      title="Image Management",
 *      responsive=true,
 *      checkable=true,
 *      ajax=@DT\Ajax(true, url="api_post_image_datatables"),
 *      columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column(null, title="IMG", className="all", render=@DT\Renderer("renderImagePreview",template="CoreSysCoreBundle:Renderers:renderImagePreview.js.twig")),
 *          @DT\Column("type", title="Type", className="all", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("filename", title="filename", className="" ),
 *          @DT\Column("title", title="Title", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("width", title="Width", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("height", title="Height", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("description", title="Description", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("mimeType", title="Mime Type", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("alt", title="Alt", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("filesize", title="Size", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("attachedTo", title="Attached To", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("attachedToId", title="Attached To ID", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("views", title="Views", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("ratio", title="Ratio", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("ratioReverse", title="Ratio2", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("createdAt", title="Created", className="none", render=@DT\Renderer\DateRenderer()),
 *          @DT\Column("updatedAt", title="Updated", className="none", render=@DT\Renderer\DateRenderer()),
 *          @DT\Column("position", title="Position", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("applyWatermark", searchable=false, title="Watermark", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("inProgress", searchable=false, title="InProgress", className="all", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("active", searchable=false, title="Active", className="all", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("sizes", title="Sizes", className="none", render=@DT\Renderer("renderImageSizes", template="CoreSysCoreBundle:Renderers:renderImageSizes.js.twig"))
 *      },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_edit_image", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_delete_image", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Image extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'m/d/y g:i a'>")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'m/d/y g:i a'>")
     */
    private $updatedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     * @JMS\Expose
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64)
     * @JMS\Expose
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @JMS\Expose
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=12, nullable=true)
     * @JMS\Expose
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=64, nullable=true)
     * @JMS\Expose
     */
    private $mimeType;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=64, nullable=true)
     * @JMS\Expose
     */
    private $alt;

    /**
     * @var int
     *
     * @ORM\Column(name="views", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $views;

    /**
     * @var int
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $width;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="ratio", type="float", nullable=true)
     * @JMS\Expose
     */
    private $ratio;

    /**
     * @var float
     *
     * @ORM\Column(name="ratio_reverse", type="float", nullable=true)
     * @JMS\Expose
     */
    private $ratioReverse;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CoreSys\CoreBundle\Entity\ImageSize", inversedBy="images")
     * @ORM\JoinTable(
     *     name="core_image_sizes",
     *     joinColumns={
     *          @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="size_id", referencedColumnName="id")
     *     }
     * )
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\ImageSize>")
     */
    private $sizes;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="apply_watermark", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $applyWatermark;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=128)
     * @JMS\Expose
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     * @JMS\Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="attached_to", type="string", length=64, nullable=true)
     * @JMS\Expose
     */
    private $attachedTo;

    /**
     * @var int
     *
     * @ORM\Column(name="attached_to_id", type="bigint", nullable=true)
     * @JMS\Expose
     */
    private $attachedToId;

    /**
     * @var string
     *
     * @ORM\Column(name="attached_to_remove_method", type="string", length=64, nullable=true)
     * @JMS\Expose
     */
    private $attachedToRemoveMethod;

    /**
     * @var int
     *
     * @ORM\Column(name="filesize", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $filesize;

    /**
     * @var bool
     *
     * @ORM\Column(name="in_progress", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $inProgress;

    /**
     * @var string
     *
     * @ORM\Column(name="image_url", type="text", nullable=true)
     * @JMS\Expose
     */
    private $imageUrl;

    /**
     * @var UploadedFile
     * @Assert\File(maxSize="10000000000000")
     */
    private $file;

    /**
     * Image constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setApplyWatermark( TRUE );
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setHeight( 0 );
        $this->setWidth( 0 );
        $this->setRatio( 0 );
        $this->setRatioReverse( 0 );
        $this->setSizes( new ArrayCollection() );
        $this->setViews( 0 );
        $this->setInProgress( FALSE );
        $this->setPosition( 0 );
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set File
     *
     * @param UploadedFile $file
     *
     * @return Image
     */
    public function setFile( $file = NULL )
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Image
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Image
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return intval( $this->position );
    }

    /**
     * Set Position
     *
     * @param int $position
     *
     * @return Image
     */
    public function setPosition( $position = 0 )
    {
        $this->position = intval( $position );

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return Image
     */
    public function setTitle( $title = NULL )
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Description
     *
     * @param string $description
     *
     * @return Image
     */
    public function setDescription( $description = NULL )
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set Extension
     *
     * @param string $extension
     *
     * @return Image
     */
    public function setExtension( $extension = NULL )
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set MimeType
     *
     * @param string $mimeType
     *
     * @return Image
     */
    public function setMimeType( $mimeType = NULL )
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set Alt
     *
     * @param string $alt
     *
     * @return Image
     */
    public function setAlt( $alt = NULL )
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get views
     *
     * @return int
     */
    public function getViews()
    {
        return intval( $this->views );
    }

    /**
     * Set Views
     *
     * @param int $views
     *
     * @return Image
     */
    public function setViews( $views = 0 )
    {
        $this->views = intval( $views );

        return $this;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return intval( $this->width );
    }

    /**
     * Set Width
     *
     * @param int $width
     *
     * @return Image
     */
    public function setWidth( $width = 0 )
    {
        $this->width = intval( $width );

        return $this;
    }

    /**
     * Get height
     *
     * @return int
     */
    public function getHeight()
    {
        return intval( $this->height );
    }

    /**
     * Set Height
     *
     * @param int $height
     *
     * @return Image
     */
    public function setHeight( $height = 0 )
    {
        $this->height = intval( $height );

        return $this;
    }

    /**
     * Get ratio
     *
     * @return float
     */
    public function getRatio()
    {
        return floatval( $this->ratio );
    }

    /**
     * Set Ratio
     *
     * @param float $ratio
     *
     * @return Image
     */
    public function setRatio( $ratio = 0 )
    {
        $this->ratio = floatval( $ratio );

        return $this;
    }

    /**
     * Get ratioReverse
     *
     * @return float
     */
    public function getRatioReverse()
    {
        return floatval( $this->ratioReverse );
    }

    /**
     * Set RatioReverse
     *
     * @param float $ratioReverse
     *
     * @return Image
     */
    public function setRatioReverse( $ratioReverse = 0 )
    {
        $this->ratioReverse = floatval( $ratioReverse );

        return $this;
    }

    /**
     * Get sizes
     *
     * @return ArrayCollection
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * Set Sizes
     *
     * @param ArrayCollection $sizes
     *
     * @return Image
     */
    public function setSizes( $sizes = NULL )
    {
        $this->sizes = $sizes;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Image
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get applyWatermark
     *
     * @return boolean
     */
    public function getApplyWatermark()
    {
        return $this->applyWatermark === TRUE;
    }

    /**
     * Set ApplyWatermark
     *
     * @param boolean $applyWatermark
     *
     * @return Image
     */
    public function setApplyWatermark( $applyWatermark = TRUE )
    {
        $this->applyWatermark = $applyWatermark === TRUE;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set Filename
     *
     * @param string $filename
     *
     * @return Image
     */
    public function setFilename( $filename = NULL )
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return Image
     */
    public function setType( $type = NULL )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get attachedTo
     *
     * @return string
     */
    public function getAttachedTo()
    {
        return $this->attachedTo;
    }

    /**
     * Set AttachedTo
     *
     * @param string $attachedTo
     *
     * @return Image
     */
    public function setAttachedTo( $attachedTo = NULL )
    {
        $this->attachedTo = $attachedTo;

        return $this;
    }

    /**
     * Get attachedToId
     *
     * @return int
     */
    public function getAttachedToId()
    {
        return intval( $this->attachedToId );
    }

    /**
     * Set AttachedToId
     *
     * @param int $attachedToId
     *
     * @return Image
     */
    public function setAttachedToId( $attachedToId = 0 )
    {
        $this->attachedToId = intval( $attachedToId );

        return $this;
    }

    /**
     * Get attachedToRemoveMethod
     *
     * @return string
     */
    public function getAttachedToRemoveMethod()
    {
        return $this->attachedToRemoveMethod;
    }

    /**
     * Set AttachedToRemoveMethod
     *
     * @param string $attachedToRemoveMethod
     *
     * @return Image
     */
    public function setAttachedToRemoveMethod( $attachedToRemoveMethod = NULL )
    {
        $this->attachedToRemoveMethod = $attachedToRemoveMethod;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return int
     */
    public function getFilesize()
    {
        return intval( $this->filesize );
    }

    /**
     * Set Filesize
     *
     * @param int $filesize
     *
     * @return Image
     */
    public function setFilesize( $filesize = 0 )
    {
        $this->filesize = intval( $filesize );

        return $this;
    }

    /**
     * Get inProgress
     *
     * @return boolean
     */
    public function getInProgress()
    {
        return $this->inProgress === TRUE;
    }

    /**
     * Set InProgress
     *
     * @param boolean $inProgress
     *
     * @return Image
     */
    public function setInProgress( $inProgress = TRUE )
    {
        $this->inProgress = $inProgress === TRUE;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set ImageUrl
     *
     * @param string $imageUrl
     *
     * @return Image
     */
    public function setImageUrl( $imageUrl = NULL )
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @param ImageSize $size
     *
     * @return $this
     */
    public function addSize( ImageSize $size )
    {
        if ( !$this->sizes->contains( $size ) ) {
            $this->sizes->add( $size );
            $size->addImage( $this );
        }

        return $this;
    }

    /**
     * @param ImageSize $size
     *
     * @return $this
     */
    public function removeSize( ImageSize $size )
    {
        if ( $this->sizes->contains( $size ) ) {
            $this->sizes->removeElement( $size );
            $size->removeImage( $this );
        }

        return $this;
    }

    /**
     * @param null $data
     *
     * @return $this|Image
     */
    public function setData( $data = NULL )
    {
        if ( $data instanceof UploadedFile ) {
            return $this->setDataFromUploadedFile( $data );
        } else if ( is_array( $data ) ) {
            $this->setDataFromArray( $data );
        }

        return $this;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setDataFromArray( array $data )
    {
        foreach ( $data as $k => $v ) {
            $func = 'set' . str_replace( ' ', '', ucwords( str_replace( '_', ' ', strtolower( trim( $k ) ) ) ) );
            if ( method_exists( $this, $func ) ) {
                $this->$func( $v );
            }
        }

        return $this;
    }

    /**
     * @param UploadedFile $data
     *
     * @return $this
     */
    public function setDataFromUploadedFile( UploadedFile $data )
    {
        $name = $data->getClientOriginalName();
        $ext  = pathinfo( $name, PATHINFO_EXTENSION );
        $type = $data->getMimeType();

        $ext = strtolower( trim( $ext ) );

        if ( empty( $this->title ) ) {
            $this->setTitle( $name );
        }
        if ( empty( $this->filename ) ) {
            $this->setFilename( $name );
        }
        if ( empty( $this->extension ) ) {
            $this->setExtension( $ext );
        }
        if ( empty( $this->mimeType ) ) {
            $this->setMimeType( $type );
        }

        $this->setFilesize( $data->getSize() );

        return $this;
    }

    /**
     * @return $this
     */
    public function incViews()
    {
        $views = intval( $this->views );
        $views++;
        $this->setViews( $views );

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $controller = $this->getBaseController();
        if ( !empty( $controller ) ) {
            $url = $controller->generateUrl( 'media_image_get', array( 'size' => $this->getFilename() ) );
            $url = str_replace( '/app_dev.php', '', $url );
            $url = str_replace( DIRECTORY_SEPARATOR . 'app_dev.php', '', $url );
            $this->setImageUrl( $url );
        }

        $ext = $this->getExtension();
        if ( empty( $ext ) ) {
            $filename = $this->getFilename();
            $ext      = pathinfo( $filename, PATHINFO_EXTENSION );
        }

        $ext = $ext === 'jpeg' ? 'jpg' : $ext;
        $this->setExtension( $ext );
    }

    /**
     * @return bool
     */
    public function isAttached()
    {
        $attachedTo = $this->getAttachedTo();

        return !empty( $attachedTo );
    }
}

