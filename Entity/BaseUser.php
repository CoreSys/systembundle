<?php

namespace CoreSys\CoreBundle\Entity;

use FOS\UserBundle\Model\GroupableInterface;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\UserInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use CoreSys\CoreBundle\Controller\BaseController;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BaseUser
 * @package CoreSys\CoreBundle\Entity
 * @JMS\ExclusionPolicy("all")
 */
class BaseUser extends BaseEntity implements UserInterface, GroupableInterface
{

    /**
     * @var string
     * @ORM\Column(length=255)
     * @JMS\Expose
     * @JMS\Groups({"Default", "admin"})
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(length=255, unique=true)
     * @JMS\Groups({"admin"})
     */
    protected $usernameCanonical;

    /**
     * @var string
     * @ORM\Column(length=255)
     * @JMS\Expose
     * @JMS\Groups({"Default", "admin"})
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(length=255, unique=true)
     * @JMS\Groups({"admin"})
     */
    protected $emailCanonical;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"Default", "admin"})
     */
    protected $enabled;

    /**
     * @var string
     * @ORM\Column(length=255)
     * @JMS\Exclude
     */
    protected $salt;

    /**
     * @var password
     * @ORM\Column(length=255)
     * @JMS\Exclude
     */
    protected $password;

    /**
     * @var string
     * @JMS\Expose
     * @JMS\Groups({"Default", "admin"})
     */
    protected $plainPassword;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     * @JMS\Groups({"Default", "admin"})
     * @JMS\SerializedName("lastLogin")
     */
    protected $lastLogin;

    /**
     * @var string|null
     * @ORM\Column(length=255, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"Default", "admin"})
     */
    protected $confirmationToken;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'m/d/y g:i a'>")
     * @JMS\Groups({"admin"})
     */
    protected $passwordRequestedAt;

    /**
     * @var Collection
     * JMS\Expose
     * JMS\Type("ArrayCollection")
     * JMS\Groups({"admin"})
     */
    protected $groups;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     */
    protected $locked;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     */
    protected $expired;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'m/d/y g:i a'>")
     * @JMS\Groups({"admin"})
     */
    protected $expiresAt;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @JMS\Expose
     * @JMS\Type("array")
     * @JMS\Groups({"admin"})
     */
    protected $roles;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'m/d/y g:i a'>")
     * @JMS\Groups({"admin"})
     */
    protected $credentialsExpireAt;

    /**
     * BaseUser constructor.
     */
    public function __construct()
    {
        $this->salt               = base_convert( sha1( uniqid( mt_rand(), TRUE ) ), 16, 36 );
        $this->enabled            = FALSE;
        $this->locked             = FALSE;
        $this->expired            = FALSE;
        $this->roles              = array();
        $this->credentialsExpired = FALSE;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled === TRUE;
    }

    /**
     * Set Enabled
     *
     * @param boolean $enabled
     *
     * @return BaseUser
     */
    public function setEnabled( $enabled = TRUE )
    {
        $this->enabled = $enabled === TRUE;

        return $this;
    }

    /**
     * Get passwordRequestedAt
     *
     * @return mixed
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param \DateTime|NULL $date
     *
     * @return $this
     */
    public function setPasswordRequestedAt( \DateTime $date = NULL )
    {
        $this->passwordRequestedAt = $date;

        return $this;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired === TRUE;
    }

    /**
     * Set Expired
     *
     * @param boolean $expired
     *
     * @return BaseUser
     */
    public function setExpired( $expired = TRUE )
    {
        $this->expired = $expired === TRUE;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return mixed
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set ExpiresAt
     *
     * @param mixed $expiresAt
     *
     * @return BaseUser
     */
    public function setExpiresAt( $expiresAt = NULL )
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired === TRUE;
    }

    /**
     * Set CredentialsExpired
     *
     * @param boolean $credentialsExpired
     *
     * @return BaseUser
     */
    public function setCredentialsExpired( $credentialsExpired = TRUE )
    {
        $this->credentialsExpired = $credentialsExpired === TRUE;

        return $this;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return mixed
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }

    /**
     * Set CredentialsExpireAt
     *
     * @param mixed $credentialsExpireAt
     *
     * @return BaseUser
     */
    public function setCredentialsExpireAt( $credentialsExpireAt = NULL )
    {
        $this->credentialsExpireAt = $credentialsExpireAt;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function addRole( $role )
    {
        $role = strtoupper( $role );
        if ( $role === static::ROLE_DEFAULT ) {
            return $this;
        }
        if ( !in_array( $role, $this->roles, TRUE ) ) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = NULL;
    }

    /**
     * Get confirmationToken
     *
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Set ConfirmationToken
     *
     * @param mixed $confirmationToken
     *
     * @return BaseUser
     */
    public function setConfirmationToken( $confirmationToken = NULL )
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get email
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set Email
     *
     * @param mixed $email
     *
     * @return BaseUser
     */
    public function setEmail( $email = NULL )
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get emailCanonical
     *
     * @return mixed
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Set EmailCanonical
     *
     * @param mixed $emailCanonical
     *
     * @return BaseUser
     */
    public function setEmailCanonical( $emailCanonical = NULL )
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Get password
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set Password
     *
     * @param mixed $password
     *
     * @return BaseUser
     */
    public function setPassword( $password = NULL )
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get plainPassword
     *
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set PlainPassword
     *
     * @param mixed $plainPassword
     *
     * @return BaseUser
     */
    public function setPlainPassword( $plainPassword = NULL )
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;
        foreach ( $this->getGroups() as $group ) {
            $roles = array_merge( $roles, $group->getRoles() );
        }

        $roles[] = static::ROLE_DEFAULT;

        return array_unique( $roles );
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles( array $roles )
    {
        $this->roles = array();

        foreach ( $roles as $role ) {
            $this->addRole( $role );
        }

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set Salt
     *
     * @param string $salt
     *
     * @return BaseUser
     */
    public function setSalt( $salt = NULL )
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get username
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set Username
     *
     * @param mixed $username
     *
     * @return BaseUser
     */
    public function setUsername( $username = NULL )
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get usernameCanonical
     *
     * @return mixed
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * Set UsernameCanonical
     *
     * @param mixed $usernameCanonical
     *
     * @return BaseUser
     */
    public function setUsernameCanonical( $usernameCanonical = NULL )
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole( $role )
    {
        return in_array( strtoupper( $role ), $this->getRoles(), TRUE );
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        if ( TRUE === $this->expired ) {
            return FALSE;
        }

        if ( NULL !== $this->expiresAt && $this->expiresAt->getTimestamp() < time() ) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        if ( TRUE === $this->credentialsExpired ) {
            return FALSE;
        }

        if ( NULL !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time() ) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param int $ttl
     *
     * @return bool
     */
    public function isPasswordRequestNonExpired( $ttl )
    {
        return $this->getPasswordRequestedAt() instanceof \DateTime &&
               $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole( static::ROLE_SUPER_ADMIN );
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function removeRole( $role )
    {
        if ( FALSE !== $key = array_search( strtoupper( $role ), $this->roles, TRUE ) ) {
            unset( $this->roles[ $key ] );
            $this->roles = array_values( $this->roles );
        }

        return $this;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used during check for
     * changes and the id.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize( array(
                              $this->password,
                              $this->salt,
                              $this->usernameCanonical,
                              $this->username,
                              $this->expired,
                              $this->locked,
                              $this->credentialsExpired,
                              $this->enabled,
                              $this->id,
                              $this->expiresAt,
                              $this->credentialsExpireAt,
                              $this->email,
                              $this->emailCanonical,
                          ) );
    }

    /**
     * @param bool $bool
     *
     * @return $this
     */
    public function setSuperAdmin( $bool )
    {
        if ( TRUE === $bool ) {
            $this->addRole( static::ROLE_SUPER_ADMIN );
        } else {
            $this->removeRole( static::ROLE_SUPER_ADMIN );
        }

        return $this;
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize( $serialized )
    {
        $data = unserialize( $serialized );
        // add a few extra elements in the array to ensure that we have enough keys when unserializing
        // older data which does not include all properties.
        $data = array_merge( $data, array_fill( 0, 2, NULL ) );

        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->expiresAt,
            $this->credentialsExpireAt,
            $this->email,
            $this->emailCanonical
            ) = $data;
    }

    /**
     * @return bool
     */
    public function isCredentialsExpired()
    {
        return !$this->isCredentialsNonExpired();
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return !$this->isAccountNonExpired();
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked === TRUE;
    }

    /**
     * Set Locked
     *
     * @param mixed $locked
     *
     * @return BaseUser
     */
    public function setLocked( $locked = NULL )
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return !$this->isAccountNonLocked();
    }

    /**
     * @param GroupInterface $group
     *
     * @return $this
     */
    public function addGroup( GroupInterface $group )
    {
        if ( !$this->getGroups()->contains( $group ) ) {
            $this->getGroups()->add( $group );
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getGroupNames()
    {
        $names = array();
        foreach ( $this->getGroups() as $group ) {
            $names[] = $group->getName();
        }

        return $names;
    }

    /**
     * Gets the groups granted to the user.
     *
     * @return Collection
     */
    public function getGroups()
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    /**
     * Set Groups
     *
     * @param mixed $groups
     *
     * @return BaseUser
     */
    public function setGroups( $groups = NULL )
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return boolean
     */
    public function hasGroup( $name )
    {
        return in_array( $name, $this->getGroupNames() );
    }

    /**
     * @param GroupInterface $group
     *
     * @return $this
     */
    public function removeGroup( GroupInterface $group )
    {
        if ( $this->getGroups()->contains( $group ) ) {
            $this->getGroups()->removeElement( $group );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getUsername();
    }

    /**
     * @param null $format
     *
     * @return \DateTime|null|string
     */
    public function getLastLogin( $format = NULL )
    {
        if( empty( $this->lastLogin ) ) {
            $this->lastLogin = new \DateTime();
        }

        if ( !empty( $format ) ) {
            return $this->lastLogin->format( $format );
        }

        return $this->lastLogin;
    }

    /**
     * @param \DateTime|NULL $time
     *
     * @return $this
     */
    public function setLastLogin( \DateTime $time = NULL )
    {
        $this->lastLogin = $time;

        return $this;
    }

}