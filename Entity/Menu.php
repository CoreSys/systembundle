<?php

namespace CoreSys\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Menu
 *
 * @ORM\Table(name="core_menu")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\MenuRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Menu",
 *     title="Menu Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_menu_",
 *     ajax=@DT\Ajax(true, url="api_menu_post_menu_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("icon", title="Icon", className="all", render=@DT\Renderer("renderIcon", template="CoreSysCoreBundle:Renderers:renderIcon.js.twig")),
 *          @DT\Column("display", title="Display", className="all", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("type", title="Type", className="text-xs-center", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("parent", title="Parent", className="", render=@DT\Renderer("renderObjectName", template="CoreSysCoreBundle:Renderers:renderObjectName.js.twig")),
 *          @DT\Column("urlName", title="UrlName", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("url", title="Url", className="", render=@DT\Renderer\UrlRenderer()),
 *          @DT\Column("role", title="Role", className="", render=@DT\Renderer("renderObjectName", template="CoreSysCoreBundle:Renderers:renderObjectName.js.twig")),
 *          @DT\Column("position", title="Pos", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("internal", searchable=false, title="Internal", className="", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("admin", searchable=false, title="Admin", className="", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("active", searchable=false, title="Active", className="all", render=@DT\Renderer\CheckboxRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_menu_new_menu", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Menu")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_menu_edit_menu", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_menu_delete_menu", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Menu extends BaseEntity
{

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16, nullable=true)
     * @JMS\Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="display", type="string", length=64, nullable=true)
     * @JMS\Expose
     */
    private $display;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="parent", cascade={"persist"})
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\Menu>")
     */
    private $children;

    /**
     * @var Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("CoreSys\CoreBundle\Entity\Menu")
     */
    private $parent;

    /**
     * @var bool
     *
     * @ORM\Column(name="admin", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $admin;

    /**
     * @var bool
     *
     * @ORM\Column(name="internal", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $internal;

    /**
     * @var string
     *
     * @ORM\Column(name="urlName", type="string", length=128, nullable=true)
     * @JMS\Expose
     */
    private $urlName;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     * @JMS\Expose
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     * @JMS\Expose
     */
    private $alt;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("CoreSys\CoreBundle\Entity\Role")
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=32, nullable=true)
     * @JMS\Expose
     */
    private $icon;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $position;

    /**
     * @var array
     *
     * @ORM\Column(name="parameters", type="array", nullable=true)
     * @JMS\Expose
     */
    private $parameters;

    /**
     * Menu constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setPosition( 0 );
        $this->setAdmin( TRUE );
        $this->setInternal( TRUE );
        $this->setChildren( new ArrayCollection() );
        $this->setType( 'menu' );
        $this->setParameters( array() );
    }

    /**
     * @param Menu $menu
     *
     * @return $this
     */
    public function addChild( Menu $menu )
    {
        if ( !$this->children->contains( $menu ) ) {
            $this->children->add( $menu );
            $menu->setParent( $this );

            $count = $this->children->count();
            $menu->setPosition( $count );
        }

        return $this;
    }

    /**
     * @param Menu $menu
     *
     * @return $this
     */
    public function removeChild( Menu $menu )
    {
        if ( $this->children->contains( $menu ) ) {
            $menu->setParent( NULL );
            $this->children->removeElement( $menu );
        }

        return $this;
    }

    /**
     * Get parameters
     *
     * @return mixed
     */
    public function getParameters()
    {
        if ( !is_array( $this->parameters ) ) {
            $this->parameters = array();
        }

        return $this->parameters;
    }

    /**
     * Set Parameters
     *
     * @param mixed $parameters
     *
     * @return Menu
     */
    public function setParameters( $parameters = NULL )
    {
        $parameters       = is_array( $parameters ) ? $parameters : array();
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function addParameter( $key, $value )
    {
        $this->getParameters();
        $this->parameters[ $key ] = $value;

        return $this;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setParameter( $key, $value )
    {
        $this->addParameter( $key, $value );
    }

    /**
     * @param $key
     *
     * @return $this
     */
    public function removeParameter( $key )
    {
        $this->getParameters();
        if ( isset( $this->parameters[ $key ] ) ) {
            unset( $this->parameters[ $key ] );
        }

        return $this;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function hasParameter( $key )
    {
        $this->getParameters();

        return isset( $this->parameters[ $key ] );
    }

    /**
     * @ORM\PrePersist()
     */
    public function prepersist()
    {
        $baseController = $this->getBaseController();
        $internal       = $this->getInternal();
        if ( $internal ) {
            if ( !empty( $baseController ) ) {
                $urlName    = $this->getUrlName();
                $parameters = $this->getParameters();
                $parameters = is_array( $parameters ) ? $parameters : array();
                $url        = $baseController->generateUrl( $urlName, $parameters );
                $this->setUrl( $url );
            }
        }
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return intval( $this->position );
    }

    /**
     * Set Position
     *
     * @param int $position
     *
     * @return Menu
     */
    public function setPosition( $position = 0 )
    {
        $this->position = intval( $position );

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Menu
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return Menu
     */
    public function setType( $type = NULL )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get display
     *
     * @return string
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Set Display
     *
     * @param string $display
     *
     * @return Menu
     */
    public function setDisplay( $display = NULL )
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set Children
     *
     * @param ArrayCollection $children
     *
     * @return Menu
     */
    public function setChildren( $children = NULL )
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Menu
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set Parent
     *
     * @param Menu $parent
     *
     * @return Menu
     */
    public function setParent( $parent = NULL )
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get admin
     *
     * @return boolean
     */
    public function getAdmin()
    {
        return $this->admin === TRUE;
    }

    /**
     * Set Admin
     *
     * @param boolean $admin
     *
     * @return Menu
     */
    public function setAdmin( $admin = TRUE )
    {
        $this->admin = $admin === TRUE;

        return $this;
    }

    /**
     * Get internal
     *
     * @return boolean
     */
    public function getInternal()
    {
        return $this->internal === TRUE;
    }

    /**
     * Set Internal
     *
     * @param boolean $internal
     *
     * @return Menu
     */
    public function setInternal( $internal = TRUE )
    {
        $this->internal = $internal === TRUE;

        return $this;
    }

    /**
     * Get urlName
     *
     * @return string
     */
    public function getUrlName()
    {
        return $this->urlName;
    }

    /**
     * Set UrlName
     *
     * @param string $urlName
     *
     * @return Menu
     */
    public function setUrlName( $urlName = NULL )
    {
        $this->urlName = $urlName;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set Url
     *
     * @param string $url
     *
     * @return Menu
     */
    public function setUrl( $url = NULL )
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set Alt
     *
     * @param string $alt
     *
     * @return Menu
     */
    public function setAlt( $alt = NULL )
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get role
     *
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set Role
     *
     * @param Role $role
     *
     * @return Menu
     */
    public function setRole( $role = NULL )
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set Icon
     *
     * @param string $icon
     *
     * @return Menu
     */
    public function setIcon( $icon = NULL )
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get an array of the route names
     *
     * @return array
     */
    public function getRoutesList()
    {
        $list = array();
        if ( $this->getType() === 'menu' ) {
            $children = $this->getChildren();
            foreach ( $children as $child ) {
                if ( $child->getType() === 'item' ) {
                    $urlName = $child->getUrlName();
                    if ( !empty( $urlName ) ) {
                        $list[] = $urlName;
                    }
                } else if ( $child->getType() === 'menu' ) {
                    $menuList = $child->getRoutesList();
                    $list     = array_merge( $list, $menuList );
                }
            }
        } else {
            $urlName = $this->getUrlName();
            if( !empty( $urlName ) ) {
                $list[] = $urlName;
            }
        }

        return $list;
    }
}

