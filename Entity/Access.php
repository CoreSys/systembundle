<?php

namespace CoreSys\CoreBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Access
 *
 * @ORM\Table(name="core_access")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\AccessRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Access",
 *     title="Access Management",
 *     responsive=true,
 *     checkable=false,
 *     ajax=@DT\Ajax(true, url="api_post_access_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("path", title="Path", className="all", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("roles", title="Roles", className="none", render=@DT\Renderer(
 *              "renderAccessRoles",
 *              template="CoreSysCoreBundle:Renderers:renderAccessRoles.js.twig"
 *           )),
 *          @DT\Column("host", title="Host", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("ip", title="IP", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("anonymous", searchable=false, title="Anonymous", className="", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("methods", title="Methods", className="none", render=@DT\Renderer(
 *              "renderAccessMethods",
 *              template="CoreSysCoreBundle:Renderers:renderAccessMethods.js.twig"
 *          )),
 *          @DT\Column("channel", title="Channel", className="", render=@DT\Renderer\BlankRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_new_access", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Access")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_edit_access", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_delete_access", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Access extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, unique=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $path;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="access", cascade={"persist"})
     * @ORM\JoinTable(name="core_access_roles")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\Role>")
     * @JMS\MaxDepth(2)
     */
    private $roles;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $host;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=32, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $ip;

    /**
     * @var bool
     *
     * @ORM\Column(name="anonymous", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $anonymous;

    /**
     * @var array
     *
     * @ORM\Column(name="methods", type="array", nullable=true)
     * @JMS\Expose
     * @JMS\Type("array")
     */
    private $methods;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=64)
     * @JMS\Expose
     * @JMS\Type("string")
     * @Gedmo\Slug(fields={"path","id"})
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="mandatory", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $mandatory;

    /**
     * @var string
     *
     * @ORM\Column(length=64, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $channel;

    /**
     * Access constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setAnonymous( FALSE );
        $this->setMandatory( FALSE );
        $this->setRoles( new ArrayCollection() );
        $this->setMethods( array( 'GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD', 'LINK', 'UNLINK' ) );
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set Path
     *
     * @param string $path
     *
     * @return Access
     */
    public function setPath( $path = NULL )
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get roles
     *
     * @return ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set Roles
     *
     * @param ArrayCollection $roles
     *
     * @return Access
     */
    public function setRoles( $roles = NULL )
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole( Role $role )
    {
        if ( !$this->roles->contains( $role ) ) {
            $role->addAccess( $this );
            $this->roles->add( $role );
        }

        return $this;
    }

    public function removeRole( Role $role )
    {
        if ( $this->roles->contains( $role ) ) {
            $role->removeAccess( $this );
            $this->roles->removeElement( $role );
        }

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Access
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set Host
     *
     * @param string $host
     *
     * @return Access
     */
    public function setHost( $host = NULL )
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set Ip
     *
     * @param string $ip
     *
     * @return Access
     */
    public function setIp( $ip = NULL )
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get anonymous
     *
     * @return boolean
     */
    public function getAnonymous()
    {
        return $this->anonymous === TRUE;
    }

    /**
     * Set Anonymous
     *
     * @param boolean $anonymous
     *
     * @return Access
     */
    public function setAnonymous( $anonymous = TRUE )
    {
        $this->anonymous = $anonymous === TRUE;

        return $this;
    }

    /**
     * Get methods
     *
     * @return array
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * Set Methods
     *
     * @param array $methods
     *
     * @return Access
     */
    public function setMethods( $methods = NULL )
    {
        $this->methods = $methods;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set Slug
     *
     * @param string $slug
     *
     * @return Access
     */
    public function setSlug( $slug = NULL )
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get mandatory
     *
     * @return boolean
     */
    public function getMandatory()
    {
        return $this->mandatory === TRUE;
    }

    /**
     * Set Mandatory
     *
     * @param boolean $mandatory
     *
     * @return Access
     */
    public function setMandatory( $mandatory = TRUE )
    {
        $this->mandatory = $mandatory === TRUE;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->slug;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prepersist()
    {
        $this->prepersistMethods();
    }

    /**
     * Get channel
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set Channel
     *
     * @param string $channel
     *
     * @return Access
     */
    public function setChannel( $channel = NULL )
    {
        $this->channel = $channel;

        return $this;
    }

    public function getData()
    {
        $roles = array();
        foreach ( $this->getRoles() as $role ) {
            $roles[] = $role->getRoleName();
        }

        $return = array(
            'path'    => $this->getPath(),
            'roles'   => $roles,
            'ip'      => $this->getIp(),
            'host'    => $this->getHost(),
            'anon'    => $this->getAnonymous(),
            'channel' => $this->getChannel(),
            'methods' => $this->getMethods()
        );

        if ( $this->getAnonymous() === TRUE ) {
            $return[ 'roles' ][] = 'IS_AUTHENTICATED_ANONYMOUSLY';
        }

        return $return;
    }

    /**
     * PrePersist methods
     * Ensure that only valid entries are made
     */
    protected function prepersistMethods()
    {
        $methods = $this->getMethods();
        $results = array();
        $valid   = array( 'get', 'post', 'put', 'delete', 'head', 'link', 'unlink' );
        foreach ( $methods as $method ) {
            $method = strtolower( trim( $method ) );
            if ( in_array( $method, $valid ) ) {
                $results[] = strtoupper( $method );
            }
        }
        $this->setMethods( $results );
    }
}

