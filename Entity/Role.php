<?php

namespace CoreSys\CoreBundle\Entity;

use CoreSys\CoreBundle\Controller\BaseController;
use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Role
 *
 * @ORM\Table(name="core_role")
 * @ORM\Entity(repositoryClass="CoreSys\CoreBundle\Repository\RoleRepository")
 * @ORM\HasLifecycleCallbacks()
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Role",
 *      title="Role Management",
 *      responsive=true,
 *      checkable=true,
 *      ajax=@DT\Ajax(true, url="api_post_role_datatables"),
 *      columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all", render=@DT\Renderer(
 *              "renderRoleName",
 *              template="CoreSysCoreBundle:Renderers:renderRoleName.js.twig"
 *           )),
 *          @DT\Column("access", title="Access", className="none", render=@DT\Renderer(
 *              "renderRoleAccess",
 *               template="CoreSysCoreBundle:Renderers:renderRoleAccess.js.twig"
 *          )),
 *          @DT\Column("parent", title="Parent", orderable=false, searchable=false, className="", render=@DT\Renderer(
 *              "renderRoleParent",
 *              template="CoreSysCoreBundle:Renderers:renderRoleParent.js.twig"
 *          )),
 *          @DT\Column("mandatory", searchable=false, title="Mandatory", className="none", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("switch", searchable=false, title="Switch", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("active", searchable=false, title="Active", render=@DT\Renderer\CheckboxRenderer())
 *      },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_new_role", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Role")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_edit_role", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_delete_role", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Role extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, unique=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="mandatory", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $mandatory;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\Role")
     */
    private $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Role", mappedBy="parent")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\Role>")
     * @JMS\MaxDepth(2)
     */
    private $children;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Access", mappedBy="roles")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<CoreSys\CoreBundle\Entity\Access>")
     * @JMS\MaxDepth(2)
     */
    private $access;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CoreSys\CoreBundle\Entity\User", mappedBy="sys_roles", cascade={"persist"})
     */
    private $users;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     * @JMS\Type("integer")
     * @var integer
     */
    private $usersCount;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=64, unique=true)
     * @JMS\Expose
     * @JMS\Type("string")
     * @Gedmo\Slug(fields={"name","id"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=12, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $color;

    /**
     * @var bool
     *
     * @ORM\Column(name="switch", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $switch;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="role_name", type="string", length=32, unique=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $roleName;

    public function __construct()
    {
        $this->setMandatory( FALSE );
        $this->setChildren( new ArrayCollection() );
        $this->setAccess( new ArrayCollection() );
        $this->setUsers( new ArrayCollection() );
        $this->setActive( TRUE );
        $this->setColor( '#428bca' );
        $this->setSwitch( FALSE );
    }

    /**
     * Get usersCount
     *
     * @return int
     */
    public function getUsersCount()
    {
        return intval( $this->usersCount );
    }

    /**
     * Set UsersCount
     *
     * @param int $usersCount
     *
     * @return Role
     */
    public function setUsersCount( $usersCount = 0 )
    {
        $this->usersCount = intval( $usersCount );

        return $this;
    }

    /**
     * Get actions
     *
     * @return mixed
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Set Actions
     *
     * @param mixed $actions
     *
     * @return Role
     */
    public function setActions( $actions = NULL )
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * Get baseController
     *
     * @return BaseController
     */
    public function getBaseController()
    {
        return parent::getBaseController();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return intval( $this->id );
    }

    /**
     * Set Id
     *
     * @param int $id
     *
     * @return Role
     */
    public function setId( $id = 0 )
    {
        $this->id = intval( $id );

        return $this;
    }

    /**
     * Set BaseController
     *
     * @param BaseController $baseController
     *
     * @return BaseEntity
     */
    public function setBaseController( $baseController = NULL )
    {
        parent::setBaseController( $baseController );

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get mandatory
     *
     * @return boolean
     */
    public function getMandatory()
    {
        return $this->mandatory === TRUE;
    }

    /**
     * Set Mandatory
     *
     * @param boolean $mandatory
     *
     * @return Role
     */
    public function setMandatory( $mandatory = TRUE )
    {
        $this->mandatory = $mandatory === TRUE;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Role
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set Parent
     *
     * @param Role $parent
     *
     * @return Role
     */
    public function setParent( Role $parent = NULL )
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set Children
     *
     * @param ArrayCollection $children
     *
     * @return Role
     */
    public function setChildren( $children = NULL )
    {
        $this->children = $children;

        return $this;
    }

    public function addChild( Role $role )
    {
        if ( !$this->children->contains( $role ) ) {
            $role->setParent( $this );
            $this->children->add( $role );
        }

        return $this;
    }

    public function removeChild( Role $role )
    {
        if ( $this->children->contains( $role ) ) {
            $role->setParent( NULL );
            $this->children->removeElement( $role );
        }

        return $this;
    }

    /**
     * Get access
     *
     * @return ArrayCollection
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set Access
     *
     * @param ArrayCollection $access
     *
     * @return Role
     */
    public function setAccess( $access = NULL )
    {
        $this->access = $access;

        return $this;
    }

    public function addAccess( Access $access )
    {
        if ( !$this->access->contains( $access ) ) {
            $access->addRole( $this );
            $this->access->add( $access );
        }

        return $this;
    }

    public function removeAccess( Access $access )
    {
        if ( $this->access->contains( $access ) ) {
            $access->removeRole( $this );
            $this->access->removeElement( $access );
        }

        return $this;
    }

    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set Users
     *
     * @param ArrayCollection $users
     *
     * @return Role
     */
    public function setUsers( $users = NULL )
    {
        $this->users = $users;

        return $this;
    }

    public function addUser( User $user )
    {
        if ( !$this->users->contains( $user ) ) {
            $this->users->add( $user );
        }

        return $this;
    }

    public function removeUser( User $user )
    {
        if ( $this->users->contains( $user ) ) {
            $this->users->removeElement( $user );
        }

        return $this;
    }

    /**
     * Get slug
     *
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set Slug
     *
     * @param mixed $slug
     *
     * @return Role
     */
    public function setSlug( $slug = NULL )
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set Color
     *
     * @param string $color
     *
     * @return Role
     */
    public function setColor( $color = NULL )
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get switch
     *
     * @return boolean
     */
    public function getSwitch()
    {
        return $this->switch === TRUE;
    }

    /**
     * Set Switch
     *
     * @param boolean $switch
     *
     * @return Role
     */
    public function setSwitch( $switch = TRUE )
    {
        $this->switch = $switch === TRUE;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Role
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get roleName
     *
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * Set RoleName
     *
     * @param string $roleName
     *
     * @return Role
     */
    public function setRoleName( $roleName = NULL )
    {
        $this->roleName = $roleName;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return int
     */
    public function usersCount()
    {
        return count( $this->getUsers() );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getRoleName();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepersist()
    {
        if ( empty( $this->color ) ) {
            $this->setColor( '#428bca' );
        }

        $name     = $this->getName();
        $roleName = 'ROLE_' . strtoupper( str_replace( ' ', '_', trim( $name ) ) );
        $this->setRoleName( $roleName );

        $this->usersCount = count( $this->getUsers() );
    }

    /**
     * @JMS\VirtualProperty
     * @return string
     */
    public function getLabel()
    {
        $color = $this->getColor();
        $style = 'style="margin-right:.5rem;background-color:' . $color . ' !important"';
        $text  = $this->getName();

        return '<span class="label label-default" ' . $style . '>' . $text . '</span>';
    }
}

