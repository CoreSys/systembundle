<?php

namespace CoreSys\CoreBundle\Annotation\Form;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Form
 * @package CoreSys\CoreBundle\Annotation\Form
 * @Annotation
 */
class Form
{

    /**
     * @var array
     */
    public $value = array();

    /**
     * @return array
     */
    public function getOptions()
    {
        $options = $this->value;
        if ( !is_array( $options ) ) {
            $options = array();
        }

        if ( !isset( $options[ 'attr' ] ) ) {
            $options[ 'attr' ] = array();
        }

        if ( !isset( $options[ 'attr' ][ 'class' ] ) ) {
            $options[ 'attr' ][ 'class' ] = 'form form-horizontal';
        }

        return $options;
    }
}