<?php

namespace CoreSys\CoreBundle\Annotation\Form;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Field
 * @package CoreSys\CoreBundle\Annotation\Form
 * @Annotation
 */
class Field
{

    /**
     * @var string
     */
    public $value;

    /**
     * @var boolean
     */
    public $required = TRUE;

    /**
     * @var string
     */
    public $type = 'text';

    /**
     * @var string
     */
    public $customType;

    /**
     * @var array
     */
    public $options = array();

    /**
     * @return string
     */
    public function getName()
    {
        if ( isset( $this->customType ) && !empty( $this->customType ) ) {
            return $this->customType;
        }

        $type = trim( $this->type );
        $type = preg_replace( '/[^a-zA-Z]+/', '', $type );

        switch ( $type ) {
            case 'datetime':
                $type = 'DateTime';
                break;
            default:
                $type = 'text';
                break;
        }

        return 'Symfony\\Component\\Form\\Extension\Core\\Type\\' . ucwords( $type ) . 'Type';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        $required              = !!$this->required;
        $options               = is_array( $this->options ) ? $this->options : array();
        $optoins[ 'required' ] = $required;

        return $options;
    }
}