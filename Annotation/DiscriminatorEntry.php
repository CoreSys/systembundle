<?php

namespace CoreSys\CoreBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class DiscriminatorEntry
 * @package CoreSys\CoreBundle\Annotation
 * @Annotation
 */
class DiscriminatorEntry
{

    public $value;
}