<?php

namespace CoreSys\CoreBundle\Annotation\Datatables\Renderer;

use CoreSys\CoreBundle\Annotation\Datatables\Renderer;

/**
 * Class MoneyRenderer
 * @package CoreSys\CoreBundle\Annotation\Datatables\Renderer
 * @Annotation
 */
class MoneyRenderer extends Renderer
{
    /**
     *
     * @var string
     */
    public $value = 'renderMoneyColumn';

    /**
     * @var string
     */
    public $template = 'CoreSysCoreBundle:Datatables:Renderer\money.js.twig';

    /**
     * @var array
     */
    public $parameters = array();
}