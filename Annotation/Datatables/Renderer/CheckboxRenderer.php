<?php

namespace CoreSys\CoreBundle\Annotation\Datatables\Renderer;

use CoreSys\CoreBundle\Annotation\Datatables\Renderer;

/**
 * Class CheckboxRenderer
 * @package CoreSys\CoreBundle\Annotation\Datatables\Renderer
 * @Annotation
 */
class CheckboxRenderer extends Renderer
{

    /**
     *
     * @var string
     */
    public $value = 'renderCheckboxColumn';

    /**
     * @var string
     */
    public $template = 'CoreSysCoreBundle:Datatables:Renderer\checkbox.js.twig';

    /**
     * @var bool
     */
    public $click = FALSE;

    /**
     * @var array
     */
    public $ajax = array();

    /**
     * @var mixed
     */
    public $confirm = FALSE;

    /**
     * @return array
     */
    public function getParameters()
    {
        $parameters = parent::getParameters();
        $parameters = array_merge( $parameters, array(
            'click'   => !!$this->click,
            'ajax'    => $this->ajax,
            'confirm' => $this->confirm
        ) );

        return $parameters;
    }
}