<?php

namespace CoreSys\CoreBundle\Annotation\Datatables\Renderer;

/**
 * Class Actions
 * @package CoreSys\CoreBundle\Annotation\Datatables\Renderer
 * @Annotation
 */
class Actions implements RendererInterface
{

    /**
     *
     * @var string
     */
    public $value = 'renderActionsColumn';

    /**
     * @var string
     */
    public $template = 'CoreSysCoreBundle:Datatables:Renderer\actions.js.twig';

    /**
     * @var array
     */
    public $parameters = array();
}