<?php

namespace CoreSys\CoreBundle\Annotation\Datatables\Renderer;

use CoreSys\CoreBundle\Annotation\Datatables\Renderer;

/**
 * Class DateRenderer
 * @package CoreSys\CoreBundle\Annotation\Datatables\Renderer
 * @Annotation
 */
class DateRenderer extends Renderer
{
    /**
     *
     * @var string
     */
    public $value = 'renderDateColumn';

    /**
     * @var string
     */
    public $template = 'CoreSysCoreBundle:Datatables:Renderer\date.js.twig';

    /**
     * @var array
     */
    public $parameters = array('format' => 'M-d-Y h:i a');
}