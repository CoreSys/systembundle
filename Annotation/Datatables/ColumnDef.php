<?php

namespace CoreSys\CoreBundle\Annotation\Datatables;

/**
 * Class ColumnDef
 * @package CoreSys\CoreBundle\Annotation\Datatables
 * @Annotation
 */
class ColumnDef
{

    /**
     * AkA targets
     * @var mixed
     */
    public $value;

    /**
     * @var bool
     */
    public $orderable;


    /**
     * @var bool
     */
    public $searchable;

    /**
     * @var bool
     */
    public $visible;

    /**
     * @var string
     */
    public $className;

    /**
     * @return array
     */
    public function getOptions()
    {
        $options = array( 'targets' => $this->value );
        foreach ( get_class_vars( get_class( $this ) ) as $var => $val ) {
            if ( $var !== 'value' ) {
                $value = $this->$var;
                if ( !empty( $value ) ) {
                    $options[ $var ] = $value;
                }
            }
        }

        return $options;
    }
}