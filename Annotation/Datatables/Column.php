<?php

namespace CoreSys\CoreBundle\Annotation\Datatables;

/**
 * Class Column
 * @package CoreSys\CoreBundle\Annotation\Datatables
 * @Annotation
 */
class Column
{

    /**
     * THIS IS ALSO THE DATA ATTRIBUTE
     *
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $title;

    /**
     * @var bool
     */
    public $searchable = TRUE;

    /**
     * @var bool
     */
    public $orderable = TRUE;

    /**
     * @var string
     */
    public $className;

    /**
     * @var CoreSys\CoreBundle\Annotation\Datatables\Renderer
     */
    public $render;

    /**
     * @var integer
     */
    public $width;

    /**
     * @return array
     */
    public function getOptions()
    {
        $options = array( 'data' => $this->value );
        foreach ( get_class_vars( get_class( $this ) ) as $var => $val ) {
            if ( $var !== 'value' ) {
                $value = $this->$var;
                if ( $value !== NULL ) {
                    $options[ $var ] = $value;
                }
            }
        }

        return $options;
    }
}