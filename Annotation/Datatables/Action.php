<?php

namespace CoreSys\CoreBundle\Annotation\Datatables;

/**
 * Class Action
 * @package CoreSys\CoreBundle\Annotation\Datatables
 * @Annotation
 */
class Action
{

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $resource;

    /**
     * @var string
     */
    public $type = 'button';

    /**
     * @var string
     */
    public $url;

    /**
     * @var array
     */
    public $urlParams;

    /**
     * @var string
     */
    public $tooltip;

    /**
     * @var string
     */
    public $buttonType;

    /**
     * @var string
     */
    public $iconClass;

    /**
     * @var string
     */
    public $target = '_self';

    /**
     * @var string
     */
    public $text;

    /**
     * @var bool
     */
    public $internal = FALSE;

    /**
     * @return array
     */
    public function getOptions()
    {
        $options = array(
            'item'     => $this->getItem(),
            'action'   => $this->value,
            'dataItem' => $this->getDataItem()
        );
        foreach ( get_class_vars( get_class( $this ) ) as $var => $val ) {
            if ( $var !== 'value' ) {
                $value = $this->$var;
                if ( $value !== 0 && !empty( $value ) ) {
                    $options[ $var ] = $value;
                }
            }
        }

        return $options;
    }

    /**
     * @return string
     */
    public function getItem()
    {
        if ( $this->type === 'a' ) {
            return $this->getLink();
        }

        return $this->getButton();
    }

    public function getDataItem()
    {
        if ( $this->type === 'a' ) {
            return $this->getDataLink();
        } else {
            return $this->getDataButton();
        }
    }

    /**
     * @return string
     */
    public function getButton()
    {
        $button = '<button type="button" data-action="' . $this->value . '"';
        $button .= ' data-type="' . $this->type . '"';
        $button .= ' class="btn btn-' . $this->buttonType . '"';
        $button .= ' data-url="' . $this->url . '"';

        switch ( strtolower( trim( $this->value ) ) ) {
            case 'create':
            case 'add':
            case 'insert':
                $button .= ' onclick="createNew' . $this->resource . '($(this))"';
                break;
            case 'link':
                $button .= ' onclick="followLink' . $this->resource . '($(this))"';
                break;
        }

        $button .= '>';
        if ( !empty( $this->iconClass ) ) {
            $button .= '<i class="' . $this->iconClass . '"></i>';
        }
        if ( !empty( $this->text ) ) {
            $button .= ' ' . $this->text;
        }
        $button .= '</button>';

        return $button;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        $link = '<a data-action="' . $this->value . '" href="' . $this->url . '" target="' . $this->target . '"';
        $link .= ' data-type="' . $this->type . '"';
        if ( !empty( $this->buttonType ) ) {
            $link .= ' class="btn btn-' . $this->buttonType . '"';
        }
        $link .= '>';
        if ( !empty( $this->iconClass ) ) {
            $link .= '<i class"' . $this->iconClass . '"></i>';
        }
        if ( !empty( $this->text ) ) {
            $link .= $this->text;
        }
        $link .= '</a>';

        return $link;
    }

    public function getDataButton()
    {
        $button   = array( '<button type="button" class="btn btn-' . $this->buttonType . '" ' );
        $button[] = 'data-type="' . $this->type . '"';
        $button[] = 'data-action="' . $this->value . '" ';
        $button[] = 'data-url="' . $this->url . '" ';

        switch ( strtolower( trim( $this->value ) ) ) {
            case 'add':
            case 'create':
            case 'insert':
                $button[] = 'onclick="createNew' . $this->resource . '($(this))" ';
            case 'edit':
            case 'alter':
                $button[] = 'onclick="edit' . $this->resource . '($(this))" ';
                break;
            case 'remove':
            case 'delete':
            case 'trash':
                $button[] = 'onclick="remove' . $this->resource . '($(this))" ';
                break;
            case 'link':
                $button[] = 'onclick="followLink' . $this->resource . '($(this))" ';
                break;
            case 'view':
                $button[] = 'onclick="view' . $this->resource . '($(this))" ';
                break;
        }

        if ( is_array( $this->urlParams ) ) {
            $params = array();
            foreach ( $this->urlParams as $k => $v ) {
                $params[ $k ] = $v;
            }
            $params   = json_encode( $params );
            $button[] = 'data-url-params=\\\'' . $params . '\\\' ';
        }

        if ( !empty( $this->tooltip ) ) {
            $button[] = 'data-toggle="tooltip" title="' . $this->tooltip . '" ';
        }

        $button[] = '>';
        if ( !empty( $this->iconClass ) ) {
            $button[] = '<i class="' . $this->iconClass . '"></i> ';
        }
        if ( !empty( $this->text ) ) {
            $button[] = $this->text;
        }

        $button[] = '</button>';

        return implode( '', $button );
    }

    public function getDataLink()
    {

    }
}