<?php

namespace CoreSys\CoreBundle\Repository;

use CoreSys\CoreBundle\Entity\Configuration;

/**
 * Class ConfigurationRepository
 * @package CoreSys\CoreBundle\Repository
 */
class ConfigurationRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return array
     */
    public function findAll()
    {
        return array(
            $this->getConfiguration()
        );
    }

    /**
     * @return Configuration|null|object
     */
    public function getConfig()
    {
        return $this->getConfiguration();
    }

    /**
     * @return Configuration|null|object
     */
    public function getSettings()
    {
        return $this->getConfiguration();
    }

    /**
     * @return Configuration|null|object
     */
    public function getConfiguration()
    {
        $config = $this->find( 1 );
        if ( empty( $config ) ) {
            $config = new Configuration();
            $this->getEntityManager()->persist( $config );
            $this->getEntityManager()->flush();
        }

        return $config;
    }
}
