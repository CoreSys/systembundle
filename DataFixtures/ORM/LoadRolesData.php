<?php

namespace CoreSys\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CoreSys\CoreBundle\Entity\Role;

/**
 * Class LoadRolesData
 * @package CoreSys\CoreBundle\DataFixtures\ORM
 */
class LoadRolesData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var
     */
    private $container;

    /**
     * @param ContainerInterface|NULL $container
     */
    public function setContainer( ContainerInterface $container = NULL )
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load( ObjectManager $manager )
    {
        $roleManager = $this->container->get( 'core_sys_core.manager.roles' );
        $roleManager->dumpYml( );
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}