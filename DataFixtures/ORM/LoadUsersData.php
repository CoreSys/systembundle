<?php

namespace CoreSys\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CoreSys\CoreBundle\Entity\Role;
use CoreSys\CoreBundle\Entity\User;

/**
 * Class LoadUsersData
 * @package CoreSys\CoreBundle\DataFixtures\ORM
 */
class LoadUsersData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var
     */
    private $container;

    /**
     * @var array
     */
    private $roles = array();

    /**
     * @param ContainerInterface|NULL $container
     */
    public function setContainer( ContainerInterface $container = NULL )
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load( ObjectManager $manager )
    {
        $users = array(
            array( 'username' => 'admin', 'password' => 'dd', 'email' => 'sales@mk2solutions.com', 'roles' => array( 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_SUPERADMIN', 'ROLE_OWNER' ) ),
            array( 'username' => 'kronox', 'password' => 'pa55w0rd', 'email' => 'kronox@shaw.ca', 'gravatar_use' => TRUE, 'roles' => array( 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_SUPERADMIN', 'ROLE_OWNER' ) )
        );

        $userManager = $this->container->get( 'core_sys_core.manager.user' );
        foreach ( $users as $userData ) {
            $user = new User();
            foreach ( $userData as $k => $v ) {
                if ( $k === 'roles' ) {
                    $sysRoles = $this->locateRoles( $manager, $v );
                    foreach ( $sysRoles as $sysRole ) {
                        $user->addSysRole( $sysRole );
                    }
                    $user->setRoles( $v );
                } else if ( $k === 'password' ) {
                    $user->setPlainPassword( $v );
                } else {
                    $method = 'set' . str_replace( ' ', '', ucwords( str_replace( '_', ' ', $k ) ) );
                    if ( method_exists( $user, $method ) ) {
                        $user->$method( $v );
                    }
                }
            }

            $userManager->updateUser( $user );
        }
    }

    /**
     * @param ObjectManager $manager
     * @param array         $roleNames
     *
     * @return array
     */
    protected function locateRoles( ObjectManager &$manager, $roleNames = array() )
    {
        $return = array();

        $repo = $manager->getRepository( 'CoreSysCoreBundle:Role' );
        foreach ( $roleNames as $rname ) {
            if ( array_key_exists( $rname, $this->roles ) ) {
                $return[] = $this->roles[ $rname ];
            } else {
                $role = $repo->findOneBy( array( 'roleName' => $rname ) );
                if ( $role instanceof Role ) {
                    $this->roles[ $rname ] = $role;
                    $return[]              = $role;
                }
            }
        }

        return $return;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}