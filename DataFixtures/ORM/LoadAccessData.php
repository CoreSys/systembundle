<?php

namespace CoreSys\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CoreSys\CoreBundle\Entity\Role;
use CoreSys\CoreBundle\Entity\Access;

/**
 * Class LoadAccessData
 * @package CoreSys\CoreBundle\DataFixtures\ORM
 */
class LoadAccessData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var
     */
    private $container;

    /**
     * @param ContainerInterface|NULL $container
     */
    public function setContainer( ContainerInterface $container = NULL )
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load( ObjectManager $manager )
    {
        $items = array(
            array( 'path' => '^/login$', 'anonymous' => TRUE ),
            array( 'path' => '^/logout$', 'anonymous' => TRUE ),
            array( 'path' => '^/admin', 'anonymous' => FALSE, 'role' => 'ROLE_ADMIN' ),
            array( 'path' => '^/members', 'anonymous' => FALSE, 'role' => 'ROLE_MEMBER' ),
            array( 'path' => '^/api', 'anonymous' => TRUE, 'host' => 'hostname.com' )
        );

        foreach ( $items as &$item ) {
            $access = new Access();
            foreach ( $item as $k => $v ) {
                if ( $k === 'role' ) {
                    // dont do this here
                    $item[ 'access' ] = $access;
                } else {
                    $key = 'set' . ucfirst( $k );
                    if ( method_exists( $access, $key ) ) {
                        $access->$key( $v );
                    }
                }
            }
            $manager->persist( $access );
        }

        $manager->flush();
        $accessManager = $this->container->get( 'core_sys_core.manager.access' );

        // now lets do the roles
//        foreach ( $items as $item ) {
//            if ( isset( $item[ 'access' ] ) && $item[ 'access' ] instanceof Access ) {
//                if ( isset( $item[ 'role' ] ) && !empty( $item[ 'role' ] ) ) {
//                    $roleName = $item[ 'role' ];
//                    $accessManager->addRoleToAccess( $item[ 'access' ], $roleName );
//                }
//            }
//        }

        $accessManager->dumpYml( );
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * @param ObjectManager $manager
     * @param               $roleName
     *
     * @return mixed
     */
    protected function findRole( ObjectManager &$manager, $roleName )
    {
        $repo = $manager->getRepository( 'CoreSysCoreBundle:Role' );
        $role = $repo->findOneBy( array( 'roleName' => $roleName ) );

        return $role;
    }
}