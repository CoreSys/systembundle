<?php

namespace CoreSys\CoreBundle\DataFixtures\ORM;

use CoreSys\CoreBundle\Entity\Menu;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CoreSys\CoreBundle\Entity\Role;
use CoreSys\CoreBundle\Entity\Access;

/**
 * Class LoadAccessData
 * @package CoreSys\CoreBundle\DataFixtures\ORM
 */
class LoadMenusData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load( ObjectManager $manager )
    {
        $menus = array(
            'Configuration' => array(
                'type'     => 'menu',
                'admin'    => TRUE,
                'internal' => TRUE,
                'icon'     => 'fa fa-gears',
                'children' => array(
                    'Site Config'     => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_configuration',
                        'icon'     => 'fa fa-gear'
                    ),
                    'Admin Menus'     => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_menus',
                        'icon'     => 'fa fa-list-ul'
                    ),
                    'User Roles'      => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_configuration_roles',
                        'icon'     => 'fa fa-warning'
                    ),
                    'User Access'     => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_configuration_access',
                        'icon'     => 'fa fa-exclamation'
                    ),
                    'User Management' => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_configuration_users',
                        'icon'     => 'fa fa-users'
                    )
                )
            ),
            'Media'         => array(
                'type'     => 'menu',
                'admin'    => TRUE,
                'internal' => TRUE,
                'icon'     => 'fa fa-folder-open',
                'children' => array(
                    'Image Sizes' => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_media_image_sizes',
                        'icon'     => 'fa fa-image'
                    ),
                    'Images'      => array(
                        'type'     => 'item',
                        'admin'    => TRUE,
                        'internal' => TRUE,
                        'urlName'  => 'admin_media_images',
                        'icon'     => 'fa fa-list-ol'
                    )
                )
            )
        );

        foreach ( $menus as $display => $menuData ) {
            $this->addMenu( $manager, $display, $menuData, NULL );
        }

        $manager->flush();
    }

    public function addMenu( ObjectManager &$manager, $display, $data, Menu $parent = NULL )
    {
        $menu = new Menu();
        $menu->setDisplay( $display );
        if ( !empty( $parent ) ) {
            $menu->setParent( $parent );
        }

        foreach ( $data as $k => $v ) {
            if ( $k !== 'children' ) {
                $method = 'set' . ucwords( $k );
                if ( method_exists( $menu, $method ) ) {
                    $menu->$method( $v );
                }
            }
        }

        $manager->persist( $menu );

        $children = isset( $data[ 'children' ] ) ? $data[ 'children' ] : array();
        if ( is_array( $children ) && count( $children ) > 0 ) {
            foreach ( $children as $childDisplay => $childData ) {
                $this->addMenu( $manager, $childDisplay, $childData, $menu );
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }
}