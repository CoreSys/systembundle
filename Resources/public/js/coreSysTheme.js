;(function ( $ ) {
    "use strict";

    var CoreSysTheme = function () {
        this.noticeType = 'notify';
        this.switchType = 'toggle';
        this.alertType  = 'sweet';
        this.selectType = 'bootstrap';
    };

    CoreSysTheme.prototype.setSelectType = function ( type ) {
        this.selectType = type;
    };

    CoreSysTheme.prototype.setAlertType = function ( type ) {
        this.alertType = type;
    };

    CoreSysTheme.prototype.setSwitchType = function ( type ) {
        this.switchType = type;
    };

    CoreSysTheme.prototype.init = function () {
        this.initPostAjax();
        this.reload();
    };

    CoreSysTheme.prototype.initSwitches = function ( contents ) {
        if ( this.switchType === 'bootstrap' || this.switchType === 'bootstrapswitch' ) {
            this.initBootstrapSwitches( contents );
        } else if ( this.switchType = 'toggle' ) {
            this.initToggleSwitches( contents );
        }
    };

    CoreSysTheme.prototype.initToggleSwitches = function ( contents ) {
        $( 'input[type="checkbox"]' ).not( '.no-toggle' ).each( function () {
            var $this   = $( this ),
                $parent = $this.closest( 'span.checkbox-toggle' ),
                $label  = $this.closest( 'label' );

            if ( $parent.length === 0 ) {
                $this.wrap( '<span class="checkbox-toggle" />' );
                if ( $label.length === 0 ) {
                    var id = $this.attr( 'id' );
                    $this.closest( '.checkbox-toggle' ).append( '<label for="' + id + '" />' );
                }
            }
        } );
    };

    CoreSysTheme.prototype.initBootstrapSwitches = function ( contents ) {
        if ( $.fn.bootstrapSwitch ) {
            var load = true;
            if ( contents !== null && contents !== undefined ) {
                load = ( contents + '' ).indexOf( 'type="checkbox"' ) > 0;
            }
            if ( load ) {
                setTimeout( function () {
                    $( 'input[type=checkbox]' ).bootstrapSwitch( {
                        onText:  '<i class="fa fa-check text-success"></i>',
                        offText: '<i class="fa fa-close text-danger"></i>'
                    } );
                }, 250 );
            }
        }
    };

    CoreSysTheme.prototype.initTabs = function () {
        $( 'ul.nav-tabs a[data-toggle="tab"]' ).off( 'click' ).on( 'click', function ( e ) {
            e.preventDefault();
            $( this ).tab( 'show' );
        } );
    };

    CoreSysTheme.prototype.removeInitialLoading = function () {
        $( '.initialLoading' ).removeClass( 'initialLoading' );
    };

    CoreSysTheme.prototype.initPostAjax = function () {
        var self = this;
        $( document ).ajaxComplete( function ( event, xhr ) {
            var contents = xhr.responseText;
            self.reload( contents );
        } );
    };

    CoreSysTheme.prototype.reload = function ( ajaxContents ) {
        this.initFancybox();
        this.initConfirmations();
        this.initTooltips();
        this.initSwitches( ajaxContents );
        this.initTabs();
        this.initSummernote();
        this.initSelects();
        this.removeInitialLoading();
        this.initTabDrop();
    };

    CoreSysTheme.prototype.initTabDrop = function () {
        if ( $.fn.tabdrop ) {
            $( '.nav-tabs ' ).not( '[data-dropped]' ).each( function () {
                $( this ).tabdrop();
                $( this ).attr( 'data-dropped', 1 );
            } );
        }
    };

    CoreSysTheme.prototype.initSelects = function () {
        if ( this.selectType === 'bootstrap' && $.fn.selectpicker ) {
            $( 'select[rel="selectpicker"]' ).selectpicker( {
                liveSearch: true
            } );
        }
    };

    CoreSysTheme.prototype.initTooltips = function () {
        if ( $.fn.tooltip ) {
            $( '[data-toggle="tooltip"],[data-tooltip]' ).not( '[data-tooltip-loaded]' ).each( function () {
                $( this ).tooltip( {container: 'body'} );
                $( this ).attr( 'data-tooltip-loaded', 1 );
            } );
        }
    };

    CoreSysTheme.prototype.initConfirmations = function () {
        if ( $.fn.confirmation ) {
            setTimeout( function () {
                $( '[data-toggle="confirmation"],[data-confirmation]' ).not( '[data-confirmed]' ).each( function () {
                    var $el = $( this );
                    $el.confirmation( {
                        placement:  'left',
                        btnOkClass: 'btn btn-sm btn-primary',
                        btnOkLabel: 'OK'
                    } );
                    $el.attr( 'data-confirmed', 1 );
                } );
            }, 250 );
        }
    };

    CoreSysTheme.prototype.initFancybox = function () {
        if ( $.fn.fancybox ) {
            $( '.fancybox' ).fancybox();
        }
    };

    CoreSysTheme.prototype.initSummernote = function () {
        if ( $.fn.summernote ) {
            $( 'textarea[rel=summernote]' ).each( function () {
                var $el    = $( this ),
                    rows   = $el.attr( 'rows' ),
                    height = 75;
                if ( rows === null || rows === undefined ) {
                    rows = 5;
                }
                height = rows * 15;
                $el.summernote( {height: height} );
            } );
        }
    };

    CoreSysTheme.prototype.setNoticeType = function ( type ) {
        this.noticeType = type;
    };

    CoreSysTheme.prototype.showNotice = function ( type, message ) {
        if ( type === 'error' ) {
            type === 'danger';
        }

        var icon  = null,
            title = null;
        if ( type === 'success' ) {
            icon  = 'fa fa-check';
            title = 'Success';
        } else if ( type === 'warning' ) {
            icon  = 'fa fa-warning';
            title = 'Warning';
        } else if ( type === 'info' ) {
            icon  = 'fa fa-exclamation';
            title = 'Info';
        } else if ( type === 'danger' ) {
            icon  = 'fa fa-minus-circle'
            title = 'Error';
        }

        if ( this.noticeType === 'notify' && $.notify ) {
            $.notify( {
                message: message,
                icon:    icon,
                title:   title
            }, {
                type:          type,
                element:       'body',
                allow_dismiss: true,
                newest_on_top: true,
                delay:         10000,
                placement:     {
                    from:  'top',
                    align: 'center'
                }
            } );
        }
    };

    CoreSysTheme.prototype.confirmDelete = function ( message, callback ) {
        if ( this.alertType === 'sweet' || this.alertType === 'swal' ) {
            if ( typeof swal === 'function' ) {
                swal( {
                    title:              'Are you sure?',
                    text:               message,
                    type:               'error',
                    showCancelButton:   true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText:  'Delete',
                    closeOnConfirm:     true
                }, callback );
            }
        }
    };

    CoreSysTheme.prototype.confirm = function ( message, callback ) {
        if ( this.alertType === 'sweet' || this.alertType === 'swal' ) {
            if ( typeof swal === 'function' ) {
                swal( {
                    title:              'Are you sure?',
                    text:               message,
                    type:               'warning',
                    showCancelButton:   true,
                    confirmButtonClass: 'btn-warning',
                    confirmButtonText:  'Yes',
                    closeOnConfirm:     true
                }, callback );
            }
        }
    };

    CoreSysTheme.prototype.success = function ( message ) {
        if ( this.alertType === 'sweet' || this.alertType === 'swal' ) {
            if ( typeof swal === 'function' ) {
                swal( "Success!", message, "success" )
            }
        }
    };

    CoreSysTheme.prototype.error = function ( message ) {
        if ( this.alertType === 'sweet' || this.alertType === 'swal' ) {
            if ( typeof swal === 'function' ) {
                swal( "Error!", message, "error" )
            }
        }
    };

    CoreSysTheme.prototype.info = function ( message ) {
        if ( this.alertType === 'sweet' || this.alertType === 'swal' ) {
            if ( typeof swal === 'function' ) {
                swal( "Info!", message, "info" );
            }
        }
    }

    $.CoreSysTheme = new CoreSysTheme();
    $.CoreSysTheme.init();
})( jQuery );