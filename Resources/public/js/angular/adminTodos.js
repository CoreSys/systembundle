CSApp.factory( 'AdminTodosData', ['$q', '$resource', function ( $q, $resource ) {
    return {
        fetch:  function () {
            var defer    = $q.defer(),
                resource = $resource( Routing.generate( 'api_get_todos', {'order_by': {'createdAt': 'desc'}} ), {}, {
                    fetch: {
                        method:  'GET',
                        isArray: true
                    }
                } );

            resource.fetch( {}, function ( res ) {
                defer.resolve( res );
            }, function ( err ) {
                defer.resolve( {error: err} );
            } );

            return defer.promise;
        },
        post:   function ( todo ) {
            var defer    = $q.defer(),
                resource = $resource( Routing.generate( 'api_post_todo' ), {}, {
                    post: {
                        method:  'POST',
                        isArray: false
                    }
                } );

            resource.post( todo, function ( res ) {
                defer.resolve( res );
            }, function ( err ) {
                defer.resolve( {error: err} );
            } );

            return defer.promise;
        },
        save:   function ( todo ) {
            var resource = $resource( Routing.generate( 'api_put_todo', {entity: todo.id} ), {}, {
                save: {
                    method:  'put',
                    isArray: false
                }
            } );
            resource.save( todo );
            return null;
        },
        delete: function ( todo ) {
            var defer    = $q.defer(),
                resource = $resource( Routing.generate( 'api_delete_todo', {entity: todo.id} ), {}, {
                    delete: {
                        method:  'DELETE',
                        isArray: false
                    }
                } );

            resource.delete( {}, function ( res ) {
                defer.resolve( res );
            }, function ( err ) {
                defer.resolve( {error: err} );
            } );
            return defer.promise;
        }
    };
}] )
    .controller( 'AdminTodosController', ['$scope', 'AdminTodosData', '$uibModal', function ( $scope, AdminTodosData, $uibModal ) {
        /**
         * Scope parameters
         */
        $scope.todos = [];

        /**
         * Scope methods
         */
        $scope.createTodo = function () {
            var modalInstance = $uibModal.open( {
                animation:   false,
                backdrop:    'static',
                templateUrl: Routing.generate( 'api_new_todo', {'_format': 'html'} ),
                controller:  ['$scope', '$uibModalInstance', function ( $scope, $uibModalInstance ) {
                    /**
                     * Scope parameters
                     */
                    $scope.todo = {
                        'type':     'todo',
                        'complete': false,
                        'title':    null,
                        'body':     null
                    };

                    $scope.data = {error: null};

                    $scope.save = function () {
                        $scope.ok();
                    };

                    $scope.ok = function () {
                        if ( $scope.todo.title === null || ( $scope.todo.title + '').trim().length <= 0 ) {
                            $scope.data.error = 'Please enter a title.';
                        } else if ( $scope.todo.body === null || ( $scope.todo.body + '').trim().length <= 0 ) {
                            $scope.data.error = 'Please enter a body.';
                        } else {
                            $uibModalInstance.close( $scope.todo );
                        }
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss( 'cancel' );
                    };
                }]
            } );
            modalInstance.result.then( function ( todo ) {
                if ( todo !== null && todo !== undefined && typeof todo === 'object' ) {
                    AdminTodosData.post( todo ).then( function ( res ) {
                        $scope.todos.unshift( res );
                        setTimeout( function () {
                            $scope.$apply();
                        }, 100 );
                    } );
                }
            }, function () {

            } );
        };

        $scope.toggleComplete = function ( todo ) {
            todo.complete = !todo.complete;
            AdminTodosData.save( todo );
        };

        $scope.deleteTodo = function ( todo, index ) {
            bootbox.confirm( 'Are you sure you want to remove this todo?', function ( bool ) {
                if ( bool ) {
                    AdminTodosData.delete( todo ).then( function ( res ) {
                        $scope.todos.splice( index, 1 );
                    } );
                }
            } );
        };

        function setup() {
            AdminTodosData.fetch().then( function ( res ) {
                $scope.todos = res;
                setTimeout( function () {
                    $scope.$apply();
                }, 100 );
            } );
            return this;
        }

        return setup();
    }] );