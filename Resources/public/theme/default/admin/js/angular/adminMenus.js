CSApp.controller( 'AdminMenusController', ['$scope', '$resource', function ( $scope, $resource ) {
    /**
     * Scope Parameters
     */
    $scope.menus = AdminMenusData;
    $scope.sortOptions = {
        handle: '.handle',
        axis:   'y'
    };

    $scope.toggleAdminMenuActive = function ( menu, idx ) {
        menu.active  = !menu.active;
        var resource = $resource( Routing.generate( 'api_put_adminmenu', {entity: menu.id} ), {}, {save: {method: 'PUT'}} );
        resource.save( menu, function ( res ) {
            $( 'li[data-admin-menu="' + menu.id + '"]' ).toggle( 'slow' );
        }, function ( err ) { console.error( err ); } );
    };

    console.debug( 'Menus', $scope.menus );
}] );