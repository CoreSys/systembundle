CSApp.controller( 'AdminMenusController', ['$scope', '$resource', function ( $scope, $resource ) {
    /**
     * Scope Parameters
     */
    $scope.menus = AdminMenusData;

    $scope.sortOptions = {
        handle: '.handle',
        axis:   'y',
        update: updateMenuPositions
    };

    $scope.toggleAdminMenuActive = function ( menu, idx ) {
        menu.active  = !menu.active;
        var resource = $resource( Routing.generate( 'api_put_adminmenu', {entity: menu.id} ), {}, {save: {method: 'PUT'}} );
        resource.save( menu, function ( res ) {
            $( 'li[data-admin-menu="' + menu.id + '"]' ).toggle( 'slow' );
        }, function ( err ) { console.error( err ); } );
    };

    $scope.saveMenuOrder = function ( idList ) {
        if ( $.isArray( idList ) ) {
            var resource = $resource( Routing.generate( 'api_put_adminmenu_order', {'_format': 'json'} ), {}, {save: {method: 'PUT'}} );
            resource.save( {order: idList}, function ( res ) {
                console.debug( 'Success', res );
            }, function ( err ) {
                console.error( err );
            } );
        }
    };

    function updateMenuPositions( event, ui ) {
        setTimeout( function () {
            var list = [];
            $.each( $scope.menus, function ( idx, menu ) {
                list.push( menu.id );
            } );
            console.debug( 'LIST', list );
            $scope.saveMenuOrder( list );

            var $container = $( 'ul.csmenu' );
            $.each( list, function ( idx, id ) {
                var $item = $container.find( 'li[data-admin-menu="' + id + '"]' );
                if ( $item.length ) {
                    $item.detach();
                    $container.append( $item );
                }
            } );
        }, 500 );
    }

    console.debug( 'Menus', $scope.menus );
}] );