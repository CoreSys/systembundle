<?php

namespace CoreSys\CoreBundle\Subscriber;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\Common\EventSubscriber;
use JMS\DiExtraBundle\Annotation as DI;
use CoreSys\CoreBundle\Controller\BaseController;

/**
 * Class DatabasePrefixSubscriber
 * @package CoreSys\CoreBundle\Subscriber
 * @DI\Service("core_sys_core.subscriber.database_prefix")
 * @DI\Tag("doctrine.event_subscriber")
 */
class DatabasePrefixSubscriber implements EventSubscriber
{

    /**
     * @var string
     */
    protected $prefix = '';

    /**
     * DatabasePrefixSubscriber constructor.
     *
     * @DI\InjectParams({
     *     "baseController" = @DI\Inject("core_sys_core.controller.base")
     * })
     * @param BaseController|null $baseController
     */
    public function __construct( BaseController $baseController = NULL )
    {
        $this->prefix = $baseController->getContainer()->getParameter( 'core_sys_core.db_prefix' );
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata( LoadClassMetadataEventArgs $eventArgs )
    {
        if ( $this->prefix === '' || empty( $this->prefix ) ) {
            return;
        }

        $classMetadata = $eventArgs->getClassMetadata();
        if ( strlen( $this->prefix ) ) {
            if ( 0 !== strpos( $classMetadata->getTableName(), $this->prefix ) ) {
                $classMetadata->setPrimaryTable( array( 'name' => $this->prefix . $classMetadata->getTableName() ) );
            }
        }
        foreach ( $classMetadata->getAssociationMappings() as $fieldName => $mapping ) {
            if ( $mapping[ 'type' ] == \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_MANY ) {
                if ( !isset( $classMetadata->associationMappings[ $fieldName ][ 'joinTable' ] ) ) {
                    continue;
                }
                try {
                    $mappedTableName = $classMetadata->associationMappings[ $fieldName ][ 'joinTable' ][ 'name' ];
                    if ( 0 !== strpos( $mappedTableName, $this->prefix ) ) {
                        $classMetadata->associationMappings[ $fieldName ][ 'joinTable' ][ 'name' ] = $this->prefix . $mappedTableName;
                    }
                } catch ( \Exception $e ) {
                    // do nothing
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array( 'loadClassMetadata' );
    }
}