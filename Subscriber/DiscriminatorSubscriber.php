<?php

namespace CoreSys\CoreBundle\Subscriber;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class DiscriminatorSubscriber
 * @package CoreSys\CoreBundle\Subscriber
 * @DI\Service("core_sys_core.subscriber.discriminator")
 * @DI\Tag("doctrine.event_subscriber")
 */
class DiscriminatorSubscriber implements EventSubscriber
{

    /**
     * @var string
     */
    const ENTRY_ANNOTATION = 'CoreSys\CoreBundle\Annotation\DiscriminatorEntry';

    /**
     * @var
     */
    private $map;

    /**
     * @var array
     */
    private $cachedMap;

    /**
     * @var
     */
    private $reader;

    /**
     * DiscriminatorSubscriber constructor.
     */
    public function __construct()
    {
        $this->reader    = new AnnotationReader();
        $this->map       = array();
        $this->cachedMap = array();
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    /**
     * @param LoadClassMetadataEventArgs $e
     */
    public function loadClassMetadata( LoadClassMetadataEventArgs $e )
    {
        $this->map = array();
        $class     = $e->getClassMetadata()->name;


        if ( !strstr( $class, 'Mizzium' ) ) {
            return;
        }

        if ( array_key_exists( $class, $this->cachedMap ) ) {
            $this->overrideMetadata( $e, $class );

            return;
        }

        $discriminatorMap = $e->getClassMetadata()->discriminatorMap;

        $hasDiscriminatorEntry = $this->extractEntry( $class );

        if ( $hasDiscriminatorEntry ) {
            $this->checkClass( $class );
        } else {
            return;
        }

        $dMap = array_flip( $this->map );

        foreach ( $this->map as $className => $discriminator ) {
            $this->addToCachedMap( $className, 'map', $dMap )
                 ->addToCachedMap( $className, 'discr', $discriminator );
        }

        $this->overrideMetadata( $e, $class );
    }

    /**
     * @param $class
     */
    private function checkClass( $class )
    {
        $rc     = new \ReflectionClass( $class );
        $parent = $rc->getParentClass();

        if ( !empty( $parent ) ) {
            $parent = $parent->name;
            $this->checkClass( $parent );
        } else {
            $this->addToCachedMap( $class, 'isParent', TRUE );
            $this->checkClassChildren( $class );
        }
    }

    /**
     * @param      $class
     * @param      $key
     * @param null $value
     *
     * @return $this
     */
    private function addToCachedMap( $class, $key, $value = NULL )
    {
        if ( !is_array( $this->cachedMap ) ) {
            $this->cachedMap = array();
        }

        if ( !isset( $this->cachedMap[ $class ] ) || !is_array( $this->cachedMap[ $class ] ) ) {
            $this->cachedMap[ $class ] = array(
                'isParent' => FALSE
            );
        }

        $this->cachedMap[ $class ][ $key ] = $value;

        return $this;
    }

    /**
     * @param $class
     */
    private function checkClassChildren( $class )
    {
        foreach ( $this->getSubClasses( $class ) as $className ) {
            $rc     = new \ReflectionClass( $className );
            $parent = $rc->getParentClass();
            if ( !empty( $parent ) ) {
                $parent = $parent->name;
                if ( $parent == $class ) {
                    $hasDiscriminatorEntry = $this->extractEntry( $className );
                    if ( $hasDiscriminatorEntry ) {
                        if ( !array_key_exists( $className, $this->map ) ) {
                            $this->checkClassChildren( $className );
                        }
                    }
                }
            }
        }
    }

    /**
     * @param LoadClassMetadataEventArgs $event
     * @param                            $class
     */
    private function overrideMetadata( LoadClassMetadataEventArgs $event, $class )
    {
        $event->getClassMetadata()->discriminatorMap   = $this->cachedMap[ $class ][ 'map' ];
        $event->getClassMetadata()->discriminatorValue = $this->cachedMap[ $class ][ 'discr' ];

        $isParent = isset( $this->cachedMap[ $class ][ 'isParent' ] ) && $this->cachedMap[ $class ][ 'isParent' ] === TRUE;

        if ( $isParent ) {
            $subClasses = $this->cachedMap[ $class ][ 'map' ];
            unset( $subClasses[ $this->cachedMap[ $class ][ 'discr' ] ] );
            $event->getClassMetadata()->subClasses = array_values( $subClasses );
        }
    }

    /**
     * @param $class
     *
     * @return array
     */
    private function getSubClasses( $class )
    {
        $subClasses = array();

        foreach ( get_declared_classes() as $potentialSubClass ) {
            $rc = new \ReflectionClass( $potentialSubClass );
            if ( $rc->isSubclassOf( $class ) ) {
                $subClasses[] = $potentialSubClass;
            }
        }

        return $subClasses;
    }

    /**
     * @param $class
     *
     * @return bool
     * @throws \Exception
     */
    private function extractEntry( $class )
    {
        $rc = new \ReflectionClass( $class );

        $annotation = $this->reader->getClassAnnotation( $rc, self::ENTRY_ANNOTATION );
        if ( !empty( $annotation ) ) {
            $value = $annotation->value;
            if ( in_array( $value, $this->map ) ) {
                throw new \Exception( 'Found duplicate discriminator map entry "$value" in "$class"' );
            }

            $this->map[ $class ] = $value;

            return TRUE;
        }

        return FALSE;
    }
}